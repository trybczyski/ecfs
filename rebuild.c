/*
  	This code is derived from function prototypes found /usr/include/fuse/fuse.h
  	Copyright (C) 2001-2007  Miklos Szeredi <miklos@szeredi.hu>
  	His code is licensed under the LGPLv2.

  	This code is derived from Big Brother File System
  	Copyright (C) 2012 Joseph J. Pfeiffer, Jr., Ph.D. <pfeiffer@cs.nmsu.edu>
  	His code is licensed under the GNU GPLv3.

	Erasure coding provided by Jerasure - A C/C++ Library for a Variety of Reed-Solomon and RAID-6 Erasure Coding Techniques
	Copright (C) 2007 James S. Plank <plank@cs.utk.edu>
	His code is licensed under the GNU LGPLv2.1.

	/raidfs -o direct_io -o default_permissions rootdir/ mountdir/
	direct_io 		- disable system cashe, bigger and constant read/write buffers
	default_permissions 	- disable file permission checking by the fuse

*/

//#include "params.h"

#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
//#include <fuse.h>
#include <time.h>
#include <libgen.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/xattr.h>
#include <math.h>

#include <sys/stat.h>
#include <signal.h>
#include "jerasure.h"
#include "reed_sol.h"
#include "galois.h"
#include "cauchy.h"
#include "liberation.h"
//#include "log.h"


int main(int argc, char *argv[])
{
	int k, m, w, blocksize, buffersize, s;
	char * s1;
	char s2[100];
	char *pt;
	char * fname;
	char * dname;
	char * dirname;
	char ** filelist;
	char ** list;
	char ** rebuildlist;
	char ** relist = NULL;
	FILE * dirfile;
	int i, j, ii;
	int fileCount = 0;
	int rebuildCount = 0;

	int *matrix;
	char *rootdir;
	char **dir_k;
	char **dir_m;
	int curdir = 5;

	char fnam2[100];
	char dnam[100];

	FILE **fdir_k;//[28];
    	FILE **fdir_m;//[2];
	//FILE *metafile;
	char **data;
	char **coding;
	char *block;
	size_t size;
	int *erasures;
	char* bptr;
	size_t reads;

	int numerased=1; //We can assume that one node is down
	
    	DIR *dp;
    	struct dirent *de;

	time_t now = time(0);
	FILE* fp;
	FILE * from_fp;
	FILE * to_fp;
	struct stat fileStat;
  
	char* buf;

	dname = (char*)malloc(sizeof(char)*(1000));
	fname = (char*)malloc(sizeof(char)*(1000));
	dirname = (char*)malloc(sizeof(char)*(1000));




	/* Read in parameters from setup file */
	fp = fopen("Coding_parameters.txt", "rb");
	if (fp == NULL) {
		fprintf(stderr, "Can't open Coding_parameters.txt!\n");
		exit(0);	
	}
	if (fscanf(fp, "%d %d %d %d %d %d", &k, &m, &w, &blocksize, &buffersize, &s) != 6) {
		fprintf(stderr, "Parameters are not correct\n");
		exit(0);
	}
	fclose(fp);

	buf = (char*)malloc(sizeof(char)*(blocksize));

	fdir_k = (FILE **)malloc(sizeof(FILE*)*k);
	fdir_m = (FILE **)malloc(sizeof(FILE*)*m);
	fdir_s = (FILE **)malloc(sizeof(FILE*)*s);
	data = (char **)malloc(sizeof(char*)*k);
	coding = (char **)malloc(sizeof(char*)*m);
	block = (char *)malloc(sizeof(char)*buffersize);
	erasures = (int *)malloc(sizeof(int)*(k+m));
	for (i = 0; i < k; i++)
		data[i] = (char *)malloc(sizeof(char)*blocksize);
	for (i = 0; i < m; i++)
		coding[i] = (char *)malloc(sizeof(char)*blocksize);
	for (i = 0; i < s; i++)
		coding[i] = (char *)malloc(sizeof(char)*blocksize);

    	// Pull the rootdir out of the argument list and save it in my
    	// internal data
    	rootdir = realpath(argv[argc-2], NULL);
    	argv[argc-2] = argv[argc-1];
    	argv[argc-1] = NULL;
    	argc--;
    	

	matrix = NULL;
	matrix = reed_sol_r6_coding_matrix(k, w);


	/*Open file with names of directories*/
	strcpy(dirname, rootdir);
	s1 = strrchr(dirname, '/');
	if (s1 != NULL){
		*s1 = '\0';
    	}
	sprintf(dname, "%s/Coding_directories.txt", dirname);
	dirfile = fopen(dname, "r");
	if (dirfile == NULL) {
		fprintf(stderr, "Can't open Coding_directories.txt!\n");
		exit(0);	
	}
	dir_k = (char **)malloc(sizeof(char*)*k);
	dir_m = (char **)malloc(sizeof(char*)*m);
	for (i = 0; i < k; i++) {
		dir_k[i] = (char *)malloc(sizeof(char)*100);
		fscanf(dirfile, "%s", dir_k[i]);
	}
	for (i = 0; i < m; i++) {
		dir_m[i] = (char *)malloc(sizeof(char)*100);
		fscanf(dirfile, "%s", dir_m[i]);
	}
    	fclose(dirfile);

	if (curdir <= k) 
		sprintf(dirname, "%s/%s", rootdir, dir_k[curdir-1]);
	else
		sprintf(dirname, "%s/%s", rootdir, dir_m[curdir-k-1]);
fprintf(stderr, "dirname=%s\n", dirname);
	dp = opendir(dirname);
	while ((de = readdir(dp)) != NULL){
		strncpy(dnam, de->d_name, 100);
		pt = strrchr(dnam, '_');
		s1 = strrchr(dnam, '.');
		if (s1 != NULL){
			*s1 = '\0';
    		}
fprintf(stderr, "%s\n", pt);
		if(pt!=NULL){
			if ((strcmp(pt , "_meta") == 0)) {
			
				fileCount++;
			}
		}
    	} 
	fprintf(stderr, "fileCount = %d\n", fileCount);
	closedir(dp);
	
	filelist =(char **)malloc(sizeof(char*)*fileCount);
	list = filelist;
	for (i = 0; i < fileCount; i++) {
		list[i] = (char *)malloc(sizeof(char)*100);
	}

	dp = opendir(dirname);
	i=0;
	while ((de = readdir(dp)) != NULL){
		strncpy(dnam, de->d_name, 100);
		pt = strrchr(dnam, '_');
		s1 = strrchr(dnam, '.');
		if (s1 != NULL){
			*s1 = '\0';
    		}
		if(pt!=NULL){
			if ((strcmp(pt , "_meta") == 0)) {
				strcpy(list[i], de->d_name);
				fprintf(stderr, "list %d = %s\n", i, list[i]);
				i++;
				
			}
		}
	}
	closedir(dp);
	for (i=0; i< s; i++){
		/*Spare disk check*/
		
	}
	for (i = 0; i < k+m; i++) {
		if (i == curdir-1) continue;
		if (i < k)
			sprintf(dirname, "%s/%s", rootdir, dir_k[i]);
		else {
			sprintf(dirname, "%s/%s", rootdir, dir_m[i-k]);
    		}
		fprintf(stderr, "dirname%s\n", dirname);
		dp = opendir(dirname);
    		if (dp == NULL) {
			break;
		}
    		de = readdir(dp);
    		if (de == NULL) {
			break;
		}
		do 
		{
			strcpy(fnam2, de->d_name);
			pt = strrchr(fnam2, '_');
			s1 = strrchr(fnam2, '.');
			memcpy(s2, s1, 100);
			if (s1 != NULL){
				*s1 = '\0';
    			}
			if ((pt != NULL) &&(strcmp(pt , "_meta") == 0) ) {
				sprintf(fname, "%s/%s", dirname, de->d_name);
				if(stat(fname,&fileStat) < 0)    
        				return 1;
				if (fileStat.st_mtime < now) {
					fprintf(stderr, "file %s ", de->d_name);
					for (j=0; j<fileCount; j++) {
						
						if(strcmp (de->d_name,list[j]) == 0) {
							fprintf(stderr, "Found\n");
							break;
						}else if (j == fileCount - 1) {
							*pt = '\0';
							if (curdir <= k) 
								sprintf(fname, "%s/%s_k%d%s", dirname, fnam2, curdir, s2);
							else
								sprintf(fname, "%s/%s_m%d%s", dirname, fnam2, curdir-k, s2);
							from_fp = fopen(fname, "rb");
							if (from_fp == NULL) {
								if (rebuildCount==0){
									fprintf(stderr, "Not found\n");
									rebuildCount++;
									rebuildlist = (char**)malloc(sizeof(char*)*rebuildCount);
									relist = rebuildlist;
									relist[rebuildCount-1] = (char*)malloc(sizeof(char)*100);
									strcpy(relist[rebuildCount-1], de->d_name);
								}else{
									for(ii = 0; ii<rebuildCount; ii++) {
										if(strcmp(relist[ii], de->d_name)==0){
											fprintf(stderr, "Already on the list\n");
											break;
										}else if(ii==(rebuildCount-1)) {
											fprintf(stderr, "Not found\n");
											rebuildCount++;
											rebuildlist = (char **)realloc(relist, sizeof(char*)*rebuildCount);
									
											relist = rebuildlist;
											relist[rebuildCount-1] = (char*)malloc(sizeof(char)*100);
											strcpy(relist[rebuildCount-1], de->d_name);
											break;
										}
									}
								}
							} else {
								sprintf(fname, "%s/%s/%s_k%d%s", rootdir, dir_k[curdir-1], fnam2, curdir, s2);
								fprintf(stderr, "Copy\n");
								fileCount++;
								filelist =(char **)realloc(list, sizeof(char*)*fileCount);
								list = filelist;
								list[fileCount-1] = (char *)malloc(sizeof(char)*100);
								strcpy(list[fileCount-1], de->d_name);
								fprintf(stderr, "added %d = %s\n", fileCount-1, list[fileCount-1]);
								to_fp = fopen(fname,"wb");
								do{
									reads = fread(buf, sizeof(char), blocksize, from_fp);
									fwrite(buf, sizeof(char), reads, to_fp);
								} while(reads==blocksize);
							
								//make copy
								fclose(to_fp);
								
								fclose(from_fp);
								break;	
							}
						}
					}
				}
			}
    		} while ((de = readdir(dp)) != NULL);
    		closedir(dp);
	}
	
    	//fuse_stat = fuse_main(argc, argv, &fs_oper, fs_data);
	

	for (ii=0; ii < rebuildCount; ii++) {
		for(j=0; j < fileCount; j++) {
			if(strcmp(relist[ii],list[j])==0)
				break;
			else if(j==fileCount-1) {
				/* RECALCULATION */
				fprintf(stderr, "rebuild %s \n", relist[ii]);
				strcpy(fnam2, relist[ii]);
				s1 = strchr(fnam2, '.');
				memcpy(s2, s1, 100);
				s1 = strchr(fnam2, '_');
    				if (s1 != NULL){
					*s1 = '\0';
    				}
				
				for (i=0;  i< k; i++) {
					sprintf(fname, "%s/%s/%s_k%d%s", rootdir, dir_k[i], fnam2, i+1, s2);
					if(i==curdir-1) {
						fdir_k[i] = fopen(fname, "wb");
						erasures[numerased] = i;
						numerased++;
					} else {
						fdir_k[i] = fopen(fname, "rb");
						if (fdir_k[i] == NULL) {
							erasures[numerased] = i;
							numerased++;
						} 
					}			
				}

				for (i=0; i<m; i++) {
					sprintf(fname, "%s/%s/%s_m%d%s", rootdir, dir_m[i], fnam2, i+1, s2);
					if (i==curdir-k-1) {
						fdir_m[i] = fopen(fname, "wb");
						erasures[numerased] = k+i;
						numerased++;
					} else {
						fdir_m[i] = fopen(fname, "rb");
						if (fdir_m[i] == NULL) {
							erasures[numerased] = k+i;
							numerased++;
						} 
					}
				}


				if(numerased>m) {
					/*
					for (i=0; i<FS_DATA->k; i++) {
						if (fdir_k[i] != NULL) {
							fclose(fdir_k[i]);
						}
					}
					for (i=0; i<m; i++)  {
						if (fdir_m[i] != NULL) {
							fclose(fdir_m[i]);
						}
					}
					*/
				}
		
				erasures[numerased] = -1;
				reads = 0;
				do{
					//fprintf(stderr, "reads = %ld\n", reads);
					for (i = 0; i < k; i++) {
						if ((fdir_k[i] != NULL) && (i!=curdir-1)) {
							reads=fread(data[i], sizeof(char), blocksize, fdir_k[i]);
						}
					}
					for (i = 0; i < m; i++) {
						if ((fdir_m[i] != NULL)&&(i!=curdir-k-1)) {
							reads=fread(coding[i], sizeof(char), blocksize, fdir_m[i]);
						}
					}
					i = jerasure_matrix_decode(k, m, w, matrix, 1, erasures, data, coding, blocksize);
					/* Copy blocks to buf */
					bptr = block;
					for (i = 0; i<k; i++) {
						memcpy(bptr, data[i], blocksize);
						bptr = bptr + blocksize;
					}
					for (i = 0; i < k; i++)  {
						memcpy(data[i], block+(i*blocksize), blocksize);
					}
					reed_sol_r6_encode(k, w, data, coding, blocksize);
					if (curdir-1<k)
						fwrite(data[curdir-1], sizeof(char), reads, fdir_k[curdir-1]);
					else
						fwrite(coding[curdir-1-k], sizeof(char), reads, fdir_m[curdir-k-1]);
				}while (reads==blocksize);
				for (i=0; i<k; i++) {
					if (fdir_k[i] != NULL) 
						fclose(fdir_k[i]);
				}

				for (i=0; i<m; i++) {
					if (fdir_m[i] != NULL) 
						fclose(fdir_m[i]);
				}
	
				/* END OF RECALCULATION */
			}
				
		}
	}


	for (i = 0; i < k; i++)
		free(data[i]);
	for (i = 0; i < m; i++)
		free(coding[i]);
	free(fdir_k);
	free(fdir_m);
	free(data);
	free(coding);
	free(block);
	free(erasures);




	for(i=0; i<fileCount; i++){
		free(list[i]);
	}
	for(i=0; i<rebuildCount; i++){
		free(relist[i]);
	}
	free(relist);
	free(list);
	free(buf);
	free(fname);
	free(dname);
	free(dirname);
	for (i = 0; i < k; i++) {
		free(dir_k[i]);
	}
	for (i = 0; i < m; i++) {
		free(dir_m[i]);
	}
	free(dir_k);
	free(dir_m);
	free(matrix);
	free(rootdir);

    	return 0;
}

