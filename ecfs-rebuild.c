#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <time.h>
#include <libgen.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/xattr.h>
#include <math.h>
#include <time.h>

#include <sys/stat.h>
#include <signal.h>
#include "jerasure.h"
#include "reed_sol.h"
#include "galois.h"
#include "cauchy.h"
#include "liberation.h"
#include <openssl/sha.h>
//#include "log.h"

#define MAX_PATH 1000

enum Coding_Technique {Reed_Sol_Van, Reed_Sol_R6_Op, Cauchy_Orig, Cauchy_Good, Liberation, Blaum_Roth, Liber8tion};

/* Function checking if w number is a prime. */
int is_prime(int w) {
	int prime55[] = {2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,
	    73,79,83,89,97,101,103,107,109,113,127,131,137,139,149,151,157,163,167,173,179,
		    181,191,193,197,199,211,223,227,229,233,239,241,251,257};
	int i;
	for (i = 0; i < 55; i++) {
		if (w == prime55[i]) 
			return 1;
	}
	return 0;
};

int update_blockfile(char * ptr, size_t count, FILE * stream) {
	int writes = 0;
	int retries = 3;
	int ret = 0;
	if (stream != NULL) {
		do {
			writes += fwrite(ptr + writes, sizeof(char), count - writes, stream);
			retries --;
		} while ((writes != count ) && (retries > 0));
		if (writes != count)	
			ret = -errno;
		else
			ret = 0;
	} else {
		ret = -1;
	}
	return ret;
}

int update_buffer(char * ptr, size_t count, FILE * stream) {
	int reads = 0;
	int retries = 3;
	int ret = 0;
	if (stream != NULL) {
		do {
			reads += fread(ptr + reads, sizeof(char), count - reads, stream);
			retries --;
		} while ((reads != count ) && (retries > 0));
		if (reads != count) 	
			ret = -errno;
		else
			ret = 0;
	} else {
		ret = -1;
	}
	return ret;
}

int main(int argc, char *argv[])
{
	int k, m, w, blocksize, buffersize, packetsize;
	char * pt;
	char path[MAX_PATH];
	char * curdir;
	int i, j;
	int technb;
	
	enum Coding_Technique tech;
	FILE * fp;

	char ** filelist;
	char ** flist = NULL;
	char ** rebuildlist;
	char ** rlist = NULL;
	char ** dir;
	char ** list = NULL;
	FILE ** blockfile;
SHA_CTX hash;
	char **data;
	char **coding;
	char * block;
	int *erasures;
	int blockfilesize;

	DIR *dp;
    	struct dirent *de;
	
	int nboffiles=0;
	int rc;
	int nbOfDirs;
	int nbOfRebuilds=0;	
	int rebuildIndex;
	
	char mname[MAX_PATH];
	FILE * metafile;

	int block_nb;
	char fname[MAX_PATH];
	int numerased=0;
	FILE * blockfile_to_rebuild;
	int to_rebuild;
	int reads;
	size_t size;  
	char dir_temp[MAX_PATH];
	int * matrix;
	int * bitmatrix;
	int **schedule;
	char ** block_hash;
	unsigned char digest[20];
	matrix = NULL;
	bitmatrix = NULL;
	schedule = NULL;
char file_hash_temp[40];
char block_hash_temp[40];
int sha_temp;  
 	
    	// Perform some sanity checking on the command line:  make sure
    	// there are enough arguments, and that neither of the last two
    	// start with a hyphen (this will break if you actually have a
    	// rootpoint or mountpoint whose name starts with a hyphen, but so
    	// will a zillion other programs)
   	if ((argc < 2) || (argv[argc-1][0] == '-')){
		fprintf(stderr, "usage:  ecfs [FUSE and mount options] mountPoint\n");
		exit(0);
	}

    	// Pull the mountdir out of the argument list and save it in my
    	// internal data
    	curdir = realpath(argv[argc-1], NULL);
  
	/* Read rebuild directory for present files */
	dp = opendir(curdir);
	while ((de = readdir(dp)) != NULL) {
		pt = strrchr(de->d_name, '_');
		if (pt==NULL) continue;
		if(strcmp(pt, "_meta")==0) {
			*pt = '\0';
			nboffiles++;
			filelist = (char **)realloc(flist, sizeof(char*)*nboffiles);
			filelist[nboffiles-1] = (char *)malloc(sizeof(char)*MAX_PATH);
			strncpy(filelist[nboffiles-1], de->d_name, MAX_PATH);
			fprintf(stderr, "added %s \n", de->d_name);
			flist = filelist;
		}
    	} 
	closedir(dp);
	fprintf(stderr, "\n nb of files in %s : %d \n", curdir, nboffiles);    	
 	
	fp = fopen("Coding_parameters.txt", "rb");
	if (fp == NULL) {
		fprintf(stderr, "Can't open Coding_parameters.txt\n");
		exit(0);	
	}
	if (fscanf(fp, "%d %d %d %d %d %d", &k, &m, &w, &packetsize, &blocksize, &technb) !=6) {
		fprintf(stderr, "Parameters are not correct\n");
		exit(0);
	}
	if (technb<0 || technb>6) {
		fprintf(stderr, "Not valid coding technique\n");
		exit(0);
	}
	fclose(fp);
	buffersize = blocksize*k;
	if (k<0 || m<0 || w<0 || packetsize<0 || blocksize<0) {
		fprintf(stderr, "Parameters are not correct\n");
		exit(0);
	}
	switch(technb) {
		case 0:
			if (w != 8 && w != 16 && w != 32) {
				fprintf(stderr,  "w must be one of {8, 16, 32}\n");
				exit(0);
			}
			tech = Reed_Sol_Van;
			break;
		case 1:
			if (m != 2) {
				fprintf(stderr,  "m must be equal to 2\n");
				exit(0);
			}
			if (w != 8 && w != 16 && w != 32) {
				fprintf(stderr,  "w must be one of {8, 16, 32}\n");
				exit(0);
			}
			tech = Reed_Sol_R6_Op;
			break;
		case 2:
			if (packetsize == 0) {
				fprintf(stderr, "Must include packetsize.\n");
				exit(0);
			}
			tech = Cauchy_Orig;
			break;
		case 3:
			if (packetsize == 0) {
				fprintf(stderr, "Must include packetsize.\n");
				exit(0);
			}
			tech = Cauchy_Good;
			break;
		case 4:
			if (k > w) {
				fprintf(stderr,  "k must be less than or equal to w\n");
				exit(0);
			}
			if (w <= 2 || !(w%2) || !is_prime(w)) {
				fprintf(stderr,  "w must be greater than two and w must be prime\n");
				exit(0);
			}
			if (packetsize == 0) {
				fprintf(stderr, "Must include packetsize.\n");
				exit(0);
			}
			if ((packetsize%(sizeof(int))) != 0) {
				fprintf(stderr,  "packetsize must be a multiple of sizeof(int)\n");
				exit(0);
			}
			tech = Liberation;
			break;
		case 5:
			if (k > w) {
				fprintf(stderr,  "k must be less than or equal to w\n");
				exit(0);
			}
			if (w <= 2 || !((w+1)%2) || !is_prime(w+1)) {
				fprintf(stderr,  "w must be greater than two and w+1 must be prime\n");
				exit(0);
			}
			if (packetsize == 0) {
				fprintf(stderr, "Must include packetsize.\n");
				exit(0);
			}
			if ((packetsize%(sizeof(int))) != 0) {
				fprintf(stderr,  "packetsize must be a multiple of sizeof(int)\n");
				exit(0);
			}
			tech = Blaum_Roth;
			break;
		case 6:
			if (packetsize == 0) {
				fprintf(stderr, "Must include packetsize\n");
				exit(0);
			}
			if (w != 8) {
				fprintf(stderr, "w must equal 8\n");
				exit(0);
			}
			if (m != 2) {
				fprintf(stderr, "m must equal 2\n");
				exit(0);
			}
			if (k > w) {
				fprintf(stderr, "k must be less than or equal to w\n");
				exit(0);
			}
			tech = Liber8tion;
			break;
	}
	if (blocksize%(sizeof(int)*w*packetsize) != 0) {
		fprintf(stderr, "Proper values for parameters: blocksize mod (sizeof(int)*w*packetsize) = 0\n");
		exit(0);
	}
	fprintf(stderr, "curdir %s \n", curdir);
	matrix = NULL;
	bitmatrix = NULL;
	schedule = NULL;
	switch(tech) {
		case Reed_Sol_Van:
			matrix 		= reed_sol_vandermonde_coding_matrix(k, m, w);
			break;
		case Reed_Sol_R6_Op:
			matrix 		= reed_sol_r6_coding_matrix(k, w);
			break;
		case Cauchy_Orig:
			matrix 		= cauchy_original_coding_matrix(k, m, w);
			bitmatrix	= jerasure_matrix_to_bitmatrix(k, m, w, matrix);
			schedule 	= jerasure_smart_bitmatrix_to_schedule(k, m, w, bitmatrix);
			break;
		case Cauchy_Good:
			matrix		= cauchy_good_general_coding_matrix(k, m, w);
			bitmatrix 	= jerasure_matrix_to_bitmatrix(k, m, w, matrix);
			schedule 	= jerasure_smart_bitmatrix_to_schedule(k, m, w, bitmatrix);
			break;
		case Liberation:
			bitmatrix 	= liberation_coding_bitmatrix(k, w);
			schedule 	= jerasure_smart_bitmatrix_to_schedule(k, m, w, bitmatrix);
			break;
		case Blaum_Roth:
			bitmatrix	= blaum_roth_coding_bitmatrix(k, w);
			schedule 	= jerasure_smart_bitmatrix_to_schedule(k, m, w,bitmatrix);
			break;
		case Liber8tion:
			bitmatrix 	= liber8tion_coding_bitmatrix(k);
			schedule 	= jerasure_smart_bitmatrix_to_schedule(k, m, w, bitmatrix);
			break;
	}

	data 		= (char **)malloc(sizeof(char *) * k);
       	coding 		= (char **)malloc(sizeof(char *) * m);
       	block 		= (char *)malloc(sizeof(char) * buffersize);
       	erasures 	= (int *)malloc(sizeof(int) * (k + m));
	erasures[0]	= -1;

	for (i=0; i < m; i++)
       	        coding[i] 	= (char *)malloc(sizeof(char) * blocksize);

	for (i=0; i < k ; i++)
		data[i] = block+(i*blocksize);
	block_hash = (char **)malloc(sizeof(char*)*(k+m));
	for (i=0; i < k + m; i++)
		block_hash[i] = (char *)malloc(sizeof(char)*100);

	blockfile = (FILE **)malloc(sizeof(FILE *)*(k+m));
	for (i=0; i < k + m; i++) {
		blockfile[i] = (FILE *) malloc(sizeof(FILE*));
		blockfile[i] = NULL;
	}


	/* Open file with the list of directories */
	
	sprintf(path, "Coding_directories.txt");
	fp = fopen(path, "r");
	if (fp == NULL) {
		fprintf(stderr, "Can't open Coding_directories.txt!\n");
		exit(0);	
	}
	i=0;
	do {
		if (fgets(dir_temp , MAX_PATH , fp) != NULL) {
			i++;
			dir = (char **)realloc(list, sizeof(char*)*i);
			dir[i-1] = (char *)malloc(sizeof(char)*MAX_PATH);
			strncpy(dir[i-1], dir_temp, MAX_PATH);
			pt = strrchr(dir[i-1], '\n');
			if(pt != NULL)
				*pt = '\0';
			list = dir;
		}
	} while (!feof(fp));
    	fclose(fp);
	nbOfDirs=i;
	fprintf(stderr, "nb of dirs: %d\n", nbOfDirs);
	for (i = 0; i < nbOfDirs; i++) {
		dp = opendir(dir[i]);
		if(dp == NULL) continue;
		
		while ((de = readdir(dp)) != NULL) {
			pt = strrchr(de->d_name, '_');
			if (pt == NULL) continue;
			if (strcmp(pt, "_meta")==0) {
				*pt = '\0';
				j = 0;
				do  {
					if (j == nboffiles) {
						nbOfRebuilds++;
						rebuildlist = (char **)realloc(rlist, sizeof(char*)*nbOfRebuilds);
						rebuildlist[nbOfRebuilds-1] = (char *)malloc(sizeof(char)*MAX_PATH);
						strncpy(rebuildlist[nbOfRebuilds-1], de->d_name, MAX_PATH);

						rlist = rebuildlist;
						
						nboffiles++;
						filelist = (char **)realloc(flist, sizeof(char*)*nboffiles);
						filelist[nboffiles-1] = (char *)malloc(sizeof(char)*MAX_PATH);
						strncpy(filelist[nboffiles-1], de->d_name, MAX_PATH);
						flist = filelist;
						break;
					} else {
						if (strcmp(de->d_name, filelist[j])==0) 
						break;
					}
					j++;
				} while (j <= nboffiles);
			}
		}
		closedir(dp);
	}
	for (rebuildIndex = 0; rebuildIndex < nbOfRebuilds; rebuildIndex++) {
		sprintf(path, "/%s", rebuildlist[rebuildIndex]);
		fprintf(stderr, "Rebuild process %d of %d. File: %s \n", rebuildIndex + 1, nbOfRebuilds, path);
		for (i=0; i<nbOfDirs; i++) {
			
			sprintf(mname, "%s%s_meta", dir[i], path);
                	/* Open meta-file */
			metafile = fopen(mname, "rb");
			if (metafile != NULL) {
				/* Read content of the file */
				fscanf(metafile, "%ld", &size);
				fscanf(metafile, "%d %*d %*d %*d %*d %*d %*d", &block_nb);
				fscanf(metafile, "%s", block_hash[block_nb]);
				fscanf(metafile, "%s", file_hash_temp);
               		 	fclose(metafile);
				sprintf(fname, "%s%s_block%d", dir[i], path, block_nb);	
 		               	blockfile[block_nb] = fopen(fname, "rb");
	                }
	        }
		numerased = 0;
		
		for (i = 0; i < k + m; i++) {
			if (blockfile[i] == NULL) {
				fprintf(stderr, "NULL \n");
				erasures[numerased] = i;
                		numerased++;
				erasures[numerased] = -1;
				if(blockfile_to_rebuild == NULL) {
					sprintf(fname, "%s%s_block%d", curdir, path, i);
					blockfile_to_rebuild = fopen(fname, "wb");
					to_rebuild = i;
					fprintf(stderr, "to rebuild block%d : %s\n", to_rebuild, fname);
				}
			}
		}	

		reads = 0;
		if(size%buffersize == 0)
			blockfilesize = size/buffersize*blocksize;
		else
			blockfilesize = (size/buffersize + 1)*blocksize;

		SHA1_Init(&hash);
		while (reads < blockfilesize) {
			for (i = 0; i < k; i++)
				update_buffer(data[i], blocksize, blockfile[i]);
			for (i = 0; i < m; i++)
				update_buffer(coding[i], blocksize, blockfile[i+k]);
	
			if (tech == Reed_Sol_Van || tech == Reed_Sol_R6_Op)
				rc = jerasure_matrix_decode(k, m, w, matrix, 1, erasures, data, coding, blocksize);
			else 
				rc = jerasure_schedule_decode_lazy(k, m, w, bitmatrix, erasures, data, coding, blocksize, packetsize, 1);

			switch(tech) {	
				case Reed_Sol_Van:
					jerasure_matrix_encode(k, m, w, matrix, data, coding, blocksize);
					break;
				case Reed_Sol_R6_Op:
					reed_sol_r6_encode(k, w, data, coding, blocksize);
					break;
				case Cauchy_Orig:
					jerasure_schedule_encode(k, m, w, schedule, data, coding, blocksize, packetsize);
					break;
				case Cauchy_Good:
					jerasure_schedule_encode(k, m, w, schedule, data, coding, blocksize, packetsize);
					break;
				case Liberation:
					jerasure_schedule_encode(k, m, w, schedule, data, coding, blocksize, packetsize);
					break;
				case Blaum_Roth:
					jerasure_schedule_encode(k, m, w, schedule, data, coding, blocksize, packetsize);
					break;
				case Liber8tion:
					jerasure_schedule_encode(k, m, w, schedule, data, coding, blocksize, packetsize);
					break;
			}
			if(to_rebuild < k) {
				SHA1_Update(&hash, data[to_rebuild], blocksize);
				update_blockfile(data[to_rebuild], blocksize, blockfile_to_rebuild);
			} else {
				SHA1_Update(&hash, coding[to_rebuild - k], blocksize);
				update_blockfile(coding[to_rebuild - k], blocksize, blockfile_to_rebuild);
			}
			reads += blocksize;
		}
		SHA1_Final(digest, &hash);
                for(j=0; j<20; j++){
                        sha_temp = (int)digest[j];
                        sprintf(&block_hash_temp[2*j],"%02x", sha_temp);
                }
                sprintf(fname, "%s%s_meta", curdir, path);
		metafile = fopen(fname, "wb");	
                if (metafile!=NULL) {
                        fprintf(metafile, "%ld\n", size);
                        fprintf(metafile, "%d %d %d %d %d %d %d\n",to_rebuild , k, m, w, packetsize, blocksize, (int)tech);
                        fprintf(metafile, "%s\n", block_hash_temp);
                        fprintf(metafile, "%s\n", file_hash_temp);
                        fclose(metafile);
                }
		for (i=0; i<k+m; i++) {
			if(blockfile[i] != NULL)
				fclose(blockfile[i]);
			blockfile[i] = NULL;
		}
		fclose(blockfile_to_rebuild);
		blockfile_to_rebuild = NULL;
	}
	return 0;
}

