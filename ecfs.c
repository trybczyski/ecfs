/*
        This code is derived from function prototypes found /usr/include/fuse/fuse.h
        Copyright (C) 2001-2007  Miklos Szeredi <miklos@szeredi.hu>
        His code is licensed under the LGPLv2.

        This code is derived from Big Brother File System
        Copyright (C) 2012 Joseph J. Pfeiffer, Jr., Ph.D. <pfeiffer@cs.nmsu.edu>
        His code is licensed under the GNU GPLv3.

        Erasure coding provided by Jerasure - A C/C++ Library for a Variety of Reed-Solomon and RAID-6 Erasure Coding Techniques
        Copright (C) 2007 James S. Plank <plank@cs.utk.edu>
        His code is licensed under the GNU LGPLv2.1.

        /ecfs -o direct_io -o default_permissions mountdir/
        direct_io               - disable system cashe, bigger and constant read/write buffers
        default_permissions     - disable file permission checking by the fuse

        Two input files: Coding_parameters.txt and Coding_directories.txt
        
        Order of parameters in Coding_parameters.txt:
        --------------------
        [k m w packetsize blocksize coding_technique]

        Description:
        ------------
        k - number of data block-files
        m - number of coding block-files
        w - length of coding word
        packetsize - size of coding packet
        blocksize - size of input/output blocks for block-files
        coding_technique - number from 0 to 6 corresponds:
                0 - Reed_Sol_Van
                1 - Reed_Sol_R6_Op
                2 - Cauchy_Orig
                3 - Cauchy_Good
                4 - Liberation
                5 - Blaum_Roth
                6 - Liber8tion
        
        Allowed values for the parameters:
        ----------------------------------
        Reed_Sol_Van
                w must be one of {8, 16, 32}
        Reed_Sol_R6_Op
                m must be equal to 2
                w must be one of {8, 16, 32}
        Cauchy_Orig
                Must include packetsize.
        Cauchy_Good
                Must include packetsize.
        Liberation
                k must be less than or equal to w
                Must include packetsize.
                w must be greater than two and w must be prime
                packetsize must be a multiple of sizeof(int)
        Blaum_Roth
                k must be less than or equal to w
                w must be greater than two and w+1 must be prime
                Must include packetsize
                packetsize must be a multiple of sizeof(int)
        Liber8tion
                Must include packetsize
                w must equal 8
                m must equal 2
                k must be less than or equal to w

*/

#define _POSIX_C_SOURCE 200112L

#include "params.h"
#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <fuse.h>
#include <libgen.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/xattr.h>
#include <math.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <signal.h>
#include <poll.h>
#include "jerasure.h"
#include "reed_sol.h"
#include "galois.h"
#include "cauchy.h"
#include "liberation.h"
#include "log.h"
#include <sys/mount.h>

#define MAX_PATH 1000
#define MAX_THREADS 50
#define HEAD_LEN 200
#define BLOCK_HASH_LENGTH 40
#define SHA1_DIEGEST_LENGTH 20
#define FLUSH_PAR 50

char * tech_tab[] = {"Reed_Sol_Van", "Reed_Sol_R6_Op", "Cauchy_Orig", "Cauchy_Good", "Liberation", "Blaum_Roth", "Liber8tion"};

int timeout_msecs = 2000;

struct ct_data {
	int nb;
	struct fs_state * fsdata;
};
/* Function checking if w number is a prime. */
int is_prime(int w) {
        int prime55[] = {2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,
            73,79,83,89,97,101,103,107,109,113,127,131,137,139,149,151,157,163,167,173,179,
                    181,191,193,197,199,211,223,227,229,233,239,241,251,257};
        int i;
        for (i = 0; i < 55; i++) {
                if (w == prime55[i]) 
                        return 1;
        }
        return 0;
};

void error_check(struct coding_data * cd, int * ret) {
        pthread_mutex_lock(cd->m_mutex);
		if (cd->error)
			*ret = cd->error;
        pthread_mutex_unlock(cd->m_mutex);
}

int sha1_finalize(SHA_CTX * hash, char * text) {
	int j;
	int sha_temp;
	unsigned char digest[SHA1_DIEGEST_LENGTH];

	SHA1_Final(digest, hash);
        for(j=0; j < SHA1_DIEGEST_LENGTH; j++) {
               	sha_temp = (int)digest[j];
               	sprintf(&text[2*j],"%02x", sha_temp);
        }
	return 0;
}

int sha1_finalize_t(SHA_CTX * hash, char * text) {

	int j;
	int sha_temp;
	unsigned char digest[SHA1_DIEGEST_LENGTH];

	SHA1_Final(digest, hash);
        for(j=0; j < SHA1_DIEGEST_LENGTH; j++) {
               	sha_temp = (int)digest[j];
               	sprintf(&text[2*j],"%02x", sha_temp);
        }
	return 0;
}

int write_lock(int fd, int * err) {

	int flags;

        struct flock strlock;

	flags = fcntl(fd, F_GETFL, 0);
	fcntl(fd, F_SETFL, flags | O_NONBLOCK);

        strlock.l_type = F_WRLCK; 
        strlock.l_whence = SEEK_SET; 
        strlock.l_start = 0; 
        strlock.l_len = 0; 
        if(fcntl (fd, F_SETLK,  &strlock)==-1) {
                *err = -errno;
                return -1;
        }
        *err = 0;
	return 0;
}

int read_lock(int fd, int * err) {

	int flags;

	struct flock strlock;

	flags = fcntl(fd, F_GETFL, 0);
	fcntl(fd, F_SETFL, flags | O_NONBLOCK); 
       
        strlock.l_type = F_RDLCK; 
        strlock.l_whence = SEEK_SET; 
        strlock.l_start = 0; 
        strlock.l_len = 0; 
        if(fcntl (fd, F_SETLK,  &strlock)==-1) {
                *err = -errno;
                return -1;
        }
        *err = 0;
	return 0;
}
/** Writing function
 * Function writes data stored in ptr of size count to a FILE* stream. It tries to write a number of times
 * before setting a return value to errno.
 */
void blockfile_write(char * ptr, size_t count, int* stream, int * ret) {

        size_t writes = 0;
	size_t sum = 0;

        int retries = 3;
        int rv = 0;
        
        struct pollfd fds;

        fds.fd = *stream;
        fds.events = POLLOUT | POLLWRBAND;
        rv = poll(&fds, 1, timeout_msecs);
        if(rv > 0) {
               if(fds.revents & POLLERR) {
                        *ret = -EPIPE;
               } else if (fds.revents & POLLNVAL) {
                        *ret = -EINVAL;
               } else if (fds.revents & POLLHUP) {
                        *ret = -EPIPE;
               } else {
                        do {
                                writes = write(*stream, ptr + sum, count - sum);
                                sum += writes;
                                retries --;
                        } while ((sum < count ) && (writes != -1) && (retries > 0));
                        if(writes == -1) {
                                *ret = -errno;
                        } else if (sum<count) {
                                *ret = -EIO;
                        } else {
				
                                *ret = 0;
			}
               }       
        } else if (rv == 0) {
                *ret = -ETIMEDOUT; /* a timeout occured */
        } else {
                *ret = -errno; /* an error accured */
        }
};


/** Reading function
 * Function reads the data from a FILE * stream and puts the data to a ptr. It tries to read a number of times
 * before setting a return value to errno.
 */
void blockfile_read(char * ptr, size_t count, int *stream, int * ret) {

        size_t reads = 0;
	size_t sum = 0;

        int retries = 3;
        int rv = 0;
        
        struct pollfd fds;

        fds.fd = *stream;
        fds.events = POLLIN | POLLRDBAND;
        rv = poll(&fds, 1, timeout_msecs);
        if(rv > 0) {
                if(fds.revents & POLLERR) {
                        *ret = -EPIPE;
                } else if (fds.revents & POLLNVAL) {
                        *ret = -EINVAL;
                } else {
                        do {
                                reads = read(*stream, ptr + sum, count - sum);
                                sum += reads;
                                retries --;
                        } while ((sum < count ) && (reads >= 0) && (retries>0));
                        if(reads < 0)
                                *ret = -errno;
                        else if(sum<count)
                                *ret = -EIO;
                        else
                                *ret = 0;
                }
        }  else if (rv == 0) {
                *ret = -ETIMEDOUT; /* a timeout occured */
        } else {
                *ret = -errno; /* an error accured */
        }
}

/** Thread function
 * Function have two states: Operation and Idle. In operation state the function is reading the jobs queue, process
 * the job and checking if the queue contains another job. If not it goes to the idle state and waits for a thread_start
 * signal from the IO function (read() or write());
 */

void * output_thread_function(void *t) {

	struct fs_state * fsdata = (struct fs_state *) t;
        struct job * next_job = NULL;
struct timespec temp = {0, 0};
        long int ret = 0;
	int timedout = 0;
	int fd;
	int error;
	char path[PATH_MAX];
	pthread_mutex_t * mutexp;
	int i=0;
        while (1) {
                // Lock the queue for reading
                pthread_mutex_lock(fsdata->output_queue_mutex);              
                	// Remove a job from the queue
                	if (fsdata->output_queue != NULL ) {
				next_job = fsdata->output_queue;  
				fsdata->output_queue = (fsdata->output_queue)->next;
			}
               	// Release the queue 
                pthread_mutex_unlock(fsdata->output_queue_mutex);
                
                // If there is no job go to idle state, otherwise process the job
                if (next_job != NULL) {
                        // Check if the job is to destroy the thread
                        if (next_job->mode == -1) {
                                free(next_job);
                                break;
                        }
			fd = *next_job->blockfile;
			mutexp = next_job->mutexp;
			error = 0;
                        switch(next_job->mode) { 
                        case 0:
				sprintf(path,"%s%s_block", fsdata->dir[*next_job->dir_nb], (next_job->cd)->path);
                                fd = creat(path, S_IRWXU | S_IRWXG | S_IRWXO);
                                if(fd == -1)
                                      	error = -errno;
                               	else
                                        write_lock(fd, &error);
                                break;
                        case 1:
				blockfile_write(next_job->data, next_job->blocksize, &fd, &error);
				i++;
				if (i>FLUSH_PAR) {
					fsync(fd);
					i = 0;
				}
				break;
			case 2:
				if(lseek(fd,  0,  SEEK_SET) == -1)
					error = -errno;
				break;
			case 3:
				if(close(fd) == -1)
					error = -errno;
				break;
                        }

                        // Signal ready to the main function
			pthread_mutex_lock(mutexp);
				if(!next_job->timedout) {
					next_job->state = 1;
					*next_job->blockfile = fd;
					*next_job->error = error;
					pthread_cond_signal(next_job->condp);
				} else {
					timedout = 1;
				}
       	               	pthread_mutex_unlock(mutexp);
			if(timedout) {
				close(fd);
				next_job->next = NULL;
               			pthread_mutex_lock(fsdata->output_job_mutex);
               				if(fsdata->output_job_pool_first == NULL)
       	        				fsdata->output_job_pool_first = next_job;
        				else
               					(fsdata->output_job_pool_last)->next = next_job;
      					fsdata->output_job_pool_last = next_job;
               			pthread_mutex_unlock(fsdata->output_job_mutex);
				sem_post(fsdata->output_jobs_nb);
				timedout = 0;
			}
			next_job = NULL;			
                } else {
			temp.tv_sec = time(NULL)+1;
                        // IDLE state
                        // Wait for the thread start signal from the main function
                        pthread_mutex_lock(fsdata->output_thread_mutex);
                        //pthread_cond_wait(fsdata->output_thread_cond, fsdata->output_thread_mutex);
			pthread_cond_timedwait(fsdata->output_thread_cond, fsdata->output_thread_mutex,&temp);
                        pthread_mutex_unlock(fsdata->output_thread_mutex);
                }
        }

        // Exit from the thread and return the number of iterations
        pthread_exit((void*) ret);
};

void * input_thread_function(void *t) {

	struct fs_state * fsdata = (struct fs_state *) t;
        struct job * next_job = NULL;

        long int ret = 0;
	int timedout = 0;
	int fd;
	char path[PATH_MAX];
	char path2[PATH_MAX];
	int error;

	pthread_mutex_t * mutexp;
struct timespec temp = {0, 0};
	char * buf = malloc(sizeof(char)*fsdata->blocksize);

        while (1) {
                // Lock the queue for reading
                pthread_mutex_lock(fsdata->input_queue_mutex);              
                	// Remove a job from the queue
                	if (fsdata->input_queue != NULL ) {
				next_job = fsdata->input_queue;  
				fsdata->input_queue = (fsdata->input_queue)->next;
			}
                // Release the queue
                pthread_mutex_unlock(fsdata->input_queue_mutex);
                
                // If there is no job go to idle state, otherwise process the job
                if (next_job != NULL) {
                        // Check if the job is to destroy the thread
                        if (next_job->mode == -1) {
                                free(next_job);
                                break;
                        }
			error = 0;
			mutexp = next_job->mutexp;
                        switch(next_job->mode) { 
                        case 0:
				sprintf(path,"%s%s_block", fsdata->dir[*next_job->dir_nb], (next_job->cd)->path);
				fd = open(path, O_RDONLY);
                                if(fd == -1) {
                                        error = -errno;
                                } else {
                                        if(read_lock(fd, &error) != -1);
						blockfile_read(buf, next_job->blocksize, &fd, &error);
				}
                                break;
                        case 1:
				fd = *next_job->blockfile;
				blockfile_read(buf, next_job->blocksize, &fd, &error);
				break;
			case 2:
				fd = *next_job->blockfile;
				if(close(fd) == -1)
					error = -errno;
				break;
			case 3:
				sprintf(path, "%s", fsdata->dir[*next_job->dir_nb]);
				sprintf(path, "%s", (next_job->cd)->path);
				sprintf(path,"%s%s_block", fsdata->dir[*next_job->dir_nb], (next_job->cd)->path);
                                sprintf(path2,"%s%s_block_R", fsdata->dir[*next_job->dir_nb], (next_job->cd)->path);
                                if(rename(path, path2) == -1)
					error = -errno;
				break;
                        }

                        // Signal ready to the main function 
			pthread_mutex_lock(mutexp);
				if(!next_job->timedout) {
					next_job->state = 1;
					if (next_job->mode != 3) {
						*next_job->error = error;
						*next_job->blockfile = fd;
					}
					if (next_job->mode == 0 || next_job->mode == 1)
						memcpy(next_job->data, buf, next_job->blocksize);
					pthread_cond_signal(next_job->condp);
				} else {
					timedout = 1;
				}
       	               	pthread_mutex_unlock(mutexp);
			if(timedout) {
				close(fd);
				timedout = 0;
				free(next_job);
			}
			next_job = NULL;			
                } else {
			temp.tv_sec = time(NULL)+1;
                        // IDLE state
                        // Wait for the thread start signal from the main function
                        pthread_mutex_lock(fsdata->input_thread_mutex);
			pthread_cond_timedwait(fsdata->input_thread_cond, fsdata->input_thread_mutex, &temp);
                        //pthread_cond_wait(fsdata->input_thread_cond, fsdata->input_thread_mutex);
                        pthread_mutex_unlock(fsdata->input_thread_mutex);
                }
        }
	free(buf);
        // Exit from the thread and return the number of iterations
        pthread_exit((void*) ret);
};


void * output_ctrl_thread_function(void* arg) {
	
	struct fs_state * fsdata = ((struct ct_data *)arg)->fsdata;

	struct timespec temp = {0, 0};
	struct job * next_job = NULL;
	struct job * new_job = NULL;

        long int ret = 0;
	int thread_nb = ((struct ct_data *)arg)->nb;
	int write_error = 0;
	int i;
	int erase_mode = 0;
	int error_count = 0;
	int mode;
	int print_error = 0;
	int dir_nb = thread_nb;
	int change_dir = 0;

	char block_hash[BLOCK_HASH_LENGTH];

	pthread_mutex_t * mutexp = malloc(sizeof(pthread_mutex_t));
        pthread_mutex_init (mutexp, NULL);

	pthread_cond_t * condp = malloc(sizeof(pthread_cond_t));
        pthread_cond_init (condp, NULL);

        while (1) {
                // Lock the queue for reading
                pthread_mutex_lock(fsdata->output_thread_queue_mutex[thread_nb]);
                	next_job = fsdata->output_thread_queue[thread_nb];
	
                	// Remove a job from the queue
               	 	if (fsdata->output_thread_queue[thread_nb] != NULL )
                        	fsdata->output_thread_queue[thread_nb] = (fsdata->output_thread_queue[thread_nb])->next;

                // Release the queue
                pthread_mutex_unlock(fsdata->output_thread_queue_mutex[thread_nb]);
		
                // If there is no job go to idle state, otherwise process the job 
                if (next_job != NULL) {
			if (next_job->mode == -1) {
                       	        free(next_job);
                       	        break;
             	 	}
			mode = next_job->mode;
			switch(mode) { 
			case 0:
				//sprintf(next_job->data, "%s%s_block", dir, (next_job->cd)->path);
				*next_job->dir_nb = dir_nb;
				next_job->mode = 0; // OPEN
				break;
			case 1:
				memset(next_job->data, 0, HEAD_LEN);
				next_job->mode = 1; // WRITE
				break;
			case 2: 
				next_job->mode = 1; // WRITE
				break;
			case 3:
				next_job->mode = 2; // LSEEK
				break;
			case 4:
				sha1_finalize_t(next_job->hash, block_hash);
				for (i=0; i<next_job->blocksize; i++) {
					if(next_job->data[i] == '\0') {
						sprintf(next_job->data+i,"%s\n", block_hash);
						break;
					}
				}
				next_job->mode = 1; // WRITE
				break;	
			case 5: 
				next_job->mode = 3; // CLOSE	
				break;	
			}
			if (*next_job->dir_nb != dir_nb)
				change_dir = 1;

			if(!erase_mode && !change_dir) {
				next_job->mutexp = mutexp;
				next_job->condp = condp;
				next_job->next = NULL;
				next_job->timedout = 0;
				next_job->state = 0;
				do {
					//lock the IO-queue, add the job and signal to threads
					pthread_mutex_lock(fsdata->output_queue_mutex);
       	               				if(fsdata->output_queue == NULL)
       	               				        fsdata->output_queue = next_job;
       	               				else
       	               			        	fsdata->last_output_job->next = next_job;
       	               				fsdata->last_output_job = next_job;
						pthread_cond_signal(fsdata->output_thread_cond);
       	               			pthread_mutex_unlock(fsdata->output_queue_mutex);
	
					// Wait for IO thread
					pthread_mutex_lock(mutexp);
					do {
						write_error = 0;
						if(next_job->state) {
							write_error = *(next_job->error);
							if (write_error)
									error_count++;
							*(next_job->error) = 0;
						} else {
							temp.tv_sec = time(NULL)+fsdata->otimeo;
	 						write_error = (-1) * pthread_cond_timedwait(condp, mutexp, &temp);
							if (write_error == -ETIMEDOUT) {
								error_count++;
								if (error_count == fsdata->oret)
									next_job->timedout = 1;
							} else if (write_error == 0) {
								write_error = *(next_job->error);
								if (write_error)
									error_count++;
								*(next_job->error) = 0;
							}
						}
					} while (write_error == -ETIMEDOUT && error_count < fsdata->oret);
					if (write_error != -ETIMEDOUT)
						pthread_mutex_unlock(mutexp);	
					else 
						break;
				} while (write_error && error_count < fsdata->oret);
			} else if (erase_mode) {
				if (!(*next_job->erased)) {
					pthread_mutex_lock((next_job->cd)->m_mutex);
						(next_job->cd)->numerased++;
						if((next_job->cd)->numerased > (next_job->cd)->m)
							(next_job->cd)->error = -EIO;
					pthread_mutex_unlock((next_job->cd)->m_mutex);
					*next_job->erased = 1;
				}	
			} else if (change_dir && !erase_mode) {
				write_error = -1;
			}

			if (write_error) {
				if (!change_dir) {
					// Change target directory
					pthread_mutex_lock(fsdata->output_dir_mutex);
					if(fsdata->output_spares<(fsdata->nbofdirs - fsdata->k - fsdata->m)) {
						//dir = fsdata->dir[fsdata->k+fsdata->m+fsdata->output_spares];
						dir_nb = fsdata->k+fsdata->m+fsdata->output_spares;
						fsdata->output_spares++;
						error_count = 0;
					} else {
						erase_mode = 1;
					}
					pthread_mutex_unlock(fsdata->output_dir_mutex);
				} else {
					change_dir = 0;
				}

				if (!erase_mode){
					sem_wait(fsdata->output_jobs_nb);
					pthread_mutex_lock(fsdata->output_job_mutex);
                				new_job = fsdata->output_job_pool_first;
						fsdata->output_job_pool_first = new_job->next;
					pthread_mutex_unlock(fsdata->output_job_mutex);
               				new_job->next           = NULL;
					memcpy(new_job->data, next_job->data, next_job->blocksize);
               				new_job->blockfile      = next_job->blockfile;
               				new_job->hash           = next_job->hash;
               				new_job->error          = next_job->error;
					new_job->erased		= next_job->erased;
               				new_job->blocksize      = next_job->blocksize;
               				new_job->mode           = mode;
					new_job->cd		= next_job->cd;
					new_job->dir_nb		= next_job->dir_nb;
	
					// Add next_job at the beginning of the queue
					pthread_mutex_lock(fsdata->output_thread_queue_mutex[thread_nb]);
	              			new_job->next = fsdata->output_thread_queue[thread_nb];
	                      		fsdata->output_thread_queue[thread_nb] = new_job;
					if (new_job->next == NULL)
						fsdata->last_output_th_job[thread_nb] = new_job;
	              			pthread_mutex_unlock(fsdata->output_thread_queue_mutex[thread_nb]);
						
					if(mode > 0)  {
						// Create new Open File job with new target directory
						sem_wait(fsdata->output_jobs_nb);
						pthread_mutex_lock(fsdata->output_job_mutex);
        	        				new_job = fsdata->output_job_pool_first;
							fsdata->output_job_pool_first = new_job->next;
						pthread_mutex_unlock(fsdata->output_job_mutex);
        	       				new_job->next           = NULL;
						//sprintf(new_job->data, "%s%s_block", dir, (next_job->cd)->path);
        	       				new_job->blockfile      = next_job->blockfile;
        	       				new_job->hash           = next_job->hash;
        	       				new_job->error          = next_job->error;
						new_job->erased		= next_job->erased;
        	       				new_job->blocksize      = MAX_PATH;
        	       				new_job->mode           = 0;
						new_job->cd		= next_job->cd;
						new_job->dir_nb		= next_job->dir_nb;
							
						// Add next_job at the beginning of the queue
						pthread_mutex_lock(fsdata->output_thread_queue_mutex[thread_nb]);
        	       					new_job->next = fsdata->output_thread_queue[thread_nb];
        	               				fsdata->output_thread_queue[thread_nb] = new_job;
							if (new_job->next == NULL)
								fsdata->last_output_th_job[thread_nb] = new_job;
        	       				pthread_mutex_unlock(fsdata->output_thread_queue_mutex[thread_nb]);
						if (!print_error)
							print_error = 1;
					}
					
				} else {
					pthread_mutex_lock((next_job->cd)->m_mutex);
						(next_job->cd)->numerased++;
						if((next_job->cd)->numerased > (next_job->cd)->m)
							(next_job->cd)->error = write_error;
					pthread_mutex_unlock((next_job->cd)->m_mutex);
					*next_job->erased = 1;
					switch (mode) {
					case 0:
						// Initiate the hashing
                                        	SHA1_Init(next_job->hash);
						break;
					case 2:	
						// Update the block-file hash
						SHA1_Update(next_job->hash, next_job->data, next_job->blocksize);
						break;
					case 5: 
						// Increment the counter and free thread_data
						pthread_mutex_lock((next_job->cd)->m_mutex);
							(*(next_job->cd)->counter)--;
							pthread_cond_signal((next_job->cd)->m_cond);
						pthread_mutex_unlock((next_job->cd)->m_mutex);
						free(next_job->blockfile);
						free(next_job->error);
						free(next_job->hash);
						free(next_job->erased);
						free(next_job->dir_nb);
						break;
					}
				}
				if (write_error != -ETIMEDOUT) {
					next_job->next = NULL;
               				pthread_mutex_lock(fsdata->output_job_mutex);
               					if(fsdata->output_job_pool_first == NULL)
       	        					fsdata->output_job_pool_first = next_job;
        					else
               						(fsdata->output_job_pool_last)->next = next_job;
      						fsdata->output_job_pool_last = next_job;
               				pthread_mutex_unlock(fsdata->output_job_mutex);
					sem_post(fsdata->output_jobs_nb);
				} else {
					pthread_mutex_unlock(mutexp);
					ret = write_error;
				}
				write_error = 0;
			} else {
				switch (mode) {
				case 0:
					// Initiate the hashing
                                        SHA1_Init(next_job->hash);
					break;
				case 2:	
					// Update the block-file hash
					SHA1_Update(next_job->hash, next_job->data, next_job->blocksize);
					break;
				case 5: 
					// Increment the counter and free thread_data
					pthread_mutex_lock((next_job->cd)->m_mutex);
						(*(next_job->cd)->counter)--;
						pthread_cond_signal((next_job->cd)->m_cond);
					pthread_mutex_unlock((next_job->cd)->m_mutex);
					free(next_job->blockfile);
					free(next_job->error);
					free(next_job->hash);
					free(next_job->erased);
					free(next_job->dir_nb);
					break;
				}
				next_job->next = NULL;
               			pthread_mutex_lock(fsdata->output_job_mutex);
               				if(fsdata->output_job_pool_first == NULL)
       	        				fsdata->output_job_pool_first = next_job;
        				else
               					(fsdata->output_job_pool_last)->next = next_job;
      					fsdata->output_job_pool_last = next_job;
               			pthread_mutex_unlock(fsdata->output_job_mutex);
				sem_post(fsdata->output_jobs_nb);
			}
		} else {
			temp.tv_sec = time(NULL)+1;
                        // IDLE state
                        // Wait for the thread start signal from the main function
                        pthread_mutex_lock(fsdata->output_ctrlth_mutex[thread_nb]);
                        //pthread_cond_wait(fsdata->output_ctrlth_cond[thread_nb], fsdata->output_ctrlth_mutex[thread_nb]);
			pthread_cond_timedwait(fsdata->output_ctrlth_cond[thread_nb], fsdata->output_ctrlth_mutex[thread_nb],&temp);
                        pthread_mutex_unlock(fsdata->output_ctrlth_mutex[thread_nb]);
                }
        }
	free(arg);
	pthread_mutex_destroy(mutexp);
	free(mutexp);
	pthread_cond_destroy(condp);
	free(condp);

        // Exit from the thread and return the number of iterations
        pthread_exit((void*) ret);
}

void * input_ctrl_thread_function(void* arg) {
	
	struct fs_state * fsdata = ((struct ct_data *)arg)->fsdata;

	struct timespec temp = {0, 0};

	struct job * next_job = NULL;
	struct job * new_job = NULL;

        long int ret = 0;
	long int size, bsize;
	int k, m, w, psize, technb, blcknb;
	int thread_nb = ((struct ct_data *)arg)->nb;
	int read_error = 0;
	int timedout = 0;
	int dir_nb = thread_nb;

        char bhash[100];
	char fhash[100];
	char header[HEAD_LEN];
	char block_hash[BLOCK_HASH_LENGTH];
	unsigned char digest[SHA1_DIEGEST_LENGTH];
	int mode;
	/*
	char logname[MAX_PATH];
	sprintf (logname, "/scratchlocal/ecfs/logfile%d.txt", thread_nb);
	//sprintf (logname, "/home/tomasz/project_fs/project_dir/logfile%d.txt", thread_nb);
	FILE * logfile;
	logfile = fopen(logname, "w");
	if (logfile == NULL)
		ret = -errno;
	else
		ret = 1234;
	*/
	pthread_mutex_t * mutexp = malloc(sizeof(pthread_mutex_t));
        pthread_mutex_init (mutexp, NULL);

	pthread_cond_t * condp = malloc(sizeof(pthread_cond_t));
        pthread_cond_init (condp, NULL);
	
        while (1) {
                // Lock the queue for reading
                pthread_mutex_lock(fsdata->input_thread_queue_mutex[thread_nb]);
                	next_job = fsdata->input_thread_queue[thread_nb];

                	// Remove a job from the queue
                	if (fsdata->input_thread_queue[thread_nb] != NULL )
                        	fsdata->input_thread_queue[thread_nb] = (fsdata->input_thread_queue[thread_nb])->next;

                // Release the queue
                pthread_mutex_unlock(fsdata->input_thread_queue_mutex[thread_nb]);
		
                // If there is no job go to idle state, otherwise process the job 
                if (next_job != NULL) {
			if (next_job->mode == -1) {
                       	        free(next_job);
                       	        break;
             	 	}
			mode = next_job->mode;
			if (*next_job->erased == 0) {
				switch (next_job->mode) {
				case 0: // Open and read the data from a header
					*next_job->dir_nb = dir_nb;
					next_job->data = header;
					next_job->blocksize = HEAD_LEN;
					break;
				case 1: // Read 1 - use first buffer
					if(*next_job->block_nb < fsdata->k)
						next_job->data = (next_job->cd)->data[*next_job->block_nb];
					else
						next_job->data = (next_job->cd)->coding[*next_job->block_nb - fsdata->k];
					next_job->mode = 1;
					break;
				case 2: // Read 2 - use second buffer
					if(*next_job->block_nb < fsdata->k)
						next_job->data = (next_job->cd)->data2[*next_job->block_nb];
					else
						next_job->data = (next_job->cd)->coding2[*next_job->block_nb - fsdata->k];
					next_job->mode = 1;
					break;
				case 3: // Close
					next_job->mode = 2;
					break;
				case 4: // Open (as mode 0) in spare directory
					next_job->mode = 0;
					next_job->data = header;
					next_job->blocksize = HEAD_LEN;
					break;	
				case 5: // Change filename
					next_job->mode = 3;
					break;	
				case 6:	// Close (as 3) without SHA1 sum calc
					next_job->mode = 2;
					break;	
				}
	
				next_job->mutexp = mutexp;
				next_job->condp = condp;
				next_job->next = NULL;
				next_job->timedout = 0;
				next_job->state = 0;
				//fprintf(logfile, "next job: %s %d start: %ld \n", (next_job->cd)->path, mode, time(NULL));
				//fflush(logfile);
				if (!timedout) {
					//lock the IO-queue, add the job and signal to threads
					pthread_mutex_lock(fsdata->input_queue_mutex);
	     	        			if(fsdata->input_queue == NULL)
	     	        			        fsdata->input_queue = next_job;
	      	        			else
	      	        		     		fsdata->last_input_job->next = next_job;
	      	        		     	fsdata->last_input_job = next_job;
						pthread_cond_signal(fsdata->input_thread_cond);
      	        			pthread_mutex_unlock(fsdata->input_queue_mutex);
					//fprintf(logfile, "added to queue: %ld \n", time(NULL));
					//fflush(logfile);					
					// Wait for IO thread
					pthread_mutex_lock(mutexp);
					//do {
					read_error = 0;
					if(next_job->state) {
						read_error = *(next_job->error);
						*(next_job->error) = 0;
					} else {
						temp.tv_sec = time(NULL)+fsdata->itimeo;
						//fprintf(logfile, "start sync, max time: %ld \n", temp.tv_sec);
						//fflush(logfile);
						read_error = (-1) * pthread_cond_timedwait(condp, mutexp, &temp);
						if (read_error== -ETIMEDOUT) {
							next_job->timedout = 1;
						} else if(read_error == 0) {
							read_error = *(next_job->error);
							*(next_job->error) = 0;
						}
						//fprintf(logfile, "max time: %ld \n", temp.tv_sec);
						//fprintf(logfile, "end %ld \n", time(NULL));
						//fprintf(logfile, "dif %ld \n", (temp.tv_sec - time(NULL)));
						//fflush(logfile);
					}
					//} while (read_error == -ETIMEDOUT && error_count < fsdata->iret);
					if (read_error != -ETIMEDOUT)
						pthread_mutex_unlock(mutexp);
				} else {
					read_error = -1;
				}
				//fprintf(logfile, "stop at: %ld returned %d \n", time(NULL), read_error);
				//fflush(logfile);
				if (read_error) {
					if (mode == 0 || mode == 4) {
						pthread_mutex_lock(fsdata->input_dir_mutex);
						if ((next_job->cd)->input_spares < (fsdata->nbofdirs - fsdata->k - fsdata->m)) {
							*next_job->dir_nb = fsdata->k + fsdata->m + (next_job->cd)->input_spares;
							(next_job->cd)->input_spares++;
						} else {
							*next_job->erased = 1;
						}
						pthread_mutex_unlock(fsdata->input_dir_mutex);
						if (*next_job->erased == 0) {
							new_job = malloc(sizeof(struct job));
							new_job->next           = NULL;
	               					new_job->mode           = 4;
							new_job->blockfile	= next_job->blockfile;
        	       					new_job->hash           = next_job->hash;
        	       					new_job->error          = next_job->error;
							new_job->erased		= next_job->erased;
        	       					new_job->blocksize      = next_job->blocksize;
							new_job->cd		= next_job->cd;
							new_job->dir_nb		= next_job->dir_nb;
							new_job->block_nb	= next_job->block_nb;

							pthread_mutex_lock(fsdata->input_thread_queue_mutex[thread_nb]);
		               					new_job->next = fsdata->input_thread_queue[thread_nb];
		                       				fsdata->input_thread_queue[thread_nb] = new_job;
								if (new_job->next == NULL)
									fsdata->last_input_th_job[thread_nb] = new_job;
		               				pthread_mutex_unlock(fsdata->input_thread_queue_mutex[thread_nb]);
						} else {
							pthread_mutex_lock((next_job->cd)->m_mutex);
	                       					(*(next_job->cd)->counter)--;
								pthread_cond_signal((next_job->cd)->m_cond);	
	                        			pthread_mutex_unlock((next_job->cd)->m_mutex);
						}
					} else {
						pthread_mutex_lock((next_job->cd)->m_mutex);
							(next_job->cd)->erasures[(next_job->cd)->numerased] = *next_job->block_nb;
							(next_job->cd)->numerased++;
							(next_job->cd)->erasures[(next_job->cd)->numerased] = -1;
							if((next_job->cd)->numerased > (next_job->cd)->m)
								(next_job->cd)->error = read_error;
						pthread_mutex_unlock((next_job->cd)->m_mutex);
						*next_job->erased = 1;
						if (mode == 3 || mode == 6 || mode == 5) {
							free(next_job->blockfile);
							free(next_job->error);
							free(next_job->hash);
							free(next_job->erased);
							free(next_job->block_nb);
							free(next_job->dir_nb);
						}
						pthread_mutex_lock((next_job->cd)->m_mutex);
	                       				(*(next_job->cd)->counter)--;
							pthread_cond_signal((next_job->cd)->m_cond);	
	                        		pthread_mutex_unlock((next_job->cd)->m_mutex);
					}
					
					if (read_error != -ETIMEDOUT)
	       	                		free(next_job); 
					else
						pthread_mutex_unlock(mutexp);
					next_job = NULL;
					if (read_error == -ETIMEDOUT) {
						timedout = 1;
						ret = -ETIMEDOUT;
					}
					read_error = 0;
				} else {
					//pthread_mutex_unlock(mutexp);
					switch (mode) {
					case 0:
						if(sscanf(next_job->data, "%ld %d %d %d %d %d %ld %d %s %s",
						&size, &blcknb,  &k, &m, &w, &psize, &bsize, &technb, fhash, bhash)==10) {
	                                		if (k == (next_job->cd)->k && m == (next_job->cd)->m && 
							w == (next_job->cd)->w && psize == (next_job->cd)->packetsize && 
							bsize == (next_job->cd)->blocksize && technb == (int)(next_job->cd)->tech) {
								// Initiate the hashing
								pthread_mutex_lock((next_job->cd)->m_mutex);
									if ((next_job->cd)->size == -1) {
		                                                		memcpy((next_job->cd)->file_hash, fhash, 100);
	                                               				(next_job->cd)->size = size;
									}
	                        				pthread_mutex_unlock((next_job->cd)->m_mutex);
								memcpy((next_job->cd)->block_hash[blcknb], bhash, 100);
								(next_job->cd)->erased[blcknb] = 0;
								*next_job->block_nb = blcknb;
								SHA1_Init(next_job->hash);
								next_job->data = NULL;
								// Increment the counter and free thread_data
								pthread_mutex_lock((next_job->cd)->m_mutex);
	                       						(*(next_job->cd)->counter)--;
									pthread_cond_signal((next_job->cd)->m_cond);	
	                        				pthread_mutex_unlock((next_job->cd)->m_mutex);
								break;
							}
						}
						*next_job->erased = 1;
						// Increment the counter and free thread_data
						pthread_mutex_lock((next_job->cd)->m_mutex);
	                       				(*(next_job->cd)->counter)--;
							pthread_cond_signal((next_job->cd)->m_cond);	
	                        		pthread_mutex_unlock((next_job->cd)->m_mutex);
						break;
					case 1:	
						// Update the block-file hash
						SHA1_Update(next_job->hash, next_job->data, next_job->blocksize);
						// Increment the counter and free thread_data
						pthread_mutex_lock((next_job->cd)->m_mutex);
	                       				(*(next_job->cd)->counter)--;
							pthread_cond_signal((next_job->cd)->m_cond);	
	                        		pthread_mutex_unlock((next_job->cd)->m_mutex);
						break;
					case 2:
						// Update the block-file hash
						SHA1_Update(next_job->hash, next_job->data, next_job->blocksize);
						// Increment the counter and free thread_data
						pthread_mutex_lock((next_job->cd)->m_mutex);
	                       				(*(next_job->cd)->counter)--;
							pthread_cond_signal((next_job->cd)->m_cond);	
	                        		pthread_mutex_unlock((next_job->cd)->m_mutex);
						break;
					case 3: 
						sha1_finalize_t(next_job->hash, block_hash);
						if (strcmp((next_job->cd)->block_hash[*next_job->block_nb], block_hash) != 0) {
							new_job = malloc(sizeof(struct job));
							new_job->next           = NULL;
	               					new_job->mode           = 5;
							new_job->blockfile	= next_job->blockfile;
        	       					new_job->hash           = next_job->hash;
        	       					new_job->error          = next_job->error;
							new_job->erased		= next_job->erased;
        	       					new_job->blocksize      = next_job->blocksize;
							new_job->cd		= next_job->cd;
							new_job->dir_nb		= next_job->dir_nb;
							new_job->block_nb	= next_job->block_nb;
							
							pthread_mutex_lock(fsdata->input_thread_queue_mutex[thread_nb]);
		               					new_job->next = fsdata->input_thread_queue[thread_nb];
		                       				fsdata->input_thread_queue[thread_nb] = new_job;
								if (new_job->next == NULL)
									fsdata->last_input_th_job[thread_nb] = new_job;
		               				pthread_mutex_unlock(fsdata->input_thread_queue_mutex[thread_nb]);
							break;
						}
						free(next_job->blockfile);
						free(next_job->error);
						free(next_job->hash);
						free(next_job->erased);
						free(next_job->block_nb);
						free(next_job->dir_nb);
						// Increment the counter and free thread_data
						pthread_mutex_lock((next_job->cd)->m_mutex);
	                       				(*(next_job->cd)->counter)--;
							pthread_cond_signal((next_job->cd)->m_cond);	
	                        		pthread_mutex_unlock((next_job->cd)->m_mutex);
						break;
					case 4:
						if(sscanf(next_job->data, "%ld %d %d %d %d %d %ld %d %s %s",
						&size, &blcknb,  &k, &m, &w, &psize, &bsize, &technb, fhash, bhash)==10) {
	                                		if (k == (next_job->cd)->k && m == (next_job->cd)->m && 
							w == (next_job->cd)->w && psize == (next_job->cd)->packetsize && 
							bsize == (next_job->cd)->blocksize && technb == (int)(next_job->cd)->tech) {
								// Initiate the hashing
								pthread_mutex_lock((next_job->cd)->m_mutex);
								if ((next_job->cd)->size == -1) {
		                                                	memcpy((next_job->cd)->file_hash, fhash, 100);
	                                               			(next_job->cd)->size = size;
								}
	                        				pthread_mutex_unlock((next_job->cd)->m_mutex);
								memcpy((next_job->cd)->block_hash[blcknb], bhash, 100);
								(next_job->cd)->erased[blcknb] = 0;
								*next_job->block_nb = blcknb;
								SHA1_Init(next_job->hash);
								next_job->data = NULL;
								// Increment the counter and free thread_data
								pthread_mutex_lock((next_job->cd)->m_mutex);
	                       						(*(next_job->cd)->counter)--;
									pthread_cond_signal((next_job->cd)->m_cond);	
	                        				pthread_mutex_unlock((next_job->cd)->m_mutex);
								break;
							}
						}
						*next_job->erased = 1;
						// Increment the counter and free thread_data
						pthread_mutex_lock((next_job->cd)->m_mutex);
	                       				(*(next_job->cd)->counter)--;
							pthread_cond_signal((next_job->cd)->m_cond);	
	                        		pthread_mutex_unlock((next_job->cd)->m_mutex);
						break;
					case 5:
						SHA1_Final(digest, next_job->hash);
						free(next_job->blockfile);
						free(next_job->error);
						free(next_job->hash);
						free(next_job->erased);
						free(next_job->block_nb);
						free(next_job->dir_nb);
						// Increment the counter and free thread_data
						pthread_mutex_lock((next_job->cd)->m_mutex);
	                       				(*(next_job->cd)->counter)--;
							pthread_cond_signal((next_job->cd)->m_cond);	
	                        		pthread_mutex_unlock((next_job->cd)->m_mutex);
						break;
					case 6:
						sha1_finalize_t(next_job->hash, block_hash);
						free(next_job->blockfile);
						free(next_job->error);
						free(next_job->hash);
						free(next_job->erased);
						free(next_job->block_nb);
						free(next_job->dir_nb);
						// Increment the counter and free thread_data
						pthread_mutex_lock((next_job->cd)->m_mutex);
	                       				(*(next_job->cd)->counter)--;
							pthread_cond_signal((next_job->cd)->m_cond);	
	                        		pthread_mutex_unlock((next_job->cd)->m_mutex);
						break;
					}
					

	       	                	free(next_job); 
					next_job = NULL;
				}
			} else {
				//ret = fprintf(logfile, "%s %d erased\n", (next_job->cd)->path, mode);
				//fflush(logfile);
				if (mode == 3 || mode == 6 || mode == 5) {
					free(next_job->blockfile);
					free(next_job->error);
					free(next_job->hash);
					free(next_job->erased);
					free(next_job->block_nb);
					free(next_job->dir_nb);
				}
				// Increment the counter and free thread_data
				pthread_mutex_lock((next_job->cd)->m_mutex);
                       			(*(next_job->cd)->counter)--;
					pthread_cond_signal((next_job->cd)->m_cond);	
                        	pthread_mutex_unlock((next_job->cd)->m_mutex);
       	                	free(next_job); 
				next_job = NULL;	
			}
		} else {
                        /* IDLE state */
                        /* Wait for the thread start signal from the main function*/
			temp.tv_sec = time(NULL)+1;
                        pthread_mutex_lock(fsdata->input_ctrlth_mutex[thread_nb]);
                        //pthread_cond_wait(fsdata->input_ctrlth_cond[thread_nb], fsdata->input_ctrlth_mutex[thread_nb]);
			pthread_cond_timedwait(fsdata->input_ctrlth_cond[thread_nb], fsdata->input_ctrlth_mutex[thread_nb], &temp);
                        pthread_mutex_unlock(fsdata->input_ctrlth_mutex[thread_nb]);
                }
        }
	//fclose(logfile);
	pthread_mutex_destroy(mutexp);
	free(mutexp);
	pthread_cond_destroy(condp);
	free(condp);
	free(arg);
        /* Exit from the thread and return the number of iterations */
        pthread_exit((void*) ret);
}
/**
 * Set parameters inside the Coding_data structure
 */
void set_input_cd(struct coding_data * ds, struct fs_state * input_struct,const char* path) {
        int i;

        ds->tech                = input_struct->tech;   
        ds->k                   = input_struct->k;
        ds->m                   = input_struct->m;
        ds->w                   = input_struct->w;
        ds->packetsize          = input_struct->packetsize;
        ds->blocksize           = input_struct->blocksize;
        ds->buffersize          = input_struct->buffersize;
        
        ds->matrix              = input_struct->matrix;
        ds->bitmatrix           = input_struct->bitmatrix;
        ds->schedule            = input_struct->schedule;
        
        ds->numerased           = 0;
        ds->size                = -1;
        ds->mode                = -1;
        ds->buf_index           = ds->buffersize;
        ds->mod                 = 0;

	ds->path		= malloc(sizeof(char) * MAX_PATH);
	strncpy(ds->path, path, MAX_PATH);
        ds->data                = malloc(sizeof(char*) * input_struct->k);
        ds->data2               = malloc(sizeof(char*) * input_struct->k);
        ds->coding              = malloc(sizeof(char*) * input_struct->m);
        for (i=0; i<ds->m; i++)
                ds->coding[i]   = malloc(sizeof(char)*input_struct->blocksize);
        ds->coding2             = malloc(sizeof(char*)*input_struct->m);
        for (i=0; i<ds->m; i++)
                ds->coding2[i]  = malloc(sizeof(char)*input_struct->blocksize);
        ds->block               = malloc(sizeof(char) * input_struct->buffersize);
        ds->block2              = malloc(sizeof(char) * input_struct->buffersize);
        ds->bptr                = ds->block;
        ds->erasures            = malloc(sizeof(int) * (input_struct->k + input_struct->m+1));
        ds->erasures[0]         = -1;
        ds->td_array            = malloc(sizeof(struct thread_data)*(input_struct->k+input_struct->m+1));
        ds->file_hash           = malloc(sizeof(char)*100);
        ds->block_hash          = malloc(sizeof(char*)*(input_struct->k+input_struct->m));
        ds->counter         	= NULL;
	ds->input_spares	= 0;

	ds->erased		= malloc(sizeof(int) * (input_struct->k + input_struct->m));

	ds->m_mutex = malloc(sizeof(pthread_mutex_t));
        pthread_mutex_init (ds->m_mutex, NULL);

	ds->m_cond = malloc(sizeof(pthread_cond_t));
        pthread_cond_init (ds->m_cond, NULL);

        for (i=0; i < (input_struct->k + input_struct->m); i++) {
		ds->erased[i] 			= 1;
                ds->block_hash[i] 		= malloc(sizeof(char)*100);
                ds->td_array[i].blockfile 	= malloc(sizeof(int));
		*ds->td_array[i].blockfile 	= -1;
                ds->td_array[i].error 		= malloc(sizeof(int));
		*ds->td_array[i].error 		= 0;
		ds->td_array[i].hash 		= malloc(sizeof(SHA_CTX));
                ds->td_array[i].erased 		= malloc(sizeof(int));
		*ds->td_array[i].erased 	= 0;
		ds->td_array[i].block_nb 	= malloc(sizeof(int));
		ds->td_array[i].dir_nb 		= malloc(sizeof(int));
                if (i<input_struct->k) { 
                        ds->data[i] 		= ds->block+(i*input_struct->blocksize);
                        ds->data2[i]		= ds->block2+(i*input_struct->blocksize);
                }             

        }
        ds->counter = malloc(sizeof(int));
        *ds->counter = ds->k+ds->m;
	ds->error = 0;
};

/**
 * Set parameters inside the Coding_data structure
 */
void set_output_cd(struct coding_data * ds, struct fs_state * input_struct,const char* path) {
        int i;

        ds->tech                = input_struct->tech;
        ds->k                   = input_struct->k;
        ds->m                   = input_struct->m;
        ds->w                   = input_struct->w;
        ds->packetsize          = input_struct->packetsize;
        ds->blocksize           = input_struct->blocksize;
        ds->buffersize          = input_struct->buffersize;

        ds->matrix              = input_struct->matrix;
        ds->bitmatrix           = input_struct->bitmatrix;
        ds->schedule            = input_struct->schedule;

        ds->numerased           = 0;
        ds->size                = 0;
        ds->mode                = -1;
        ds->buf_index           = 0;

        ds->mod                 = 0;

	ds->path		= malloc(sizeof(char) * MAX_PATH);
	strncpy(ds->path, path, MAX_PATH);
        ds->data                = malloc(sizeof(char*)*input_struct->k);
        ds->coding              = malloc(sizeof(char*)*input_struct->m);
        for (i = 0; i < input_struct->m; i++)
                ds->coding[i]   = malloc(sizeof(char)*input_struct->blocksize);
        ds->block               = malloc(sizeof(char)*input_struct->buffersize);
        ds->bptr                = ds->block;
        ds->erasures            = malloc(sizeof(int)*(input_struct->k+input_struct->m+1));
        ds->erasures[0]         = -1;
        ds->td_array            = malloc(sizeof(struct thread_data)*(input_struct->k+input_struct->m));
        ds->file_hash           = malloc(sizeof(char)*100);
        ds->block_hash          = malloc(sizeof(char*)*(input_struct->k+input_struct->m));
        ds->counter         = NULL;

	ds->m_mutex = malloc(sizeof(pthread_mutex_t));
        pthread_mutex_init (ds->m_mutex, NULL);

	ds->m_cond = malloc(sizeof(pthread_cond_t));
        pthread_cond_init (ds->m_cond, NULL);

        for (i = 0; i < input_struct->k + input_struct->m; i++) {
                ds->td_array[i].blockfile 	= malloc(sizeof(int));
		*ds->td_array[i].blockfile	= -1;
                ds->td_array[i].error 		= malloc(sizeof(int));
		*ds->td_array[i].error 		= 0;
		ds->td_array[i].hash 		= malloc(sizeof(SHA_CTX));
                ds->td_array[i].erased 		= malloc(sizeof(int));
		*ds->td_array[i].erased 	= 0;
		ds->td_array[i].dir_nb 		= malloc(sizeof(int));
                if (i<input_struct->k)
                        ds->data[i] 		= ds->block+(i*input_struct->blocksize);
        }
        ds->counter = malloc(sizeof(int));
        *ds->counter = ds->k+ds->m;
	ds->error = 0;
};

/** Clear the structure */
void free_output_cd(struct coding_data * ds) {
        int i;
	free(ds->counter);
	pthread_mutex_destroy(ds->m_mutex);
	pthread_cond_destroy(ds->m_cond);
	free(ds->m_mutex);
	free(ds->m_cond);
        for (i = 0; i < ds->m; i++){
                free(ds->coding[i]);
        }       
        free(ds->coding);
        free(ds->block_hash);
	free(ds->file_hash);
        free(ds->td_array);
        free(ds->data);
        free(ds->erasures);
        free(ds->block);
	free(ds->path);
        free(ds);
};

void free_input_cd(struct coding_data * ds) {
        int i;
 	for (i=0; i < (ds->k + ds->m); i++)
               free(ds->block_hash[i]);
	pthread_mutex_destroy(ds->m_mutex);
	pthread_cond_destroy(ds->m_cond);
	free(ds->m_mutex);
	free(ds->m_cond);
        for (i = 0; i < ds->m; i++){
                free(ds->coding[i]);
                free(ds->coding2[i]);
        }       
        free(ds->coding);
        free(ds->coding2);
        free(ds->block_hash);
	free(ds->file_hash);
        free(ds->td_array);
        free(ds->data);
        free(ds->data2);
        free(ds->erasures);
        free(ds->counter);
        free(ds->block);
        free(ds->block2);
	free(ds->path);
	free(ds->erased);
        free(ds);
};

/** Make the jobs 
 * Function fills the queue with the jobs. If a blockfile is erased, it appends the job_counter and sends no job to the queue.
 */
void add_output_jobs(int k, int m, struct thread_data * td, char ** data, char ** coding, size_t blocksize, int mode, struct coding_data * cd) {
	int jobs_now;
	sem_getvalue(FS_DATA->output_jobs_nb, &jobs_now);
	//log_msg("SEM VAL: %d\n", jobs_now);
        struct job * new_job;
        int i;
        /* Goes through the data blocks */
        for (i=0; i<k; i++) {
		sem_wait(FS_DATA->output_jobs_nb);

		pthread_mutex_lock(FS_DATA->output_job_mutex);
                	new_job = FS_DATA->output_job_pool_first;
			FS_DATA->output_job_pool_first = new_job->next;
		pthread_mutex_unlock(FS_DATA->output_job_mutex);

		memcpy(new_job->data, data[i], blocksize);
                new_job->next           = NULL;
                new_job->blockfile      = td[i].blockfile;
                new_job->hash           = td[i].hash;
                new_job->error          = td[i].error;
		new_job->erased		= td[i].erased;
		new_job->dir_nb		= td[i].dir_nb;
                new_job->blocksize      = blocksize;
                new_job->mode           = mode;
		new_job->cd		= cd;
                /* Lock the queue */
                pthread_mutex_lock(FS_DATA->output_thread_queue_mutex[i]);
                if(FS_DATA->output_thread_queue[i] == NULL)
                       FS_DATA->output_thread_queue[i] = new_job;
                else
                       FS_DATA->last_output_th_job[i]->next = new_job;
                /* Set the current job pointer to newly added job */
                FS_DATA->last_output_th_job[i] = new_job;
                pthread_mutex_unlock(FS_DATA->output_thread_queue_mutex[i]);
		/* Signal thread_start */
                pthread_cond_signal(FS_DATA->output_ctrlth_cond[i]);
        }
        /* Goes through the coding blocks */
        for (i=k; i<k+m; i++) {
                sem_wait(FS_DATA->output_jobs_nb);

       		pthread_mutex_lock(FS_DATA->output_job_mutex);
                	new_job = FS_DATA->output_job_pool_first;
			FS_DATA->output_job_pool_first = new_job->next;
		pthread_mutex_unlock(FS_DATA->output_job_mutex);

                new_job->next           = NULL;
		memcpy(new_job->data, coding[i-k], blocksize);
                new_job->blockfile      = td[i].blockfile;
                new_job->hash           = td[i].hash;
                new_job->error          = td[i].error;
		new_job->erased		= td[i].erased;
		new_job->dir_nb		= td[i].dir_nb;
                new_job->blocksize      = blocksize;
                new_job->mode           = mode;
		new_job->cd		= cd;
                /* Lock the queue */
                pthread_mutex_lock(FS_DATA->output_thread_queue_mutex[i]);
                if(FS_DATA->output_thread_queue[i] == NULL)
        	        FS_DATA->output_thread_queue[i] = new_job;
	        else
                	FS_DATA->last_output_th_job[i]->next = new_job;
                        /* Set the current job pointer to newly added job */
               	FS_DATA->last_output_th_job[i] = new_job;
                pthread_mutex_unlock(FS_DATA->output_thread_queue_mutex[i]);
                /* Signal thread_start */
                pthread_cond_signal(FS_DATA->output_ctrlth_cond[i]);
        }
};


void add_input_jobs(int nb, struct thread_data * td, size_t blocksize, int mode, struct coding_data * cd) {
        struct job * new_job;
        int i;
        /* Goes through the data blocks */
        for (i = 0; i < nb; i++) {	
		new_job = (struct job *)malloc(sizeof(struct job));
                new_job->next           = NULL;
                new_job->blockfile      = td[i].blockfile;
                new_job->hash           = td[i].hash;
                new_job->error          = td[i].error;
		new_job->erased		= td[i].erased;
		new_job->block_nb	= td[i].block_nb;
		new_job->dir_nb		= td[i].dir_nb;
                new_job->blocksize      = blocksize;
                new_job->mode           = mode;
		new_job->cd		= cd;
                /* Lock the queue */
                pthread_mutex_lock(FS_DATA->input_thread_queue_mutex[i]);
                if(FS_DATA->input_thread_queue[i] == NULL)
                       FS_DATA->input_thread_queue[i] = new_job;
                else
                       FS_DATA->last_input_th_job[i]->next = new_job;
                /* Set the current job pointer to newly added job */
                FS_DATA->last_input_th_job[i] = new_job;
                pthread_mutex_unlock(FS_DATA->input_thread_queue_mutex[i]);
		/* Signal thread_start */
                pthread_cond_signal(FS_DATA->input_ctrlth_cond[i]);
        }        
};

/** Job synchornization
 * Function waits until a precise number of jobs froma a single IO function is done. Each IO function, such as read() or write() operates on a different 
 * job_counter. 
 */


int synchronize_jobs(int * counter, pthread_cond_t * m_condp, pthread_mutex_t * m_mutexp, int th, int mode) {
        int i=1000;
	struct timespec temp = {0, 0};
        pthread_mutex_lock(m_mutexp);
        while(*counter>0) {
                temp.tv_sec = time(NULL)+10;
                if(pthread_cond_timedwait(m_condp, m_mutexp, &temp)==ETIMEDOUT) {
                       log_msg("mode: %d etimedout %ld\n",mode, temp.tv_sec);
                }
                i--;
                if (i==0) break;
        }
	*counter = th;
        pthread_mutex_unlock(m_mutexp);
        if (i==0)
                return -1;
        else
                return 0;
	return 0;
};

/** Decoding
 * This points to the functions from Jerasure library.
 */
int decode(enum Coding_Technique tech, int k, int m, int w, int * matrix, int * bitmatrix, int *erasures, char ** data, char ** coding, size_t blocksize, int packetsize) {
        int rc = 0;
        if (tech == Reed_Sol_Van || tech == Reed_Sol_R6_Op)
                rc = jerasure_matrix_decode(k, m, w, matrix, 1, erasures, data, coding, blocksize);
        else 
                rc = jerasure_schedule_decode_lazy(k, m, w, bitmatrix, erasures, data, coding, blocksize, packetsize, 1);
        return rc;      
};

/** Encoding
 * from Jerasure library.
 */
void encode(enum Coding_Technique tech, int k, int m, int w, int * matrix, int ** schedule, char ** data, char ** coding, size_t blocksize, int packetsize) {
        switch(tech) {      
                case Reed_Sol_Van:
                        jerasure_matrix_encode(k, m, w, matrix, data, coding, blocksize);
                        break;
                case Reed_Sol_R6_Op:
                        reed_sol_r6_encode(k, w, data, coding, blocksize);
                        break;
                case Cauchy_Orig:
                        jerasure_schedule_encode(k, m, w, schedule, data, coding, blocksize, packetsize);
                        break;
                case Cauchy_Good:
                        jerasure_schedule_encode(k, m, w, schedule, data, coding, blocksize, packetsize);
                        break;
                case Liberation:
                        jerasure_schedule_encode(k, m, w, schedule, data, coding, blocksize, packetsize);
                        break;
                case Blaum_Roth:
                        jerasure_schedule_encode(k, m, w, schedule, data, coding, blocksize, packetsize);
                        break;
                case Liber8tion:
                        jerasure_schedule_encode(k, m, w, schedule, data, coding, blocksize, packetsize);
                        break;
        }
};

/****************************************************************************************
 *                                      FUSE functions                                  *
 ****************************************************************************************/


/** Get file attributes */
int fs_getattr(const char *path, struct stat *statbuf)
{
        int retstat = -ENOENT;
        int i;
        char fdir[PATH_MAX];
        size_t size=0;
        int fd;
        char header[HEAD_LEN];
        int block_nb;
        char block_hash[100];
        int errread = 0;
        log_msg("fs_getattr path=\"%s\"\n", path);

        memset(statbuf, 0, sizeof(struct stat));
        for (i = 0; i < FS_DATA->m; i++) {
		log_msg("fs_getattr path=\"%s\" dir %d \n",path, i);
                sprintf(fdir, "%s%s_block", FS_DATA->dir[i], path);
                fd = open(fdir, O_RDONLY);
                if (fd == -1) {
                        sprintf(fdir, "%s%s",FS_DATA->dir[i], path);
			//fd = open(fdir, O_RDONLY);
                        if(stat(fdir, statbuf) ==-1)  {
				retstat = -errno;
				log_msg("fs_getattr path=\"%s\" ERROR stat file %s returned %d \n",path, fdir, -errno);
                                //break;
                        } else  {
                                retstat = 0;
                                //break;
                        }
                } else {
			fstat(fd, statbuf);
			statbuf->st_nlink = 1;
			
                        if(read_lock(fd, &retstat)<0) {
                                log_msg("fs_getattr path=\"%s\" ERROR getlock %s \n",path, fdir); 
                                if(close(fd) == -1)
                                log_msg("fs_getattr path=\"%s\" ERROR close file %s returned %d \n",path, fdir, -errno);
                                //break;
                        } else {
                                blockfile_read(header, HEAD_LEN, &fd, &errread);
                                if(errread < 0) {
                                        log_msg("fs_getattr path=\"%s\" ERROR read file %s returned %d \n",path, fdir, errread);
                                        retstat = errread;
                                        if(close(fd) == -1)
                                log_msg("fs_getattr path=\"%s\" ERROR close file %s returned %d \n",path, fdir, -errno);
                                        //break;
                                } else {
                                        if(sscanf(header, "%ld %d %*d %*d %*d %*d %*d %*d %s", &size, &block_nb, block_hash) != 3) {
                                                log_msg("fs_getattr path=\"%s\" ERROR sscanf file %s returned %d \n",path, fdir, -errno);
                                                //retstat = -EIO;
						retstat = 0;
                                                if(close(fd) == -1)
                                log_msg("fs_getattr path=\"%s\" ERROR close file %s returned %d \n",path, fdir, -errno);
                                                //break;
                                        } else {
                                                //memset(statbuf, 0, sizeof(struct stat));
                                                //fstat(fd, statbuf);
                                                //statbuf->st_mode = S_IFREG | 0444;
                                                //statbuf->st_nlink = 1;
                                                statbuf->st_size = size;
                                                retstat = 0;
                                                if(close(fd) == -1)
                                log_msg("fs_getattr path=\"%s\" ERROR close file %s returned %d \n",path, fdir, -errno);
                                                //break;
                                        }
                                }
                        }
			//statbuf->st_mode = S_IFREG | 0777;
			 statbuf->st_mode = S_IFREG | 0751;
        statbuf->st_uid = 1013;
        statbuf->st_gid = 1000;
                } 
		break;                           
        }
        log_msg("fs_getattr path=\"%s\" returned %d\n", path, retstat);
        return retstat;
}

/** Read the target of a symbolic link */
int fs_readlink(const char *path, char *link, size_t size)
{
        log_msg("fs_readlink path=\"%s\" no ret\n", path);
        return -EPERM;
}

/** Create a file node */
int fs_mknod(const char *path, mode_t mode, dev_t dev)
{
        log_msg("fs_mknod path=\"%s\"  no ret\n", path);
        return -EPERM;
}

/** Create a directory */
int fs_mkdir(const char *path, mode_t mode)
{
        char fdir[PATH_MAX];
        int i;
        int rc = 0;
        int ret = 0;
        log_msg("fs_mkdir path=\"%s\"\n", path);
        for (i = 0; i < FS_DATA->nbofdirs; i++)
        {
                sprintf(fdir, "%s%s", FS_DATA->dir[i], path);
                if(mkdir(fdir, S_IRWXU | S_IRWXG | S_IRWXO) == -1)
                        log_msg("fs_mkdir path=\"%s\" ERROR mkdir %s returned %d\n", path, fdir, -errno);
                else
                        rc++;
                        
        }
        if (rc < FS_DATA->k)
                ret = -errno; 
        //log_msg("fs_mkdir path=\"%s\" returned %d\n", path, ret);
        return ret;
}

/** Remove a file 
 *
 * Function looks into each coding directory and removes all meta- and block-files
 * with matching name. If no files were removed function returns errno from last unlink()
 */
int fs_unlink(const char *path)
{
        int retstat;
        int retcount = 0;
        int i;
        char fpath[PATH_MAX];

        log_msg("fs_unlink path=\"%s\"\n", path);

        for (i = 0; i < FS_DATA->nbofdirs; i++) {
                sprintf(fpath, "%s%s_block", FS_DATA->dir[i], path);
                if (!unlink(fpath))
                        retcount++;  
                //else
                        //log_msg("fs_unlink path=\"%s\" ERROR unlink %s returned %d\n", path, fpath, -errno);    
                sprintf(fpath, "%s%s_block_R", FS_DATA->dir[i], path);
                if (!unlink(fpath))
                        retcount++;  
                //else
                        //log_msg("fs_unlink path=\"%s\" ERROR unlink %s returned %d\n", path, fpath, -errno);         
        }

        if (retcount == 0)
                retstat = -errno;
        else    
                retstat = 0;   
        //log_msg("fs_unlink path=\"%s\" returned %d\n", path, retstat);
        return retstat;
}

/** Remove a directory */
int fs_rmdir(const char *path)
{
        char fdir[PATH_MAX];
        char fname[PATH_MAX];
        int i;
        int rc = 0;
        int ret = 0;
        DIR *dp;
        struct dirent *de;
        log_msg("fs_rmdir path=\"%s\" \n", path);
        /* If forced_remove (frm) parameter is set, remove also the content of removed directory */
        if(FS_DATA->frm) {
                for (i = 0; i < FS_DATA->nbofdirs; i++)
                {
                        sprintf(fdir, "%s%s", FS_DATA->dir[i], path);
                        dp = opendir(fdir);
                        if(dp == NULL) continue;
                        while ((de = readdir(dp)) != NULL){
                                sprintf(fname, "%s%s/%s", FS_DATA->dir[i], path, de->d_name);
                                unlink(fname);
                        }
                        closedir(dp);
                        if(!rmdir(fdir))
                                rc++;
                }
        } else {
                for (i = 0; i < FS_DATA->nbofdirs; i++)
                {
                        sprintf(fdir, "%s%s", FS_DATA->dir[i], path);
                        if(!rmdir(fdir))
                                rc++;
                }
        }
        if(rc == 0) ret = -errno;
        //log_msg("fs_rmdir path=\"%s\" returned %d\n", path, ret);
        return ret;
}

/** Create a symbolic link */
int fs_symlink(const char *path, const char *link)
{
        log_msg("fs_symlink path=\"%s\" \n", path);
        return -EPERM;
}

/** Rename a file */
int fs_rename(const char *path, const char *newpath)
{
        log_msg("fs_rename path=\"%s\" \n", path);
        return -EPERM;
}

/** Create a hard link to a file */
int fs_link(const char *path, const char *newpath)
{
        log_msg("fs_link path=\"%s\" \n", path);
        return -EPERM;
}

/** Change the permission bits of a file */
int fs_chmod(const char *path, mode_t mode)
{
        log_msg("fs_chmod path=\"%s\" \n", path);
        return -EPERM;
}

/** Change the owner and group of a file */
int fs_chown(const char *path, uid_t uid, gid_t gid)
{
        log_msg("fs_chown(path=\"%s\" \n", path);
        return -EPERM;
}

/** Change the size of a file */
int fs_truncate(const char *path, off_t newsize)
{
        log_msg("fs_truncate path=\"%s\" \n", path);
        return -EPERM;
}

/** Change the access and/or modification times of a file */
/* note -- I'll want to change this as soon as 2.6 is in debian testing */
int fs_utime(const char *path, struct utimbuf *ubuf)
{
        log_msg("fs_utime path=\"%s\" \n", path);
        return -EPERM;
}

/** File open operation
 *
 * Function allocates memory for a coding_data structure and fills in the file pointers. 
 * Pointer to the structure is passed to all followiing FUSE operations by fi->fh field.
 * Function checks if the meta-file of a block-file can be accessed. If not, the block-file remains closed. This is to
 * avoid reading a file, if the writing is not finished. The meta-file is generated after the end of writing. If
 * there are not enough block-files which can be opened, a proper error code is returned. At the end function
 * initiates the hashing.
 */
int fs_open(const char *path, struct fuse_file_info *fi)
{
	log_msg("fs_open path=%s \n", path);

	// If a file is opened for something else than reading, return an error
        if((fi->flags & 3) != O_RDONLY )
                return -EACCES;

	int retstat = 0;
	int i;

	struct coding_data * cd = malloc(sizeof(struct coding_data));;

        set_input_cd(cd, FS_DATA, path);
 
        add_input_jobs(cd->k + cd->m, cd->td_array, HEAD_LEN, 0,  cd);
        synchronize_jobs(cd->counter, cd->m_cond, cd->m_mutex, cd->k+cd->m, 0);

	for (i = 0; i < cd->k + cd->m; i++) {
		if (cd->erased[i]) {
			cd->erasures[cd->numerased] = i;
			cd->numerased++;
		}
	}
	cd->erasures[cd->numerased] = -1;

        /* If all files are opened erase one file (Must be done for following coding methods) */
        if (cd->numerased == 0 && (cd->tech == Cauchy_Orig || cd->tech ==  Cauchy_Good || cd->tech == Liberation || cd->tech == Blaum_Roth || cd->tech == Liber8tion)) {
                cd->erasures[cd->numerased] = 0;
                cd->erasures[cd->numerased+1] = -1;
        }
        if(cd->numerased>cd->m) {
        	add_input_jobs(cd->k + cd->m, cd->td_array, HEAD_LEN, 6,  cd);
        	synchronize_jobs(cd->counter, cd->m_cond, cd->m_mutex, cd->k+cd->m, 6);
                free_input_cd(cd);
		retstat = -EIO;
        } else {
                /* Initiate the SHA1 algorithm for the whole file */
                SHA1_Init(&cd->hash);
		fi->fh = (uint64_t)cd;
                add_input_jobs(cd->k + cd->m, cd->td_array, cd->blocksize, 1,  cd) ;          
        }
        /* Set the file-handler in fuse_file_info structure to coding_data structure pointer */
          
        log_msg("fs_open path=%s returned %d\n", path, retstat);
        return retstat;
}

/** Read the data from an open file
 *
 ** Read should return exactly the number of bytes requested except     *
 ** on EOF or error, otherwise the rest of the data will be             *
 ** substituted with zeroes.  An exception to this is when the          *
 ** 'direct_io' mount option is specified, in which case the return     *
 ** value of the read system call will reflect the return value of      *
 ** this operation.                                                     *
 ** Changed in version 2.2                                              *
 *
 * Function creates threads to read data simultaneously from all opened files. Filling the coding buffer 
 * triggers the encoding of the data. After encoding system buffer is filled with small portions of encoded
 * data, until all is passed. If an error occurs during operation it is ignored unless enough files are accessible.
 * If not, EIO is returned. Function updates SHA1 hash for the whole file. 
 */
int fs_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
        //log_msg("\nfs_read(path=\"%s\", buf=0x%08x, size=%d, offset=%lld, fi=0x%08x)\n", path, buf, size, offset, fi);

        int buf_offset = 0;
        int retstat = 0;

	struct coding_data * cd = (struct coding_data *)fi->fh;

	cd->mode = 1;
        // If the offset is bigger than file size, finish reading
        if (offset >= cd->size) {
		cd->mode = 3;
                return 0;
	}
        if (cd->buf_index>=cd->buffersize) {
                synchronize_jobs(cd->counter, cd->m_cond, cd->m_mutex, cd->k+cd->m, 1);
		error_check(cd, &retstat);
		if (retstat != 0) {
			log_msg("fs_read path=\"%s\" ERROR erasures\n", path);
			cd->mode = 2;
			return retstat;
		}
         	if(!cd->mod) {
         	      	if (decode(cd->tech, cd->k, cd->m,cd->w, cd->matrix, cd->bitmatrix, cd->erasures, cd->data, cd->coding, cd->blocksize, cd->packetsize)) {
         			log_msg("fs_read path=\"%s\" ERROR decode - read finished, mode set to 3\n", path);
				cd->mode = 2;
				return -EIO;	
      	        	}
			if (cd->size - offset > cd->buffersize)
				add_input_jobs(cd->k + cd->m, cd->td_array, cd->blocksize, 2, cd) ;
				
       		        cd->mod = 1;
			cd->bptr = cd->block;
		} else {
        		if (decode(cd->tech, cd->k, cd->m,cd->w, cd->matrix, cd->bitmatrix, cd->erasures, cd->data2, cd->coding2, cd->blocksize, cd->packetsize)) {
				log_msg("fs_read path=\"%s\" ERROR decode - read finished, mode set to 3\n", path);
				cd->mode = 2;
				return -EIO;
			}
			if (cd->size - offset > cd->buffersize)
				add_input_jobs(cd->k + cd->m, cd->td_array, cd->blocksize, 1, cd) ;

			cd->mod = 0;
			cd->bptr = cd->block2;
		}       
		cd->buf_index = 0;
        } 
        if (cd->size - offset >= size) {
                cd->buf_index += size;
                if (cd->buf_index<=cd->buffersize) {
                        memcpy(buf, cd->bptr,  size);
                        SHA1_Update(&cd->hash, buf, size);
                        cd->bptr+=size;
                        retstat = size;
                } else {
                        buf_offset = cd->buf_index - cd->buffersize;
                        memcpy(buf, cd->bptr,  size - buf_offset);
                        SHA1_Update(&cd->hash, buf, size - buf_offset);
                        retstat = size - buf_offset;
                }          
        } else {
                memcpy(buf, cd->bptr, cd->size - offset);
                SHA1_Update(&cd->hash, buf, cd->size - offset);
                retstat = cd->size - offset;
        }
	//log_msg("fs_read return %d mode %d\n", retstat, cd->mode);
        return retstat;
}

/** Write data to an open file
 *
 ** Write should return exactly the number of bytes requested           *
 ** except on error.  An exception to this is when the 'direct_io'      *
 ** mount option is specified (see read operation).                     *
 **                                                                     *
 ** Changed in version 2.2                                              *
 *
 * 
 */
int fs_write(const char *path, const char *buf, size_t size, off_t offset,
             struct fuse_file_info *fi)
{
	//log_msg("WRITE SIZE=%d OFFSET=%d\n", size, offset);

        int retstat = size;
        int buf_offset = 0;

        char buf_temp[size];

        struct coding_data * cd = (struct coding_data *)fi->fh;

        // Set mode to 0=writing
        cd->mode = 0;

        // Update the hash with new data
        SHA1_Update(&cd->hash, buf, size);

        // Append the file size
        cd->size += size;

        // Append the buffer's index
        cd->buf_index += size;

        // Set the pointers
        if (cd->buf_index>=cd->buffersize) {
                // Calculate buffer offset
                buf_offset = cd->buf_index-cd->buffersize;

                // Copy the system buffer's content to the coding buffer
                memcpy(cd->bptr, buf, size - buf_offset);

                // Set the coding buffer's poiter to the offset value
                if(buf_offset > 0) {
                        memcpy(buf_temp, buf, size);
                        cd->bptr = buf_temp + size - buf_offset;
                }

                // Encode
                encode(cd->tech, cd->k, cd->m, cd->w, cd->matrix, cd->schedule, cd->data, cd->coding, cd->blocksize, cd->packetsize);
                
		// Fill the queue with the jobs
                add_output_jobs(cd->k, cd->m, cd->td_array, cd->data, cd->coding, cd->blocksize, 2, cd) ;  
                if (buf_offset > 0)
                	memcpy(cd->block, cd->bptr, buf_offset);

                // Set the pointer and index to the beggining of the buffer. If there was an offset, add it to them
                cd->bptr = cd->block + buf_offset;

                // Set the buffer index to the offset value
                cd->buf_index = buf_offset;
        } else {
                // Copy the data from the coding buffer to the buf from the system call. Set the pointer to the next chunk
                memcpy(cd->bptr, buf, size);
                cd->bptr+=size;
        }
	error_check(cd, &retstat);
	//log_msg("fs_write return %d mode %d\n", retstat, cd->mode);
        return retstat;
}

/** Get file system statistics
 *
 * The 'f_frsize', 'f_favail', 'f_fsid' and 'f_flag' fields are ignored
 *
 * Replaced 'struct statfs' parameter with 'struct statvfs' in
 * version 2.5
 */
int fs_statfs(const char *path, struct statvfs *statv)
{
        int retstat = 0;
        char fpath[PATH_MAX];
    
        log_msg("fs_statfs(path=\"%s\", statv=0x%08x)\n",path, statv);
    //fs_fullpath(fpath, path);
    sprintf(fpath, "%s%s", FS_DATA->dir[0], path);
    // get stats for underlying filesystem
    retstat = statvfs(fpath, statv);
    if (retstat < 0)
        retstat = -errno;
    
    //log_statvfs(statv);
    //log_msg("fs_statfs closed\n");
    return retstat;
}

/** Possibly flush cached data
 *
 * BIG NOTE: This is not equivalent to fsync().  It's not a
 * request to sync dirty data.
 *
 * Flush is called on each close() of a file descriptor.  So if a
 * filesystem wants to return write errors in close() and the file
 * has cached dirty data, this is a good place to write back data
 * and return any errors.  Since many applications ignore close()
 * errors this is not always useful.
 *
 * NOTE: The flush() method may be called more than once for each
 * open().  This happens if more than one file descriptor refers
 * to an opened file due to dup(), dup2() or fork() calls.  It is
 * not possible to determine if a flush is final, so each flush
 * should be treated equally.  Multiple write-flush sequences are
 * relatively rare, so this shouldn't be a problem.
 *
 * Filesystems shouldn't assume that flush will always be called
 * after some writes, or that if will be called at all.
 *
 * Changed in version 2.2
 */
int fs_flush(const char *path, struct fuse_file_info *fi)
{
	log_msg("fs_flush path=\"%s\" \n", path);

        int retstat = 0;
	size_t mod;
	int i;
        char file_hash_temp[BLOCK_HASH_LENGTH];
	char * headindex;
	unsigned char digest[SHA1_DIEGEST_LENGTH];
	
        struct coding_data* cd = (struct coding_data*) fi->fh;
        switch (cd->mode) {
	case 0:
		// Finalize hashing
		sha1_finalize(&cd->hash, file_hash_temp);
		// If necessary, fill the rest of the data with zeros
                mod = (cd->size)%cd->buffersize;
                if (mod!=0) {
                        for (i = mod; i < cd->buffersize; i++) 
                                cd->block[i]='0';
                        encode(cd->tech, cd->k, cd->m, cd->w, cd->matrix, cd->schedule, cd->data, cd->coding, cd->blocksize, cd->packetsize);
                        add_output_jobs(cd->k, cd->m, cd->td_array, cd->data, cd->coding, cd->blocksize, 2, cd) ;
                }
                // Create header data
                for (i = 0; i<cd->k+cd->m; i++) {
                	if( i<cd->k) {
                        	memset(cd->data[i], 0, HEAD_LEN);
                        	headindex = cd->data[i];
                        } else {
                               	memset(cd->coding[i-cd->k], 0, HEAD_LEN);
                                headindex = cd->coding[i-cd->k];
                        }
                        headindex+=sprintf(headindex, "%ld\n", cd->size);
                        headindex+=sprintf(headindex, "%d %d %d %d %d %ld %d\n",i,  cd->k, cd->m, cd->w, cd->packetsize, cd->blocksize, (int)cd->tech) ;
                        headindex+=sprintf(headindex, "%s\n", file_hash_temp);
                        /* If external app is set - provide an operation on a closed file */
                        /*if(FS_DATA->ext_argc>0) {
                                sprintf(fname, "%s%s_block", cd->td_array[i].dir, path);
                                strcpy(FS_DATA->ext_arg[FS_DATA->ext_argc-2], fname);
                                execve(FS_DATA->ext_arg[0], FS_DATA->ext_arg, NULL);
                        }*/
                }
		// Set file offset to 0
		add_output_jobs(cd->k, cd->m, cd->td_array, cd->data, cd->coding, HEAD_LEN, 3, cd);
		// Write data to the header
                add_output_jobs(cd->k, cd->m, cd->td_array, cd->data, cd->coding, HEAD_LEN, 4, cd);
		// Close the files
		add_output_jobs(cd->k, cd->m, cd->td_array, cd->data, cd->coding, HEAD_LEN, 5, cd);
		synchronize_jobs(cd->counter, cd->m_cond, cd->m_mutex, cd->k+cd->m, 5); 
		break;
	case 1:
		synchronize_jobs(cd->counter, cd->m_cond, cd->m_mutex, cd->k+cd->m, 1); 
		SHA1_Final(digest, &cd->hash);
		add_input_jobs(cd->k + cd->m, cd->td_array, HEAD_LEN, 6, cd);
		synchronize_jobs(cd->counter, cd->m_cond, cd->m_mutex, cd->k+cd->m, 6); 
		retstat = 0;
		break;
	case 2:
		SHA1_Final(digest, &cd->hash);
		add_input_jobs(cd->k + cd->m, cd->td_array, HEAD_LEN, 3, cd);
		synchronize_jobs(cd->counter, cd->m_cond, cd->m_mutex, cd->k+cd->m, 3); 
		retstat = -EIO;
		break;
	case 3:
		sha1_finalize(&cd->hash, file_hash_temp);
                if(strcmp(file_hash_temp,cd->file_hash)!=0) {
                       	log_msg("fs_flush path=\"%s\" ERROR file corrupted, original hash: %s, calc: %s \n", path, cd->file_hash, file_hash_temp);
                       	retstat = -EAGAIN;
                }
		add_input_jobs(cd->k + cd->m, cd->td_array, HEAD_LEN, 3, cd);
		synchronize_jobs(cd->counter, cd->m_cond, cd->m_mutex, cd->k+cd->m, 3); 
		break;
	}
	error_check(cd, &retstat);
        log_msg("fs_flush path=\"%s\" returned %d\n", path, retstat);
        return retstat;
}

/** Release an open file
 *
 * Release is called when there are no more references to an open
 * file: all file descriptors are closed and all memory mappings
 * are unmapped.
 *
 * For every open() call there will be exactly one release() call
 * with the same flags and file descriptor.  It is possible to
 * have a file opened more than once, in which case only the last
 * release will mean, that no more reads/writes will happen on the
 * file.  The return value of release is ignored.
 *
 * Changed in version 2.2
 */
int fs_release(const char *path, struct fuse_file_info *fi) 
{
	log_msg("fs_release path=\"%s\" \n", path);

        int retstat = 0;

        struct coding_data* cd = (struct coding_data*) fi->fh;
	unsigned char digest[SHA1_DIEGEST_LENGTH];
	if(cd->mode == 0)
        	free_output_cd(cd);
	else if (cd->mode == 1 || cd->mode == 2 || cd->mode == 3)
		free_input_cd(cd);
	else if (cd->mode == -1) {
		synchronize_jobs(cd->counter, cd->m_cond, cd->m_mutex, cd->k+cd->m, 1); 
		SHA1_Final(digest, &cd->hash);
		add_input_jobs(cd->k + cd->m, cd->td_array, HEAD_LEN, 6, cd);
		synchronize_jobs(cd->counter, cd->m_cond, cd->m_mutex, cd->k+cd->m, 6);
		free_input_cd(cd); 
	}
		
        log_msg("fs_release path=\"%s\" returned %d\n", path, retstat);
        return retstat;
}

/** Synchronize file contents */
int fs_fsync(const char *path, int datasync, struct fuse_file_info *fi)
{
        log_msg("fs_fsync(path=\"%s\")\n", path);
        return -EROFS;
}

/** Set extended attributes */
int fs_setxattr(const char *path, const char *name, const char *value, size_t size, int flags)
{    
        log_msg("fs_setxattr(path=\"%s\")\n", path);
        return -ENOTSUP;
}

/** Get extended attributes */
int fs_getxattr(const char *path, const char *name, char *value, size_t size)
{
        log_msg("fs_getxattr(path=\"%s\")\n", path);
        return -ENOTSUP;
}

/** List extended attributes */
int fs_listxattr(const char *path, char *list, size_t size)
{
        log_msg("fs_listxattr(path=\"%s\")\n", path);
        return -ENOTSUP;
}

/** Remove extended attributes */
int fs_removexattr(const char *path, const char *name)
{    
        log_msg("fs_removexattr(path=\"%s\")\n", path);
        return -EPERM;
}

/** Open directory
 *
 * This method should check if the open operation is permitted for
 * this  directory
 *
 * Introduced in version 2.3
 */
int fs_opendir(const char *path, struct fuse_file_info *fi)
{
        int retstat = 0;

        log_msg("fs_opendir(path=\"%s\", fi=0x%08x)\n", path, fi);

        //log_fi(fi);

        return retstat;
}

/** Read directory
 *
 * This supersedes the old getdir() interface.  New applications
 * should use this.
 *
 * The filesystem may choose between two modes of operation:
 *
 * 1) The readdir implementation ignores the offset parameter, and
 * passes zero to the filler function's offset.  The filler
 * function will not return '1' (unless an error happens), so the
 * whole directory is read in a single readdir operation.  This
 * works just like the old getdir() method.
 *
 * 2) The readdir implementation keeps track of the offsets of the
 * directory entries.  It uses the offset parameter and always
 * passes non-zero offset to the filler function.  When the buffer
 * is full (or an error happens) the filler function will return
 * '1'.
 *
 * Introduced in version 2.3
 */
int fs_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
        int retstat = 0;
        DIR *dp;
        struct dirent *de;
        char * pt;
        char fnam2[PATH_MAX];
        char dirname[PATH_MAX];
        int dirs = 0;
        int dirnb = 0;
        log_msg("fs_readdir path=\"%s\" \n", path);
        
        do {
                sprintf(dirname, "%s%s", FS_DATA->dir[dirnb], path);
                dp = opendir(dirname);
                if (dp != NULL) {
                        while ((de = readdir(dp)) != NULL) {
                                if ((strcmp(de->d_name,  ".") == 0) || (strcmp(de->d_name,  "..") == 0)) {
                                        if (filler(buf, de->d_name, NULL, 0) != 0) {
                                                log_msg("    ERROR fs_readdir filler:  buffer full");
                                                return -ENOMEM;
                                        }
                                } else if (de->d_type==0x4) {
                                        filler(buf, de->d_name, NULL, 0);
                                        dirs++;
                                        retstat = 0;
                                } else {
                                        memcpy(fnam2, de->d_name, PATH_MAX);
                                        pt = strrchr(fnam2, '_');
                                        if (pt != NULL) {
                                                if ((strcmp(pt,  "_block") == 0)) {
                                                        *pt = '\0';
                                                        if (filler(buf, fnam2, NULL, 0) != 0) {
                                                                log_msg("    ERROR fs_readdir filler:  buffer full");
                                                                return -ENOMEM;
                                                        } else {
                                                                retstat = 0;
                                                                dirs++;
                                                        }
                                                }
                                        }
                                }
                        }
                        closedir(dp);
                } else {
                        retstat = -errno;
                }
                dirnb++;
        } while(dirs==0 && dirnb<FS_DATA->m);
        //log_fi(fi);
        //log_msg("fs_readdir path=\"%s\" returned %d \n", path, retstat);
        return retstat;
}

/** Release directory */
int fs_releasedir(const char *path, struct fuse_file_info *fi)
{
    int retstat = 0;
    
    log_msg("fs_releasedir(path=\"%s\", fi=0x%08x)\n", path, fi);
    //log_fi(fi);
    
    return retstat;
}

/** Synchronize directory contents
 *
 * If the datasync parameter is non-zero, then only the user data
 * should be flushed, not the meta data
 *
 * Introduced in version 2.3
 */
// when exactly is this called?  when a user calls fsync and it
// happens to be a directory? ???
int fs_fsyncdir(const char *path, int datasync, struct fuse_file_info *fi)
{
    int retstat = 0;
    
    log_msg("fs_fsyncdir(path=\"%s\", datasync=%d, fi=0x%08x)\n", path, datasync, fi);
    //log_fi(fi);
    
    return retstat;
}

/**
 * Initialize filesystem
 *
 * The return value will passed in the private_data field of
 * fuse_context to all file operations and as a parameter to the
 * destroy() method.
 *
 * Introduced in version 2.3
 * Changed in version 2.6
 */
// Undocumented but extraordinarily useful fact:  the fuse_context is
// set up before this function is called, and
// fuse_get_context()->private_data returns the user_data passed to
// fuse_main().  Really seems like either it should be a third
// parameter coming in here, or else the fact should be documented
// (and this might as well return void, as it did in older versions of
// FUSE).
void *fs_init(struct fuse_conn_info *conn)
{
	log_msg("fs_init()\n");

        int i;
        int rc;
        
        struct ct_data ** ti = malloc(sizeof(struct ct_data *)*(FS_DATA->k + FS_DATA->m));
	struct ct_data ** ti2 = malloc(sizeof(struct ct_data *)*(FS_DATA->k + FS_DATA->m));

	// output
	char ** memory_pool = malloc(sizeof(char*)*FS_DATA->oqueue);
	for (i=0; i<FS_DATA->oqueue; i++)
		memory_pool[i] = malloc(sizeof(char)*FS_DATA->blocksize);

	FS_DATA->output_job_pool = malloc(sizeof(struct job)*FS_DATA->oqueue);
	FS_DATA->output_job_pool_first = &FS_DATA->output_job_pool[0];
	for (i=0; i<FS_DATA->oqueue; i++) {
		FS_DATA->output_job_pool[i].data = memory_pool[i];
		if (i<FS_DATA->oqueue-1)
                	FS_DATA->output_job_pool[i].next = &FS_DATA->output_job_pool[i+1];
	}
	FS_DATA->output_job_pool_last = &FS_DATA->output_job_pool[i-1];
	FS_DATA->output_job_pool_last->next = NULL;

	FS_DATA->output_job_mutex = malloc(sizeof(pthread_mutex_t));
        pthread_mutex_init (FS_DATA->output_job_mutex, NULL);
	
	FS_DATA->output_queue_mutex = malloc(sizeof(pthread_mutex_t));
        pthread_mutex_init (FS_DATA->output_queue_mutex, NULL);

	FS_DATA->output_thread_mutex = malloc(sizeof(pthread_mutex_t));
        pthread_mutex_init (FS_DATA->output_thread_mutex, NULL);

	FS_DATA->output_thread_cond = malloc(sizeof(pthread_cond_t));
        pthread_cond_init (FS_DATA->output_thread_cond, NULL);

	FS_DATA->output_dir_mutex = malloc(sizeof(pthread_mutex_t));
        pthread_mutex_init (FS_DATA->output_dir_mutex, NULL);

	FS_DATA->output_jobs_nb = malloc(sizeof(sem_t));
	sem_init(FS_DATA->output_jobs_nb, 0, FS_DATA->oqueue);

	FS_DATA->output_thread_queue = malloc(sizeof(struct job *)*(FS_DATA->k+FS_DATA->m));
	FS_DATA->last_output_th_job = malloc(sizeof(struct job *)*(FS_DATA->k+FS_DATA->m));
	FS_DATA->output_thread_queue_mutex = malloc(sizeof(pthread_mutex_t *)*(FS_DATA->k+FS_DATA->m));
	FS_DATA->output_ctrlth_cond = malloc(sizeof(pthread_cond_t *)*(FS_DATA->k+FS_DATA->m));
	FS_DATA->output_ctrlth_mutex = malloc(sizeof(pthread_mutex_t *)*(FS_DATA->k+FS_DATA->m));
	FS_DATA->output_queue = NULL;
	FS_DATA->last_output_job = NULL;
	for (i=0; i<FS_DATA->k+FS_DATA->m; i++) {
		FS_DATA->output_thread_queue_mutex[i] = malloc(sizeof(pthread_mutex_t));
		pthread_mutex_init (FS_DATA->output_thread_queue_mutex[i], NULL);

		FS_DATA->output_ctrlth_mutex[i] = malloc(sizeof(pthread_mutex_t));
        	pthread_mutex_init (FS_DATA->output_ctrlth_mutex[i], NULL);

		FS_DATA->output_ctrlth_cond[i] = malloc(sizeof(pthread_cond_t));
        	pthread_cond_init (FS_DATA->output_ctrlth_cond[i], NULL);

		FS_DATA->output_thread_queue[i] = NULL;
		FS_DATA->last_output_th_job[i] = NULL;
	}

        FS_DATA->output_thread = malloc (sizeof(pthread_t *)*FS_DATA->othreads);
        for (i = 0; i < FS_DATA->othreads; i++) {
                FS_DATA->output_thread[i] = malloc(sizeof(pthread_t));
        }

        for (i = 0; i < FS_DATA->othreads; i++) {
                rc = pthread_create(FS_DATA->output_thread[i], NULL, output_thread_function, FS_DATA);
                if (rc) {
                        return NULL;
                }
        }

	FS_DATA->output_ctrl_thread =  malloc (sizeof(pthread_t *)*(FS_DATA->k+FS_DATA->m));
	for (i = 0; i < FS_DATA->k+FS_DATA->m; i++) {
                FS_DATA->output_ctrl_thread[i] = malloc(sizeof(pthread_t));
        }

        for (i = 0; i < FS_DATA->k+FS_DATA->m; i++) {
		ti[i] = malloc(sizeof(struct ct_data));
		ti[i]->nb = i;
		ti[i]->fsdata = FS_DATA;
                rc = pthread_create(FS_DATA->output_ctrl_thread[i], NULL, output_ctrl_thread_function, ti[i]);
		log_msg("Creating control thread nb: %d return %d \n", i, rc);
                if (rc) {
                        return NULL;
                }
        }

	// input
	FS_DATA->input_queue_mutex = malloc(sizeof(pthread_mutex_t));
        pthread_mutex_init (FS_DATA->input_queue_mutex, NULL);

	FS_DATA->input_thread_mutex = malloc(sizeof(pthread_mutex_t));
        pthread_mutex_init (FS_DATA->input_thread_mutex, NULL);

	FS_DATA->input_dir_mutex = malloc(sizeof(pthread_mutex_t));
        pthread_mutex_init (FS_DATA->input_dir_mutex, NULL);

	FS_DATA->input_thread_cond = malloc(sizeof(pthread_cond_t));
        pthread_cond_init (FS_DATA->input_thread_cond, NULL);

	FS_DATA->input_thread_queue = malloc(sizeof(struct job *)*(FS_DATA->k+FS_DATA->m));
	FS_DATA->last_input_th_job = malloc(sizeof(struct job *)*(FS_DATA->k+FS_DATA->m));
	FS_DATA->input_thread_queue_mutex = malloc(sizeof(pthread_mutex_t *)*(FS_DATA->k+FS_DATA->m));
	FS_DATA->input_ctrlth_cond = malloc(sizeof(pthread_cond_t *)*(FS_DATA->k+FS_DATA->m));
	FS_DATA->input_ctrlth_mutex = malloc(sizeof(pthread_mutex_t *)*(FS_DATA->k+FS_DATA->m));
	FS_DATA->input_queue = NULL;
	FS_DATA->last_input_job = NULL;
	for (i=0; i<FS_DATA->k+FS_DATA->m; i++) {
		FS_DATA->input_thread_queue_mutex[i] = malloc(sizeof(pthread_mutex_t));
		pthread_mutex_init (FS_DATA->input_thread_queue_mutex[i], NULL);

		FS_DATA->input_ctrlth_mutex[i] = malloc(sizeof(pthread_mutex_t));
        	pthread_mutex_init (FS_DATA->input_ctrlth_mutex[i], NULL);

		FS_DATA->input_ctrlth_cond[i] = malloc(sizeof(pthread_cond_t));
        	pthread_cond_init (FS_DATA->input_ctrlth_cond[i], NULL);

		FS_DATA->input_thread_queue[i] = NULL;
		FS_DATA->last_input_th_job[i] = NULL;
	}

        FS_DATA->input_thread = malloc (sizeof(pthread_t *)*FS_DATA->ithreads);
        for (i = 0; i < FS_DATA->ithreads; i++) {
                FS_DATA->input_thread[i] = malloc(sizeof(pthread_t));
        }

        for (i = 0; i < FS_DATA->ithreads; i++) {
                rc = pthread_create(FS_DATA->input_thread[i], NULL, input_thread_function, FS_DATA);
                if (rc) {
                        return NULL;
                }
        }

	FS_DATA->input_ctrl_thread =  malloc (sizeof(pthread_t *)*(FS_DATA->k+FS_DATA->m));
	for (i = 0; i < FS_DATA->k+FS_DATA->m; i++) {
                FS_DATA->input_ctrl_thread[i] = malloc(sizeof(pthread_t));
        }

        for (i = 0; i < FS_DATA->k+FS_DATA->m; i++) {
		ti2[i] = malloc(sizeof(struct ct_data));
		ti2[i]->nb = i;
		ti2[i]->fsdata = FS_DATA;
                rc = pthread_create(FS_DATA->input_ctrl_thread[i], NULL, input_ctrl_thread_function, ti2[i]);
		log_msg("Creating control thread nb: %d return %d \n", i, rc);
                if (rc) {
                        return NULL;
                }
        }




	free(ti); 
	free(ti2);
	free(memory_pool);

        return FS_DATA;
}

/**
 * Clean up filesystem
 *
 * Called on filesystem exit.
 *
 * Introduced in version 2.3
 */
void fs_destroy(void *userdata)
{
        void * status;
        int i, rc;
        log_msg("fs_destroy(userdata=0x%08x)\n", userdata);
        //struct fs_state * FS_DATA;
        struct job * new_job;

        //FS_DATA = (struct fs_state *) userdata;
        for (i=0; i<FS_DATA->othreads; i++) {
                new_job = malloc(sizeof(struct job));
                new_job->next           = NULL;
                new_job->data           = NULL;
                new_job->blockfile      = NULL;
                new_job->hash           = NULL;
                new_job->counter        = NULL;
                new_job->error          = NULL;
                new_job->blocksize      = 0;
                new_job->mode           = -1; //'0'-WRITING, '1'-READING, '-1'-to destroy;
                pthread_mutex_lock(FS_DATA->output_queue_mutex);
                if(FS_DATA->output_queue == NULL)
                        FS_DATA->output_queue = new_job;
                else
                        FS_DATA->last_output_job->next = new_job;
                FS_DATA->last_output_job = new_job;
                pthread_mutex_unlock(FS_DATA->output_queue_mutex);
                pthread_cond_signal(FS_DATA->output_thread_cond);
        }
        for(i=0; i<FS_DATA->othreads; i++) {
                rc = pthread_join(*FS_DATA->output_thread[i], &status);
                if (rc) {
                        log_msg("pthread_join() in destroy() returned %d\n", rc);
                        break;
                } else {
                        log_msg("thread [%d] iter = %ld\n", i, (long int) status);
                }
        }
        while(FS_DATA->output_queue != NULL) {
                FS_DATA->last_output_job = FS_DATA->output_queue;
                FS_DATA->output_queue = FS_DATA->output_queue->next;
                free(FS_DATA->last_output_job);
        }  


        for (i=0; i<FS_DATA->k+FS_DATA->m; i++) {
                new_job = malloc(sizeof(struct job));
                new_job->next           = NULL;
                new_job->data           = NULL;
                new_job->blockfile      = NULL;
                new_job->hash           = NULL;
                new_job->counter        = NULL;
                new_job->error          = NULL;
                new_job->blocksize      = 0;
                new_job->mode           = -1; //'0'-WRITING, '1'-READING, '-1'-to destroy;
                pthread_mutex_lock(FS_DATA->output_thread_queue_mutex[i]);
                if(FS_DATA->output_thread_queue[i] == NULL)
                        FS_DATA->output_thread_queue[i] = new_job;
                else
                        FS_DATA->last_output_th_job[i]->next = new_job;
                FS_DATA->last_output_th_job[i] = new_job;
                pthread_mutex_unlock(FS_DATA->output_thread_queue_mutex[i]);
                pthread_cond_signal(FS_DATA->output_ctrlth_cond[i]);
        }
        for(i=0; i<(FS_DATA->k+FS_DATA->m); i++) {
                rc = pthread_join(*FS_DATA->output_ctrl_thread[i], &status);
                if (rc) {
                        log_msg("pthread_join() in destroy() returned %d\n", rc);
                        break;
                } else {
                        log_msg("thread [%d] iter = %ld\n", i, (long int) status);
                }
        }     

	for (i=0; i<FS_DATA->k+FS_DATA->m; i++) {
		free(FS_DATA->output_thread_queue[i]);
		pthread_mutex_destroy(FS_DATA->output_thread_queue_mutex[i]);
		free(FS_DATA->output_thread_queue_mutex[i]);
		pthread_mutex_destroy(FS_DATA->output_ctrlth_mutex[i]);
		free(FS_DATA->output_ctrlth_mutex[i]);
		pthread_cond_destroy(FS_DATA->output_ctrlth_cond[i]);
        	free(FS_DATA->output_ctrlth_cond[i]);
	}
	for (i=0; i<FS_DATA->oqueue; i++) {
		free((FS_DATA->output_job_pool[i]).data);
	} 
	free(FS_DATA->output_job_pool);

	pthread_mutex_destroy(FS_DATA->output_job_mutex);
	free(FS_DATA->output_job_mutex);

	pthread_mutex_destroy(FS_DATA->output_queue_mutex);
	free(FS_DATA->output_queue_mutex);

	pthread_mutex_destroy(FS_DATA->output_thread_mutex);
	free(FS_DATA->output_thread_mutex);

	pthread_cond_destroy(FS_DATA->output_thread_cond);
        free(FS_DATA->output_thread_cond);

	pthread_mutex_destroy(FS_DATA->output_dir_mutex);
        free(FS_DATA->output_dir_mutex);

	sem_destroy(FS_DATA->output_jobs_nb);
	free(FS_DATA->output_jobs_nb);
	free(FS_DATA->output_thread_queue);
	free(FS_DATA->output_thread_queue_mutex);
	free(FS_DATA->output_ctrlth_cond);
	free(FS_DATA->output_ctrlth_mutex);
	free(FS_DATA->last_output_th_job);

        for (i = 0; i < FS_DATA->othreads; i++)
                free(FS_DATA->output_thread[i]);
	for (i=0; i<FS_DATA->k+FS_DATA->m; i++)
		free(FS_DATA->output_ctrl_thread[i]);
        free(FS_DATA->output_thread);
	free(FS_DATA->output_ctrl_thread);

	// input
	for (i=0; i<FS_DATA->ithreads; i++) {
                new_job = malloc(sizeof(struct job));
                new_job->next           = NULL;
                new_job->data           = NULL;
                new_job->blockfile      = NULL;
                new_job->hash           = NULL;
                new_job->counter        = NULL;
                new_job->error          = NULL;
                new_job->blocksize      = 0;
                new_job->mode           = -1; //'0'-WRITING, '1'-READING, '-1'-to destroy;
                pthread_mutex_lock(FS_DATA->input_queue_mutex);
                if(FS_DATA->input_queue == NULL)
                        FS_DATA->input_queue = new_job;
                else
                        FS_DATA->last_input_job->next = new_job;
                FS_DATA->last_input_job = new_job;
                pthread_mutex_unlock(FS_DATA->input_queue_mutex);
                pthread_cond_signal(FS_DATA->input_thread_cond);
        }
        for(i=0; i<FS_DATA->ithreads; i++) {
                rc = pthread_join(*FS_DATA->input_thread[i], &status);
                if (rc) {
                        log_msg("pthread_join() in destroy() returned %d\n", rc);
                        break;
                } else {
                        log_msg("thread [%d] iter = %ld\n", i, (long int) status);
                }
        }
        while(FS_DATA->input_queue != NULL) {
                FS_DATA->last_input_job = FS_DATA->input_queue;
                FS_DATA->input_queue = FS_DATA->input_queue->next;
                free(FS_DATA->last_input_job);
        }  


        for (i=0; i<FS_DATA->k+FS_DATA->m; i++) {
                new_job = malloc(sizeof(struct job));
                new_job->next           = NULL;
                new_job->data           = NULL;
                new_job->blockfile      = NULL;
                new_job->hash           = NULL;
                new_job->counter        = NULL;
                new_job->error          = NULL;
                new_job->blocksize      = 0;
                new_job->mode           = -1; //'0'-WRITING, '1'-READING, '-1'-to destroy;
                pthread_mutex_lock(FS_DATA->input_thread_queue_mutex[i]);
                if(FS_DATA->input_thread_queue[i] == NULL)
                        FS_DATA->input_thread_queue[i] = new_job;
                else
                        FS_DATA->last_input_th_job[i]->next = new_job;
                FS_DATA->last_input_th_job[i] = new_job;
                pthread_mutex_unlock(FS_DATA->input_thread_queue_mutex[i]);
                pthread_cond_signal(FS_DATA->input_ctrlth_cond[i]);
        }
        for(i=0; i<(FS_DATA->k+FS_DATA->m); i++) {
                rc = pthread_join(*FS_DATA->input_ctrl_thread[i], &status);
                if (rc) {
                        log_msg("pthread_join() in destroy() returned %d\n", rc);
                        break;
                } else {
                        log_msg("thread [%d] iter = %ld\n", i, (long int) status);
                }
        }     

	for (i=0; i<FS_DATA->k+FS_DATA->m; i++) {
		free(FS_DATA->input_thread_queue[i]);
		pthread_mutex_destroy(FS_DATA->input_thread_queue_mutex[i]);
		free(FS_DATA->input_thread_queue_mutex[i]);
		pthread_mutex_destroy(FS_DATA->input_ctrlth_mutex[i]);
		free(FS_DATA->input_ctrlth_mutex[i]);
		pthread_cond_destroy(FS_DATA->input_ctrlth_cond[i]);
        	free(FS_DATA->input_ctrlth_cond[i]);
	}
	pthread_mutex_destroy(FS_DATA->input_queue_mutex);
	free(FS_DATA->input_queue_mutex);

	pthread_mutex_destroy(FS_DATA->input_thread_mutex);
	free(FS_DATA->input_thread_mutex);

	pthread_cond_destroy(FS_DATA->input_thread_cond);
        free(FS_DATA->input_thread_cond);

	pthread_mutex_destroy(FS_DATA->input_dir_mutex);
        free(FS_DATA->input_dir_mutex);

	free(FS_DATA->input_thread_queue);
	free(FS_DATA->input_thread_queue_mutex);
	free(FS_DATA->input_ctrlth_cond);
	free(FS_DATA->input_ctrlth_mutex);
	free(FS_DATA->last_input_th_job);

        for (i = 0; i < FS_DATA->ithreads; i++)
                free(FS_DATA->input_thread[i]);
	for (i=0; i<FS_DATA->k+FS_DATA->m; i++)
		free(FS_DATA->input_ctrl_thread[i]);
        free(FS_DATA->input_thread);
	free(FS_DATA->input_ctrl_thread);
        //pthread_exit(NULL);
        
}

/** Check file access permissions */
int fs_access(const char *path, int mask)
{
        log_msg("fs_access(path=\"%s\"\n", path);
        return -EACCES;
}

/**
 * Create and open a file
 *
 * If the file does not exist, first create it with the specified
 * mode, and then open it.
 *
 * If this method is not implemented or under Linux kernel
 * versions earlier than 2.6.15, the mknod() and open() methods
 * will be called instead.
 *
 * Introduced in version 2.5
 */
int fs_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
	log_msg("fs_create path=\"%s\"\n", path);

        int retstat = 0;
        
        // Allocate the coding_data structure
        struct coding_data * cd = malloc(sizeof(struct coding_data));
        set_output_cd(cd, FS_DATA, path);

	// Mode = 0: Open file descriptors
        add_output_jobs(cd->k, cd->m, cd->td_array, cd->data, cd->coding, HEAD_LEN, 0, cd) ;

	// Mode = 1: Fill the header with zeros
	add_output_jobs(cd->k, cd->m, cd->td_array, cd->data, cd->coding, HEAD_LEN, 1, cd) ;

	// Assign structure pointer to a file_handler in fuse_file_info structure
	fi->fh = (uint64_t)cd;
	
	// Initiate hashing
        SHA1_Init(&cd->hash);

        //log_msg("fs_create path=\"%s\" returned: %d\n", path, retstat);
	error_check(cd, &retstat);
	if(retstat) {
		add_output_jobs(cd->k, cd->m, cd->td_array, cd->data, cd->coding, HEAD_LEN, 5, cd);
		synchronize_jobs(cd->counter, cd->m_cond, cd->m_mutex, cd->k+cd->m, 5); 
		free_output_cd(cd);
	}
	log_msg("fs_create path=\"%s\" return=%d\n", path, retstat);
        return retstat;
}

/** Change the size of an open file */
int fs_ftruncate(const char *path, off_t offset, struct fuse_file_info *fi)
{
        log_msg("fs_ftruncate(path=\"%s\")\n", path);
        return -EPERM;
}

/** Get attributes from an open file
 *
 * This method is called instead of the getattr() method if the
 * file information is available.
 *
 * Currently this is only called after the create() method if that
 * is implemented (see above).  Later it may be called for
 * invocations of fstat() too.
 *
 * Introduced in version 2.5
 */
int fs_fgetattr(const char *path, struct stat *statbuf, struct fuse_file_info *fi)
{
	log_msg("fs_fgetattr path=\"%s\"\n", path);

        int retstat = 0;
	char fdir[PATH_MAX];
        memset(statbuf, 0, sizeof(struct stat));
        sprintf(fdir, "%s%s_block", FS_DATA->dir[0], path);
	stat(fdir, statbuf);
        statbuf->st_mode = S_IFREG | 0751;
	statbuf->st_uid = 1013;
	statbuf->st_gid = 1000;
        statbuf->st_nlink = 1;
        statbuf->st_size = 0;
        
	log_msg("fs_fgetattr path=\"%s\" returned %d\n", path, retstat);
        return retstat;
}

struct fuse_operations fs_oper = {
  .getattr = fs_getattr,
  .readlink = fs_readlink,
  // no .getdir -- that's deprecated
  .getdir = NULL,
  .mknod = fs_mknod,
  .mkdir = fs_mkdir,
  .unlink = fs_unlink,
  .rmdir = fs_rmdir,
  .symlink = fs_symlink,
  .rename = fs_rename,
  .link = fs_link,
  .chmod = fs_chmod,
  .chown = fs_chown,
  .truncate = fs_truncate,
  .utime = fs_utime,
  .open = fs_open,
  .read = fs_read,
  .write = fs_write,
  /** Just a placeholder, don't set */ // huh???
  .statfs = fs_statfs,
  .flush = fs_flush,
  .release = fs_release,
  .fsync = fs_fsync,
  .setxattr = fs_setxattr,
  .getxattr = fs_getxattr,
  .listxattr = fs_listxattr,
  .removexattr = fs_removexattr,
  .opendir = fs_opendir,
  .readdir = fs_readdir,
  .releasedir = fs_releasedir,
  .fsyncdir = fs_fsyncdir,
  .init = fs_init,
  .destroy = fs_destroy,
  .access = fs_access,
  .create = fs_create,
  .ftruncate = fs_ftruncate,
  .fgetattr = fs_fgetattr
};

int main(int argc, char *argv[])
{
        int fuse_stat;
        size_t blocksize, buffersize;
        int k, m, w, packetsize;
	int ithreads, itimeo, iret, othreads, otimeo, oret, oqueue;
        char * s1;
        char * s2;
        char * dir;
        int i,j;
        int technb;
        char ** list = NULL;
        int threads = 1;
        int frm = 0;
        char external[1000];
        enum Coding_Technique tech;
        FILE * fp;
        FILE * dirfile;
        struct fs_state *fs_data;
        char ** arglist = NULL;
        DIR *dp;
	char confpath[PATH_MAX];
	char path[PATH_MAX];
        if ((getuid() == 0) || (geteuid() == 0)) {
                fprintf(stderr, "Warning!: Running filesystem as root opens unnacceptable security holes\n");
        }

        // Perform some sanity checking on the command line:  make sure
        // there are enough arguments, and that neither of the last two
        // start with a hyphen (this will break if you actually have a
        // rootpoint or mountpoint whose name starts with a hyphen, but so
        // will a zillion other programs)
        if ((argc < 2) || (argv[argc-1][0] == '-')){
                fprintf(stderr, "usage:  ecfs [FUSE and mount options] mountPoint\n");
                exit(0);
        }
        
	confpath[0] = '\0';
        /* Read the number of threads from parameter list */
        for(i=0; i<argc; i++) {
                if (strncmp(argv[i], "-yd=", 4)==0) {
                        sscanf(&argv[i][4], "%s", confpath);
                        for(j=i; j<(argc-1); j++)
                                argv[j] = argv[j+1];
                        argv[argc-1] = NULL;
                        argc--;
                        break;
                }
        }
fprintf(stderr, "confpath: %s\n", confpath);
        /* Check for the Force remove option in the parameters list */
        for(i=0; i<argc; i++) {
                if (strncmp(argv[i], "-frm=", 5)==0) {
                        sscanf(&argv[i][5], "%d", &frm);
                        for(j=i; j<(argc-1); j++)
                                argv[j] = argv[j+1];
                        argv[argc-1] = NULL;
                        argc--;
                        break;
                }
        }

        if( threads<1 && threads > MAX_THREADS) {
                fprintf(stderr, "Threads: %d\n", threads);
                exit(0);
        }

        /* Read the parameters from the setup file */
	sprintf(path,"%sCoding_parameters.txt",confpath);
        fp = fopen(path, "rb");
        if (fp == NULL) {
                fprintf(stderr, "Can't open Coding_parameters.txt\n");
                exit(0);        
        }
        if (fscanf(fp, "%d %d %d %d %zd %d", &k, &m, &w, &packetsize, &blocksize, &technb) !=6) {
                fprintf(stderr, "Wrong number of coding parameters\n");
                exit(0);
        }
        if (fscanf(fp, "%d %d %d %d %d %d %d", &ithreads, &itimeo, &iret, &othreads, &otimeo, &oret, &oqueue) !=7) {
                fprintf(stderr, "Wrong number of filesystem parameters\n");
                exit(0);
        }
	fprintf(stderr, "ithreads: %d\n", ithreads);
	fprintf(stderr, "othreads: %d\n", othreads);
	if( ithreads<1 || ithreads > MAX_THREADS) {
                fprintf(stderr, "Invalid number of input threads: %d\n", ithreads);
                exit(0);
        }
	if( othreads<1 || othreads > MAX_THREADS) {
                fprintf(stderr, "Invalid number of output threads: %d\n", othreads);
                exit(0);
        }
        if (technb<0 || technb>6) {
                fprintf(stderr, "Not valid coding technique\n");
                exit(0);
        }
        fclose(fp);
        buffersize = blocksize*k;
        if (k<0 || m<0 || w<0 || packetsize<0 || blocksize<0) {
                fprintf(stderr, "Parameters are not correct\n");
                exit(0);
        }
        switch(technb) {
                case 0:
                        if (w != 8 && w != 16 && w != 32) {
                                fprintf(stderr,  "w must be one of {8, 16, 32}\n");
                                exit(0);
                        }
                        tech = Reed_Sol_Van;
                        break;
                case 1:
                        if (m != 2) {
                                fprintf(stderr,  "m must be equal to 2\n");
                                exit(0);
                        }
                        if (w != 8 && w != 16 && w != 32) {
                                fprintf(stderr,  "w must be one of {8, 16, 32}\n");
                                exit(0);
                        }
                        tech = Reed_Sol_R6_Op;
                        break;
                case 2:
                        if (packetsize == 0) {
                                fprintf(stderr, "Must include packetsize.\n");
                                exit(0);
                        }
                        tech = Cauchy_Orig;
                        break;
                case 3:
                        if (packetsize == 0) {
                                fprintf(stderr, "Must include packetsize.\n");
                                exit(0);
                        }
                        tech = Cauchy_Good;
                        break;
                case 4:
                        if (k > w) {
                                fprintf(stderr,  "k must be less than or equal to w\n");
                                exit(0);
                        }
                        if (w <= 2 || !(w%2) || !is_prime(w)) {
                                fprintf(stderr,  "w must be greater than two and w must be prime\n");
                                exit(0);
                        }
                        if (packetsize == 0) {
                                fprintf(stderr, "Must include packetsize.\n");
                                exit(0);
                        }
                        if ((packetsize%(sizeof(int))) != 0) {
                                fprintf(stderr,  "packetsize must be a multiple of sizeof(int)\n");
                                exit(0);
                        }
                        tech = Liberation;
                        break;
                case 5:
                        if (k > w) {
                                fprintf(stderr,  "k must be less than or equal to w\n");
                                exit(0);
                        }
                        if (w <= 2 || !((w+1)%2) || !is_prime(w+1)) {
                                fprintf(stderr,  "w must be greater than two and w+1 must be prime\n");
                                exit(0);
                        }
                        if (packetsize == 0) {
                                fprintf(stderr, "Must include packetsize.\n");
                                exit(0);
                        }
                        if ((packetsize%(sizeof(int))) != 0) {
                                fprintf(stderr,  "packetsize must be a multiple of sizeof(int)\n");
                                exit(0);
                        }
                        tech = Blaum_Roth;
                        break;
                case 6:
                        if (packetsize == 0) {
                                fprintf(stderr, "Must include packetsize\n");
                                exit(0);
                        }
                        if (w != 8) {
                                fprintf(stderr, "w must equal 8\n");
                                exit(0);
                        }
                        if (m != 2) {
                                fprintf(stderr, "m must equal 2\n");
                                exit(0);
                        }
                        if (k > w) {
                                fprintf(stderr, "k must be less than or equal to w\n");
                                exit(0);
                        }
                        tech = Liber8tion;
                        break;
        }
        if (blocksize%(sizeof(int)*w*packetsize) != 0) {
                fprintf(stderr, "Proper values for parameters: blocksize mod (sizeof(int)*w*packetsize) = 0\n");
                exit(0);
        }
        fs_data = malloc(sizeof(struct fs_state));
        if (fs_data == NULL) {
                perror("main calloc");
                abort();
        }
        fs_data->ext_arg = NULL;
        fs_data->ext_argc = 0;
        for(i=0; i<argc; i++) {
                if (strncmp(argv[i], "-fe=", 4)==0) {
                        strcpy(external,&argv[i][4]);
                        s1 = external;
                        do {
                                s2 = strchr(s1, ' ');
                                if (s2 != NULL) {
                                        fs_data->ext_argc = fs_data->ext_argc + 1;
                                        *s2 = '\0';
                                        fs_data->ext_arg = (char **)realloc(arglist, sizeof(char*)*fs_data->ext_argc);
                                        fs_data->ext_arg[fs_data->ext_argc-1] = malloc(sizeof(char)*MAX_PATH);
                                        strcpy(fs_data->ext_arg[fs_data->ext_argc-1], s1);
                                        arglist = fs_data->ext_arg;
                                        s1 = s2 + 1;
                                }
                        } while(s2 != NULL);
                        fs_data->ext_argc = fs_data->ext_argc + 1;
                        fs_data->ext_arg = (char **)realloc(arglist, sizeof(char*)*fs_data->ext_argc);
                        fs_data->ext_arg[fs_data->ext_argc-1] = malloc(sizeof(char)*MAX_PATH);
                        strcpy(fs_data->ext_arg[fs_data->ext_argc-1], s1);
                        arglist = fs_data->ext_arg;
                        fs_data->ext_argc = fs_data->ext_argc + 1;
                        fs_data->ext_arg = (char **)realloc(arglist, sizeof(char*)*fs_data->ext_argc);
                        fs_data->ext_arg[fs_data->ext_argc-1] = malloc(sizeof(char)*MAX_PATH);
                        arglist = fs_data->ext_arg;
                        fs_data->ext_argc = fs_data->ext_argc + 1;
                        fs_data->ext_arg = (char **)realloc(arglist, sizeof(char*)*fs_data->ext_argc);
                        fs_data->ext_arg[j-1] = NULL;
                        for(j=0; j<fs_data->ext_argc; j++)
                                 fprintf(stderr, "\n %s \n", fs_data->ext_arg[j]);
                        for(j=i; j<argc; j++)
                                argv[j] = argv[j+1];
                        argv[argc-1] = NULL;
                        argc--;
                        break;
                }
        }
        fs_data->tech = tech;
        fs_data->k = k;
        fs_data->m = m;
        fs_data->w = w;
        fs_data->packetsize = packetsize;
        fs_data->blocksize = blocksize;
        fs_data->buffersize = buffersize;
	sprintf(path,"%slogfile.txt",confpath);
        fs_data->logfile = log_open(path);
        fs_data->matrix = NULL;
        fs_data->bitmatrix = NULL;
        fs_data->schedule = NULL;
        fs_data->nbofdirs = 20;
        fs_data->threads = threads;
        fs_data->frm = frm;
	fs_data->output_spares		= 0;
	fs_data->input_spares		= 0;
	fs_data->ithreads = ithreads;
	fs_data->itimeo = itimeo;
	fs_data->iret = iret;
	fs_data->othreads = othreads;
	fs_data->otimeo = otimeo;
	fs_data->oret = oret;
	fs_data->oqueue = oqueue;
        switch(tech) {
                case Reed_Sol_Van:
                        fs_data->matrix         = reed_sol_vandermonde_coding_matrix(k, m, w);
                        break;
                case Reed_Sol_R6_Op:
                        fs_data->matrix         = reed_sol_r6_coding_matrix(k, w);
                        break;
                case Cauchy_Orig:
                        fs_data->matrix         = cauchy_original_coding_matrix(k, m, w);
                        fs_data->bitmatrix      = jerasure_matrix_to_bitmatrix(k, m, w, fs_data->matrix);
                        fs_data->schedule       = jerasure_smart_bitmatrix_to_schedule(k, m, w, fs_data->bitmatrix);
                        break;
                case Cauchy_Good:
                        fs_data->matrix         = cauchy_good_general_coding_matrix(k, m, w);
                        fs_data->bitmatrix      = jerasure_matrix_to_bitmatrix(k, m, w, fs_data->matrix);
                        fs_data->schedule       = jerasure_smart_bitmatrix_to_schedule(k, m, w, fs_data->bitmatrix);
                        break;
                case Liberation:
                        fs_data->bitmatrix      = liberation_coding_bitmatrix(k, w);
                        fs_data->schedule       = jerasure_smart_bitmatrix_to_schedule(k, m, w, fs_data->bitmatrix);
                        break;
                case Blaum_Roth:
                        fs_data->bitmatrix      = blaum_roth_coding_bitmatrix(k, w);
                        fs_data->schedule       = jerasure_smart_bitmatrix_to_schedule(k, m, w, fs_data->bitmatrix);
                        break;
                case Liber8tion:
                        fs_data->bitmatrix      = liber8tion_coding_bitmatrix(k);
                        fs_data->schedule       = jerasure_smart_bitmatrix_to_schedule(k, m, w, fs_data->bitmatrix);
                        break;
        }


        /* Open the file with the list of directories */
 	sprintf(path,"%sCoding_directories.txt",confpath);
        dirfile = fopen(path, "r");
        if (dirfile == NULL) {
                fprintf(stderr, "Can't open Coding_directories.txt!\n");
                exit(0);        
        }
        i=0;
        dir = malloc(sizeof(char)*MAX_PATH);
        do {
                if (fgets(dir,  MAX_PATH,  dirfile) != NULL) {
                        i++;
                        fs_data->dir = (char **)realloc(list, sizeof(char*)*i);
                        fs_data->dir[i-1] = malloc(sizeof(char)*MAX_PATH);
                        strncpy(fs_data->dir[i-1], dir, MAX_PATH);
                        s1 = strrchr(fs_data->dir[i-1], '\n');
                        if(s1 != NULL) {
                                *s1 = '\0';
                                s1 = NULL;
                        }
                        list = fs_data->dir;
                }
        } while (!feof(dirfile));
        fclose(dirfile);
        if(i<k+m){
                fprintf(stderr, "Nb of directories is less than k+m!\n");
                exit(0);
        }
        fs_data->nbofdirs = i;
        fprintf(stderr, "ECFS mounted at %s\n", argv[argc-1]);
        fprintf(stderr, "coding technique: %s\n", tech_tab[technb]);
        fprintf(stderr, "k=%d m=%d w=%d\n", k, m, w);
        fprintf(stderr, "packetsize=%d\n", packetsize);
        fprintf(stderr, "blocksize=%zd\n", blocksize);
        fprintf(stderr, "buffersize=%zd\n", buffersize);
        fprintf(stderr, "Coding directories: \n");
        for(i=0; i < k + m; i++) {
                dp = opendir(fs_data->dir[i]);
                if(dp!=NULL) {
                        fprintf(stderr, "%s \n", fs_data->dir[i]);
                        closedir(dp);
                } else {
                        fprintf(stderr, "%s !\n", fs_data->dir[i]);
                }
        }
        fprintf(stderr, "Spare directories: \n");
        for(i = k + m; i<fs_data->nbofdirs; i++) {
                dp = opendir(fs_data->dir[i]);
                if(dp!=NULL) {
                        fprintf(stderr, "%s \n", fs_data->dir[i]);
                        closedir(dp);
                } else {
                        fprintf(stderr, "%s !\n", fs_data->dir[i]);
                }
        }       
        fprintf(stderr, "about to call fuse_main\n");
        
	
        fuse_stat = fuse_main(argc, argv, &fs_oper, fs_data);
        free(dir);
        jerasure_free_schedule(fs_data->schedule);
        for (i = 0; i < fs_data->nbofdirs; i++) {
                free(fs_data->dir[i]);
        }
                
        free(fs_data->dir);
        free(fs_data->matrix);
        free(fs_data->bitmatrix);
        //free(fs_data->schedule);
        fclose(fs_data->logfile);
        free(fs_data);
        fprintf(stderr, "fuse_main returned %d\n", fuse_stat);
        return fuse_stat;
}
