/*
  Copyright (C) 2012 Joseph J. Pfeiffer, Jr., Ph.D. <pfeiffer@cs.nmsu.edu>

  This program can be distributed under the terms of the GNU GPLv3.
  See the file COPYING.

  There are a couple of symbols that need to be #defined before
  #including all the headers.
*/

#ifndef _PARAMS_H_
#define _PARAMS_H_

// The FUSE API has been changed a number of times.  So, our code
// needs to define the version of the API that we assume.  As of this
// writing, the most current API version is 26
#define FUSE_USE_VERSION 26

// need this to get pwrite().  I have to use setvbuf() instead of
// setlinebuf() later in consequence.
#define _XOPEN_SOURCE 500
#define _POSIX_C_SOURCE 200112L
// maintain bbfs state in here
#include <limits.h>
#include <stdio.h>
#include <openssl/sha.h>
#include <pthread.h>
#include <sys/types.h>
#include <semaphore.h>

enum Coding_Technique {Reed_Sol_Van, Reed_Sol_R6_Op, Cauchy_Orig, Cauchy_Good, Liberation, Blaum_Roth, Liber8tion};


struct thread_data {
	char * dir;
	int *error;
	int *blockfile;
	SHA_CTX *hash;
	int * erased;
	int * block_nb;
	int * dir_nb;
};

struct job {
	struct job * next;
	char * data;
	int *blockfile;
	SHA_CTX * hash;
	int * counter;
	size_t blocksize;
	int mode;
	int * error;
	struct coding_data * cd;
	int timedout;
	pthread_mutex_t *mutexp;
	pthread_cond_t *condp;
	int state;
	int * erased;
	int * block_nb;
	int  * dir_nb;
};

struct fs_state {
    	FILE *logfile;

	int ithreads, itimeo, iret, othreads, otimeo, oret, oqueue;
	int threads;
	char **dir;//Data drives
	int nbofdirs;
	int output_spares;
	int frm;
	char ** ext_arg;
	int ext_argc;
	int numerased;

	/* Default coding parameters */
	enum Coding_Technique tech;
	int k;
	int w;
	int m;
	size_t buffersize;
	size_t blocksize;
	int packetsize;

	int * matrix;
	int * bitmatrix;
	int **schedule;
	int input_spares;

	pthread_mutex_t *output_dir_mutex;
	pthread_mutex_t *input_dir_mutex;

	// Set for output jobs (write jobs)
	pthread_t ** output_thread;
	pthread_t ** output_ctrl_thread;

	struct job * output_queue;
	struct job * last_output_job;
	struct job *output_job_pool;

	struct job ** output_thread_queue;
	struct job ** last_output_th_job;

	sem_t* output_jobs_nb;

	pthread_mutex_t *output_queue_mutex;
	pthread_cond_t *output_thread_cond;
	pthread_mutex_t *output_thread_mutex;
	
	pthread_mutex_t ** output_thread_queue_mutex;
	pthread_cond_t ** output_ctrlth_cond;
	pthread_mutex_t ** output_ctrlth_mutex;

	pthread_mutex_t * output_job_mutex;
	struct job * output_job_pool_first;
	struct job * output_job_pool_last;

	// Set for input jobs (read jobs)
	pthread_t ** input_thread;
	pthread_t ** input_ctrl_thread;

	struct job * input_queue;
	struct job * last_input_job;

	struct job ** input_thread_queue;
	struct job ** last_input_th_job;

	pthread_mutex_t *input_queue_mutex;
	pthread_cond_t *input_thread_cond;
	pthread_mutex_t *input_thread_mutex;
	
	pthread_mutex_t ** input_thread_queue_mutex;
	pthread_cond_t ** input_ctrlth_cond;
	pthread_mutex_t ** input_ctrlth_mutex;
};

struct coding_data {
	int mod;
	int buf_index;
	int numerased;
	int test;
	char **data;
	char **data2;
	char **coding;
	char **coding2;
	char * block;
	char * block2;
	off_t size;
	int *erasures;
	int mode; //0 when created
	char *bptr;
	SHA_CTX hash;
	struct thread_data * td_array;
	char ** block_hash;
	char * file_hash;
	int * counter;
	int input_spares;
	pthread_cond_t * m_cond;
	pthread_mutex_t * m_mutex;
	

	int * erased;
	char * path;
	int error;

	/* Coding parameters for reading */
	enum Coding_Technique tech;
	int k;
	int w;
	int m;
	size_t buffersize;
	size_t blocksize;
	int packetsize;

	int * matrix;
	int * bitmatrix;
	int **schedule;
};


#define FS_DATA ((struct fs_state *) fuse_get_context()->private_data)

#endif
