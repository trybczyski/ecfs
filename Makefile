ecfs : ecfs.o log.o
	gcc -g -o ecfs  ecfs.o log.o liberation.o jerasure.o galois.o reed_sol.o cauchy.o `pkg-config fuse --libs` -lcrypto -lssl

ecfs.o : ecfs.c log.h params.h
	gcc -g -Wall `pkg-config fuse --cflags` -c ecfs.c

log.o : log.c log.h params.h
	gcc -g -Wall `pkg-config fuse --cflags` -c log.c

clean:
	rm -f ecfs *.o
