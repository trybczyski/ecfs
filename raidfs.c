/*
  	This code is derived from function prototypes found /usr/include/fuse/fuse.h
  	Copyright (C) 2001-2007  Miklos Szeredi <miklos@szeredi.hu>
  	His code is licensed under the LGPLv2.

  	This code is derived from Big Brother File System
  	Copyright (C) 2012 Joseph J. Pfeiffer, Jr., Ph.D. <pfeiffer@cs.nmsu.edu>
  	His code is licensed under the GNU GPLv3.

	Erasure coding provided by Jerasure - A C/C++ Library for a Variety of Reed-Solomon and RAID-6 Erasure Coding Techniques
	Copright (C) 2007 James S. Plank <plank@cs.utk.edu>
	His code is licensed under the GNU LGPLv2.1.

	/raidfs -o direct_io -o default_permissions rootdir/ mountdir/
	direct_io 		- disable system cashe, bigger and constant read/write buffers
	default_permissions 	- disable file permission checking by the fuse

	Two input files: Coding_parameters.txt and Coding_directories.txt
	
	Order of parameters in Coding_parameters.txt:
	--------------------
	[k m w packetsize blocksize coding_technique]

	Description:
	------------
	k - number of data block-files
	m - number of coding block-files
	w - length of coding word
	packetsize - size of coding packet
	blocksize - size of input/output blocks for block-files
	coding_technique - number from 0 to 6 corresponds:
		0 - Reed_Sol_Van
		1 - Reed_Sol_R6_Op
		2 - Cauchy_Orig
		3 - Cauchy_Good
		4 - Liberation
		5 - Blaum_Roth
		6 - Liber8tion
	
	Allowed values for the parameters:
	----------------------------------
	Reed_Sol_Van
		w must be one of {8, 16, 32}
	Reed_Sol_R6_Op
		m must be equal to 2
		w must be one of {8, 16, 32}
	Cauchy_Orig
		Must include packetsize.
	Cauchy_Good
		Must include packetsize.
	Liberation
		k must be less than or equal to w
		Must include packetsize.
		w must be greater than two and w must be prime
		packetsize must be a multiple of sizeof(int)
	Blaum_Roth
		k must be less than or equal to w
		w must be greater than two and w+1 must be prime
		Must include packetsize
		packetsize must be a multiple of sizeof(int)
	Liber8tion
		Must include packetsize
		w must equal 8
		m must equal 2
		k must be less than or equal to w

*/

#include "params.h"

#include <pthread.h>

#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <fuse.h>
#include <libgen.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/xattr.h>
#include <math.h>

#include <sys/stat.h>
#include <signal.h>
#include "jerasure.h"
#include "reed_sol.h"
#include "galois.h"
#include "cauchy.h"
#include "liberation.h"
#include "log.h"


#define MAX_DIR_LENGTH 1000
#define MAX_FILENAME_LENGTH 100

char * tech_tab[] = {"Reed_Sol_Van", "Reed_Sol_R6_Op", "Cauchy_Orig", "Cauchy_Good", "Liberation", "Blaum_Roth", "Liber8tion"};

int is_prime(int w) {
	int prime55[] = {2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,
	    73,79,83,89,97,101,103,107,109,113,127,131,137,139,149,151,157,163,167,173,179,
		    181,191,193,197,199,211,223,227,229,233,239,241,251,257};
	int i;
	for (i = 0; i < 55; i++) {
		if (w == prime55[i]) 
			return 1;
	}
	return 0;
};

void * twrite(void *t)
{
	long int ret = 0;
	int writes = 0;
	struct thread_data * my_data;

	my_data = (struct thread_data *) t;
	
	if (my_data->blockfile!=NULL) {
		writes=fwrite(my_data->data, sizeof(char), my_data->blocksize, my_data->blockfile);
		if (writes!=my_data->blocksize)
			if(feof(my_data->blockfile))
				ret = -EOF;
			else
				ret = -EIO;
		else
			SHA1_Update(&my_data->c, my_data->data, my_data->blocksize);
	}
	pthread_exit((void*) ret);
};

void * tread(void *t)
{
	long int ret = 0;
	int reads = 0;
	struct thread_data * my_data;

	my_data = (struct thread_data *) t;
	
	if (my_data->blockfile!=NULL) {
		reads=fread(my_data->data, sizeof(char), my_data->blocksize, my_data->blockfile);
		if (reads!=my_data->blocksize) {
			if(feof(my_data->blockfile))
				ret = -EOF;
			else
				ret = -EIO;
		} else {
			SHA1_Update(&my_data->c, my_data->data, my_data->blocksize);
		}
	}
	pthread_exit((void*) ret);
};

// Check whether the given user is permitted to perform the given operation on the given 

//  All the paths I see are relative to the root of the mounted
//  filesystem.  In order to get to the underlying filesystem, I need to
//  have the mountpoint.  I'll save it away early on in main(), and then
//  whenever I need a path for something I'll call this to construct
//  it.
static void fs_fullpath(char fpath[PATH_MAX], const char *path)
{
    strcpy(fpath, FS_DATA->rootdir);
    strncat(fpath, path, PATH_MAX); // ridiculously long paths will
				    // break here

    log_msg("    fs_fullpath:  rootdir = \"%s\", path = \"%s\", fpath = \"%s\"\n",
	    FS_DATA->rootdir, path, fpath);
}

///////////////////////////////////////////////////////////
//
// Prototypes for all these functions, and the C-style comments,
// come indirectly from /usr/include/fuse.h
//
/** Get file attributes.
 *
 * Similar to stat().  The 'st_dev' and 'st_blksize' fields are
 * ignored.  The 'st_ino' field is ignored except if the 'use_ino'
 * mount option is given.
 */
int fs_getattr(const char *path, struct stat *statbuf)
{
    	int retstat = 0;
	int i;

	FILE* metafile;
	char *fdir;
	size_t size;
	log_msg("\nfs_getattr(path=\"%s\", statbuf=0x%08x)\n", path, statbuf);

	fdir = (char*)malloc(sizeof(char)*(strlen(FS_DATA->rootdir)+MAX_FILENAME_LENGTH));

    	sprintf(fdir, "%s/%s",FS_DATA->rootdir, path);

    	retstat = lstat(fdir, statbuf);
    	if (retstat != 0) {
		for (i = 0; i < FS_DATA->k; i++)
		{
			sprintf(fdir, "%s/%s%s_meta",FS_DATA->rootdir, FS_DATA->dir_k[i], path);
			metafile = fopen(fdir, "rb");
			if (metafile != NULL) {
				fscanf(metafile, "%ld", &size);
				memset(statbuf, 0, sizeof(struct stat));
   				statbuf->st_mode = S_IFREG | 0444;
				statbuf->st_nlink = 1;
				statbuf->st_size = size;
				fclose(metafile);
				retstat = 0;
				break;
			}	
				
		}
		if (retstat != 0)
			retstat = -errno;		
    	}
    	log_stat(statbuf);
	free(fdir);
    	return retstat;
}

/** Read the target of a symbolic link
 *
 * The buffer should be filled with a null terminated string.  The
 * buffer size argument includes the space for the terminating
 * null character.  If the linkname is too long to fit in the
 * buffer, it should be truncated.  The return value should be 0
 * for success.
 */
int fs_readlink(const char *path, char *link, size_t size)
{
    return -EPERM;
}

/** Create a file node
 *
 * There is no create() operation, mknod() will be called for
 * creation of all non-directory, non-symlink nodes.
 */
int fs_mknod(const char *path, mode_t mode, dev_t dev)
{
    return -EPERM;
}

/** Create a directory */
int fs_mkdir(const char *path, mode_t mode)
{
    	return -EPERM;
}

/** Remove a file */
int fs_unlink(const char *path)
{
    	int retstat = 0;
    	char fpath[PATH_MAX];
	int retcount = 0;
	int i;

    	log_msg("fs_unlink(path=\"%s\")\n", path);

	for (i = 0; i < FS_DATA->k; i++) {
		sprintf(fpath, "%s/%s%s_meta",FS_DATA->rootdir, FS_DATA->dir_k[i], path);
    		retstat = unlink(fpath);
    		if (retstat < 0)
			retcount++;
		sprintf(fpath, "%s/%s%s_k%d",FS_DATA->rootdir, FS_DATA->dir_k[i], path, i+1);
    		retstat = unlink(fpath);
    		if (retstat < 0)
			retcount++;
			
	}
	for (i = 0; i < FS_DATA->m; i++) {
		sprintf(fpath, "%s/%s%s_meta",FS_DATA->rootdir, FS_DATA->dir_m[i], path);
    		retstat = unlink(fpath);
    		if (retstat < 0)
			retcount++;
		sprintf(fpath, "%s/%s%s_m%d",FS_DATA->rootdir, FS_DATA->dir_m[i], path, i+1);
    		retstat = unlink(fpath);
    		if (retstat < 0)
			retcount++;
			
	}
	if (retcount == 0)
		retstat = -errno;
	else	
		retstat = 0;    
    return retstat;
}

/** Remove a directory */
int fs_rmdir(const char *path)
{
    return -EPERM;
}

/** Create a symbolic link */
int fs_symlink(const char *path, const char *link)
{
    return -EPERM;
}

/** Rename a file */
int fs_rename(const char *path, const char *newpath)
{
    return -EPERM;
}

/** Create a hard link to a file */
int fs_link(const char *path, const char *newpath)
{
    return -EPERM;
}

/** Change the permission bits of a file */
int fs_chmod(const char *path, mode_t mode)
{
    return -EPERM;
}

/** Change the owner and group of a file */
int fs_chown(const char *path, uid_t uid, gid_t gid)
{
    return -EPERM;
}

/** Change the size of a file */
int fs_truncate(const char *path, off_t newsize)
{
    return -EPERM;
}

/** Change the access and/or modification times of a file */
/* note -- I'll want to change this as soon as 2.6 is in debian testing */
int fs_utime(const char *path, struct utimbuf *ubuf)
{
    return -EPERM;
}

/** File open operation
 *
 * No creation, or truncation flags (O_CREAT, O_EXCL, O_TRUNC)
 * will be passed to open().  Open should check if the operation
 * is permitted for the given flags.  Optionally open may also
 * return an arbitrary filehandle in the fuse_file_info structure,
 * which will be passed to all file operations.
 *
 * Changed in version 2.2
 */
int fs_open(const char *path, struct fuse_file_info *fi)
{
    	int retstat = 0;
        char *fname;
	char *mname;
	unsigned char digest[20];
        int i;
        FILE* metafile;
        struct coding_data * CODING_DATA;

        log_msg("\nfs_open(path=%s, fi=0x%08x)\n", path, fi);

	if((fi->flags & 3) != O_RDONLY )
      		return -EACCES;

        fi->fh = (uint64_t)(struct coding_data*)malloc(sizeof(struct coding_data));
        CODING_DATA = (struct coding_data*)fi->fh;
	CODING_DATA->numerased 		= 0;
	CODING_DATA->numerased 		= 0;
        CODING_DATA->data 		= (char **)malloc(sizeof(char*)*FS_DATA->k);
        CODING_DATA->coding 		= (char **)malloc(sizeof(char*)*FS_DATA->m);
        CODING_DATA->block 		= (char *)malloc(sizeof(char)*FS_DATA->buffersize);
        CODING_DATA->erasures 		= (int *)malloc(sizeof(int)*(FS_DATA->k+FS_DATA->m));
	CODING_DATA->thread_data_array 	= (struct thread_data *)malloc(sizeof(struct thread_data)*(FS_DATA->k+FS_DATA->m));
        CODING_DATA->shasumb 		= (char **)malloc(sizeof(char*)*(FS_DATA->k+FS_DATA->m));
	CODING_DATA->shasumc 		= (char *)malloc(sizeof(char)*100);
	for (i=0; i<FS_DATA->m; i++)
                CODING_DATA->coding[i] 	= (char *)malloc(sizeof(char)*FS_DATA->blocksize);
	for (i=0; i<(FS_DATA->k+FS_DATA->m); i++)
		CODING_DATA->shasumb[i] = (char *)malloc(sizeof(char)*100);
	
        /* Memory allocation for file name*/
        fname = (char*)malloc(sizeof(char)*(strlen(FS_DATA->rootdir)+MAX_FILENAME_LENGTH));
	mname = (char*)malloc(sizeof(char)*(strlen(FS_DATA->rootdir)+MAX_FILENAME_LENGTH));
         
        /* Open k and m directories for reading. Check for erased dirs*/
        for (i=0; i<FS_DATA->k+FS_DATA->m; i++) {
		SHA1_Init(&CODING_DATA->thread_data_array[i].c);
                CODING_DATA->thread_data_array[i].blocksize = FS_DATA->blocksize;
		CODING_DATA->thread_data_array[i].blockfile = NULL;
                if(i<FS_DATA->k) {
                        CODING_DATA->data[i] = CODING_DATA->block+(i*FS_DATA->blocksize);
                        CODING_DATA->thread_data_array[i].data = CODING_DATA->data[i];
                        sprintf(fname, "%s/%s%s_k%d", FS_DATA->rootdir, FS_DATA->dir_k[i], path, i+1);
			sprintf(mname, "%s/%s%s_meta", FS_DATA->rootdir, FS_DATA->dir_k[i], path);
                } else {
                        CODING_DATA->thread_data_array[i].data = CODING_DATA->coding[i-FS_DATA->k];
                        sprintf(fname, "%s/%s%s_m%d", FS_DATA->rootdir, FS_DATA->dir_m[i-FS_DATA->k], path, i-FS_DATA->k+1);
			sprintf(mname, "%s/%s%s_meta", FS_DATA->rootdir, FS_DATA->dir_m[i - FS_DATA->k], path);
                }
                
		metafile = fopen(mname, "rb");
		if (metafile != NULL) {
			fscanf(metafile, "%ld", & CODING_DATA->size);
			fscanf(metafile, "%s", CODING_DATA->shasumb[i]);
			fscanf(metafile, "%s", CODING_DATA->shasumc);
                	fclose(metafile);
                	CODING_DATA->thread_data_array[i].blockfile = fopen(fname, "rb");
                	if (CODING_DATA->thread_data_array[i].blockfile == NULL) {
                        	CODING_DATA->erasures[CODING_DATA->numerased] = i;
                        	CODING_DATA->numerased++;
                        	log_msg("\nFile missing\n");
                	}
                } else {
			CODING_DATA->erasures[CODING_DATA->numerased] = i;
                        CODING_DATA->numerased++;
                        log_msg("\nFile missing\n");
		}
        }
 	if(CODING_DATA->numerased == 0 && (FS_DATA->tech == Cauchy_Orig || FS_DATA->tech ==  Cauchy_Good || FS_DATA->tech == Liberation || FS_DATA->tech == Blaum_Roth || FS_DATA->tech == Liber8tion)) {
		CODING_DATA->thread_data_array[0].blockfile = NULL;
	}
        CODING_DATA->erasures[CODING_DATA->numerased] = -1;
        CODING_DATA->mode = 1;
	
        if(CODING_DATA->numerased>FS_DATA->m) {
                for (i=0; i<FS_DATA->k+FS_DATA->m; i++) {
			SHA1_Final(digest, &CODING_DATA->thread_data_array[i].c);
			if (CODING_DATA->thread_data_array[i].blockfile != NULL)
                               	fclose(CODING_DATA->thread_data_array[i].blockfile);
                }
		for (i = 0; i<FS_DATA->k+FS_DATA->m; i++)
			free(CODING_DATA->shasumb[i]);
		for (i = 0; i < FS_DATA->m; i++)
			free(CODING_DATA->coding[i]);
		free(CODING_DATA->shasumb);
		free(CODING_DATA->shasumc);
		free(CODING_DATA->thread_data_array);
		free(CODING_DATA->data);
		free(CODING_DATA->coding);
		free(CODING_DATA->block);
		free(CODING_DATA->erasures);
		free(CODING_DATA);
		if (CODING_DATA->numerased == FS_DATA->m+FS_DATA->k)
			retstat = -ENOENT;
		else
                	retstat = -ESTALE;
        } else {
		SHA1_Init(&CODING_DATA->c);
	}
        
        free(fname);
	free(mname);

        return retstat;
}

/** Read data from an open file
 *
 * Read should return exactly the number of bytes requested except
 * on EOF or error, otherwise the rest of the data will be
 * substituted with zeroes.  An exception to this is when the
 * 'direct_io' mount option is specified, in which case the return
 * value of the read system call will reflect the return value of
 * this operation.	
 * Changed in version 2.2
 */
int fs_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
        int i, rc;
	void *status;
	int buf_offset = 0;
	int retstat = 0;
	long int tret = 0;
        struct coding_data * CODING_DATA;
	
        log_msg("\nfs_read(path=\"%s\", buf=0x%08x, size=%d, offset=%lld, fi=0x%08x)\n", path, buf, size, offset, fi);

        CODING_DATA = (struct coding_data *)fi->fh;

        pthread_t thread[FS_DATA->k + FS_DATA->m];
        pthread_attr_t attr;
        
        if (offset >= CODING_DATA->size) return 0;

        if (CODING_DATA->buf_index>=FS_DATA->buffersize) {
                pthread_attr_init(&attr);
                pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
                /* Open files, check for erasures, read in data/coding */
                for (i = 0; i < (FS_DATA->k + FS_DATA->m); i++) {
                        rc = pthread_create(&thread[i], &attr, tread, (void *) &CODING_DATA->thread_data_array[i]);
			if (rc != 0) {
				log_msg("thread error %d\n", rc);
				retstat = -EIO;
				CODING_DATA->mode=3;
				return retstat;
			}
				
                }
                pthread_attr_destroy(&attr);
                for(i = 0; i < (FS_DATA->k + FS_DATA->m); i++) {
                        rc = pthread_join(thread[i], &status);
                        if (rc) {
                                log_msg("ERROR; return code from pthread_join() is %d\n", rc);
                                retstat = -EIO;
				CODING_DATA->mode=3;
				return retstat;
                        } else {
				tret = (long int) status;
				if(tret!=0) {
					CODING_DATA->erasures[CODING_DATA->numerased] = i;
                        		CODING_DATA->numerased++;
					CODING_DATA->erasures[CODING_DATA->numerased] = -1;
					if (CODING_DATA->thread_data_array[i].blockfile != NULL) {
						fclose(CODING_DATA->thread_data_array[i].blockfile);
						CODING_DATA->thread_data_array[i].blockfile = NULL;
					}
					log_msg("error reading file %d\n", i);
				}
			}
                }
                /* Choose proper decoding method */
		if (CODING_DATA->numerased>FS_DATA->m) {
			if (tret!=0)
				retstat = tret;
			else
				retstat = -EIO;
			CODING_DATA->mode=3;
			return retstat;
		}
		if (FS_DATA->tech == Reed_Sol_Van || FS_DATA->tech == Reed_Sol_R6_Op) {
			rc = jerasure_matrix_decode(FS_DATA->k, FS_DATA->m, FS_DATA->w, FS_DATA->matrix, 1, CODING_DATA->erasures, CODING_DATA->data, CODING_DATA->coding, FS_DATA->blocksize);
		} else {
			rc = jerasure_schedule_decode_lazy(FS_DATA->k, FS_DATA->m, FS_DATA->w, FS_DATA->bitmatrix, CODING_DATA->erasures, CODING_DATA->data, CODING_DATA->coding, FS_DATA->blocksize, FS_DATA->packetsize, 1);	
		}
               	if (rc == -1) {
                       	retstat = -EIO;
			CODING_DATA->mode=3;
			return retstat;
		}
			
               	CODING_DATA->bptr = CODING_DATA->block;
		CODING_DATA->buf_index = 0;
        } 
        if (CODING_DATA->size - offset >= size) {
		CODING_DATA->buf_index += size;
		if (CODING_DATA->buf_index<=FS_DATA->buffersize) {
                	memcpy(buf, CODING_DATA->bptr , size);
			SHA1_Update(&CODING_DATA->c, buf, size);
       	       		CODING_DATA->bptr+=size;
               		retstat = size;
		} else {
			buf_offset = CODING_DATA->buf_index - FS_DATA->buffersize;
			memcpy(buf, CODING_DATA->bptr , size - buf_offset);
			SHA1_Update(&CODING_DATA->c, buf, size - buf_offset);
			retstat = size - buf_offset;
		}
        } else {
		memcpy(buf, CODING_DATA->bptr, CODING_DATA->size - offset);
		SHA1_Update(&CODING_DATA->c, buf, CODING_DATA->size - offset);
                retstat = CODING_DATA->size - offset;
        }

	return retstat;
}

/** Write data to an open file
 *
 * Write should return exactly the number of bytes requested
 * except on error.  An exception to this is when the 'direct_io'
 * mount option is specified (see read operation).
 *
 * Changed in version 2.2
 */
// As  with read(), the documentation above is inconsistent with the
// documentation for the write() system call.
int fs_write(const char *path, const char *buf, size_t size, off_t offset,
	     struct fuse_file_info *fi)
{
	int retstat = size;
	int i;
	int rc;
	long int tret = 0;
	int buf_offset = 0;
	struct coding_data * CODING_DATA;
	
	pthread_t thread[FS_DATA->k + FS_DATA->m];
	pthread_attr_t attr;
	void *status;
	
	//log_msg("\nfs_write(path=\"%s\", buf=0x%08x, size=%d, offset=%lld, fi=0x%08x)\n", path, buf, size, offset, fi);
	CODING_DATA = (struct coding_data *)fi->fh;
	SHA1_Update(&CODING_DATA->c, buf, size);
	/*Append file size */
	CODING_DATA->size += size;
	CODING_DATA->buf_index+=size;

	/* Set pointers to point to file data */
	if (CODING_DATA->buf_index>=FS_DATA->buffersize) {
		buf_offset = CODING_DATA->buf_index-FS_DATA->buffersize;
		memcpy(CODING_DATA->bptr, buf, size - buf_offset);
		if(buf_offset > 0)
			CODING_DATA->bptr = buf + size - buf_offset;

		pthread_attr_init(&attr);
		pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

		/* Encode*/
		switch(FS_DATA->tech) {	
			case Reed_Sol_Van:
				jerasure_matrix_encode(FS_DATA->k, FS_DATA->m, FS_DATA->w, FS_DATA->matrix, CODING_DATA->data, CODING_DATA->coding, FS_DATA->blocksize);
				break;
			case Reed_Sol_R6_Op:
				reed_sol_r6_encode(FS_DATA->k, FS_DATA->w, CODING_DATA->data, CODING_DATA->coding, FS_DATA->blocksize);
				break;
			case Cauchy_Orig:
				jerasure_schedule_encode(FS_DATA->k, FS_DATA->m, FS_DATA->w, FS_DATA->schedule, CODING_DATA->data, CODING_DATA->coding, FS_DATA->blocksize, FS_DATA->packetsize);
				break;
			case Cauchy_Good:
				jerasure_schedule_encode(FS_DATA->k, FS_DATA->m, FS_DATA->w, FS_DATA->schedule, CODING_DATA->data, CODING_DATA->coding, FS_DATA->blocksize, FS_DATA->packetsize);
				break;
			case Liberation:
				jerasure_schedule_encode(FS_DATA->k, FS_DATA->m, FS_DATA->w, FS_DATA->schedule, CODING_DATA->data, CODING_DATA->coding, FS_DATA->blocksize, FS_DATA->packetsize);
				break;
			case Blaum_Roth:
				jerasure_schedule_encode(FS_DATA->k, FS_DATA->m, FS_DATA->w, FS_DATA->schedule, CODING_DATA->data, CODING_DATA->coding, FS_DATA->blocksize, FS_DATA->packetsize);
				break;
			case Liber8tion:
				jerasure_schedule_encode(FS_DATA->k, FS_DATA->m, FS_DATA->w, FS_DATA->schedule, CODING_DATA->data, CODING_DATA->coding, FS_DATA->blocksize, FS_DATA->packetsize);
				break;
		}
		/* Write data and encoded data to k+m files */
		
		for (i = 0; i < (FS_DATA->k + FS_DATA->m); i++) {
			rc = pthread_create(&thread[i], &attr, twrite, (void *) &CODING_DATA->thread_data_array[i]);
			if (rc) {
				retstat = -EIO;
				CODING_DATA->mode=2;
         			return retstat;
			}
		}
		pthread_attr_destroy(&attr);
		for(i = 0; i < (FS_DATA->k + FS_DATA->m); i++) {
      			rc = pthread_join(thread[i], &status);
      			if (rc) {
         			log_msg("ERROR; return code from pthread_join() is %d\n", rc);
				retstat = -EIO;
				CODING_DATA->mode=2;
         			return retstat;
         		} else {
				tret = (long int) status;
				if(tret!=0)
                        		CODING_DATA->numerased++;
			}

		}
		if (CODING_DATA->numerased>FS_DATA->m){
			if (tret!=0)
				retstat = tret;
			else
				retstat = -EIO;
			CODING_DATA->mode=2;
			return retstat;
		}
		if (buf_offset > 0)
			memcpy(CODING_DATA->block, CODING_DATA->bptr, buf_offset);
		CODING_DATA->bptr = CODING_DATA->block + buf_offset;
		CODING_DATA->buf_index = buf_offset;
	} else {
		memcpy(CODING_DATA->bptr, buf, size);
		CODING_DATA->bptr+=size;
	}
	return retstat;
}

/** Get file system statistics
 *
 * The 'f_frsize', 'f_favail', 'f_fsid' and 'f_flag' fields are ignored
 *
 * Replaced 'struct statfs' parameter with 'struct statvfs' in
 * version 2.5
 */
int fs_statfs(const char *path, struct statvfs *statv)
{
    int retstat = 0;
    char fpath[PATH_MAX];
    
    log_msg("\nfs_statfs(path=\"%s\", statv=0x%08x)\n",
	    path, statv);
    fs_fullpath(fpath, path);
    
    // get stats for underlying filesystem
    retstat = statvfs(fpath, statv);
    if (retstat < 0)
	retstat = -errno;
    
    log_statvfs(statv);
    
    return retstat;
}

/** Possibly flush cached data
 *
 * BIG NOTE: This is not equivalent to fsync().  It's not a
 * request to sync dirty data.
 *
 * Flush is called on each close() of a file descriptor.  So if a
 * filesystem wants to return write errors in close() and the file
 * has cached dirty data, this is a good place to write back data
 * and return any errors.  Since many applications ignore close()
 * errors this is not always useful.
 *
 * NOTE: The flush() method may be called more than once for each
 * open().  This happens if more than one file descriptor refers
 * to an opened file due to dup(), dup2() or fork() calls.  It is
 * not possible to determine if a flush is final, so each flush
 * should be treated equally.  Multiple write-flush sequences are
 * relatively rare, so this shouldn't be a problem.
 *
 * Filesystems shouldn't assume that flush will always be called
 * after some writes, or that if will be called at all.
 *
 * Changed in version 2.2
 */
int fs_flush(const char *path, struct fuse_file_info *fi)
{
    int retstat = 0;
    
    log_msg("\nfs_flush(path=\"%s\", fi=0x%08x)\n", path, fi);
    log_fi(fi);
	
    return retstat;
}

/** Release an open file
 *
 * Release is called when there are no more references to an open
 * file: all file descriptors are closed and all memory mappings
 * are unmapped.
 *
 * For every open() call there will be exactly one release() call
 * with the same flags and file descriptor.  It is possible to
 * have a file opened more than once, in which case only the last
 * release will mean, that no more reads/writes will happen on the
 * file.  The return value of release is ignored.
 *
 * Changed in version 2.2
 */
int fs_release(const char *path, struct fuse_file_info *fi)
{
    	int retstat = 0;
    	int i,j;
	char *fname;
	FILE * metafile;
	size_t mod;
	unsigned char digest[20];
	char sha1[40];
	char sha2[(FS_DATA->k+FS_DATA->m)][40];
	fname = (char*)malloc(sizeof(char)*(strlen(FS_DATA->rootdir)+MAX_FILENAME_LENGTH));
	log_msg("\nfs_release(path=\"%s\", fi=0x%08x)\n", path, fi);
    	log_fi(fi);
	struct coding_data* CODING_DATA;

	pthread_t thread[FS_DATA->k + FS_DATA->m];
	pthread_attr_t attr;
	void *status;
	int rc;
	int sha_temp;

	CODING_DATA = (struct coding_data*) fi->fh;

	SHA1_Final(digest, &CODING_DATA->c);
	for(j=0; j<20; j++){
		sha_temp = (int)digest[j];
		sprintf(&sha1[j*2],"%02x", sha_temp);
	}
	/* WRITING */
	if (CODING_DATA->mode == 0) {
		mod = (CODING_DATA->size)%FS_DATA->buffersize;
		if (mod!=0) {
			pthread_attr_init(&attr);
			pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
			for (i = mod; i < FS_DATA->buffersize; i++) 
				CODING_DATA->block[i]='0';
			switch(FS_DATA->tech) {	
			case Reed_Sol_Van:
				jerasure_matrix_encode(FS_DATA->k, FS_DATA->m, FS_DATA->w, FS_DATA->matrix, CODING_DATA->data, CODING_DATA->coding, FS_DATA->blocksize);
				break;
			case Reed_Sol_R6_Op:
				reed_sol_r6_encode(FS_DATA->k, FS_DATA->w, CODING_DATA->data, CODING_DATA->coding, FS_DATA->blocksize);
				break;
			case Cauchy_Orig:
				jerasure_schedule_encode(FS_DATA->k, FS_DATA->m, FS_DATA->w, FS_DATA->schedule, CODING_DATA->data, CODING_DATA->coding, FS_DATA->blocksize, FS_DATA->packetsize);
				break;
			case Cauchy_Good:
				jerasure_schedule_encode(FS_DATA->k, FS_DATA->m, FS_DATA->w, FS_DATA->schedule, CODING_DATA->data, CODING_DATA->coding, FS_DATA->blocksize, FS_DATA->packetsize);
				break;
			case Liberation:
				jerasure_schedule_encode(FS_DATA->k, FS_DATA->m, FS_DATA->w, FS_DATA->schedule, CODING_DATA->data, CODING_DATA->coding, FS_DATA->blocksize, FS_DATA->packetsize);
				break;
			case Blaum_Roth:
				jerasure_schedule_encode(FS_DATA->k, FS_DATA->m, FS_DATA->w, FS_DATA->schedule, CODING_DATA->data, CODING_DATA->coding, FS_DATA->blocksize, FS_DATA->packetsize);
				break;
			case Liber8tion:
				jerasure_schedule_encode(FS_DATA->k, FS_DATA->m, FS_DATA->w, FS_DATA->schedule, CODING_DATA->data, CODING_DATA->coding, FS_DATA->blocksize, FS_DATA->packetsize);
				break;
		}
	
			/* Write data and encoded data to k+m files */
			for (i = 0; i < (FS_DATA->k + FS_DATA->m); i++) {
				rc = pthread_create(&thread[i], &attr, twrite, (void *) &CODING_DATA->thread_data_array[i]);
			}
			pthread_attr_destroy(&attr);
			for(i = 0; i < (FS_DATA->k + FS_DATA->m); i++) {

      				rc = pthread_join(thread[i], &status);
      				if (rc) {
         				log_msg("ERROR; return code from pthread_join() is %d\n", rc);
         				return 1;
         			}	
			}
		}
		/* Getting file name with '/' before from path*/
		/* cut extension from file name */
		for (i = 0; i<FS_DATA->k+FS_DATA->m; i++) {
			if (i<FS_DATA->k)
				sprintf(fname, "%s/%s%s_meta", FS_DATA->rootdir, FS_DATA->dir_k[i], path);
			else
				sprintf(fname, "%s/%s%s_meta", FS_DATA->rootdir, FS_DATA->dir_m[i-FS_DATA->k], path);
			metafile = fopen(fname, "wb");
			SHA1_Final(digest, &CODING_DATA->thread_data_array[i].c);
			for(j=0; j<20; j++){
				sha_temp = (int)digest[j];
				sprintf(&sha2[i][2*j],"%02x", sha_temp);
			}
			
			if (metafile!=NULL) {
				fprintf(metafile, "%ld\n", CODING_DATA->size);
				fprintf(metafile,"%s\n", sha2[i]);
				fprintf(metafile,"%s\n", sha1);
				fprintf(metafile, "%d %d %d %d %ld %d\n", FS_DATA->k, FS_DATA->m, FS_DATA->w, FS_DATA->packetsize, FS_DATA->blocksize, (int)FS_DATA->tech );
				fprintf(metafile, "%s", tech_tab[(int)FS_DATA->tech]);
				fclose(metafile);
			}
		}
	/* READING */
	} else if (CODING_DATA->mode == 1) {
		for (i = 0; i<FS_DATA->k+FS_DATA->m; i++) {
			SHA1_Final(digest, &CODING_DATA->thread_data_array[i].c);
			for(j=0; j<20; j++){
				sha_temp = (int)digest[j];
				sprintf(&sha2[i][2*j],"%02x", sha_temp);
			}
			if(strcmp(sha2[i],CODING_DATA->shasumb[i])==0)
				log_msg("\nOK\n");
			else
				log_msg("\nNot match\n");
		}
		if(strcmp(sha1,CODING_DATA->shasumc)==0)
			log_msg("\nOK\n");
		else
			log_msg("\nNot match\n");
		for (i = 0; i<FS_DATA->k+FS_DATA->m; i++)
			free(CODING_DATA->shasumb[i]);
		free(CODING_DATA->shasumb);
		free(CODING_DATA->shasumc);
	/* ERROR WHEN WRITING */
	} else if (CODING_DATA->mode==2) {
		for (i = 0; i < FS_DATA->k + FS_DATA->m; i++) {
			SHA1_Final(digest, &CODING_DATA->thread_data_array[i].c);
			if (i < FS_DATA->k) {
				sprintf(fname, "%s/%s%s_meta",FS_DATA->rootdir, FS_DATA->dir_k[i], path);
    				retstat = unlink(fname);
  				sprintf(fname, "%s/%s%s_k%d",FS_DATA->rootdir, FS_DATA->dir_k[i], path, i+1);
    				retstat = unlink(fname);
			} else {
				sprintf(fname, "%s/%s%s_meta",FS_DATA->rootdir, FS_DATA->dir_m[i-FS_DATA->k], path);
    				retstat = unlink(fname);
				sprintf(fname, "%s/%s%s_m%d",FS_DATA->rootdir, FS_DATA->dir_m[i-FS_DATA->k], path, i-FS_DATA->k+1);
    				retstat = unlink(fname);
			}
		}
	} else if (CODING_DATA->mode==3) {
		for (i = 0; i<FS_DATA->k+FS_DATA->m; i++)
			free(CODING_DATA->shasumb[i]);
		free(CODING_DATA->shasumb);
		free(CODING_DATA->shasumc);
	}

	for (i=0; i<FS_DATA->k+FS_DATA->m; i++) {
		if (CODING_DATA->thread_data_array[i].blockfile != NULL) 
			fclose(CODING_DATA->thread_data_array[i].blockfile);
	}

	for (i = 0; i < FS_DATA->m; i++)
		free(CODING_DATA->coding[i]);

	free(CODING_DATA->thread_data_array);
	free(CODING_DATA->data);
	free(CODING_DATA->coding);
	free(CODING_DATA->block);
	free(CODING_DATA->erasures);
	free(CODING_DATA);
	free(fname);
    
    	return retstat;
}

/** Synchronize file contents
 *
 * If the datasync parameter is non-zero, then only the user data
 * should be flushed, not the meta data.
 *
 * Changed in version 2.2
 */
int fs_fsync(const char *path, int datasync, struct fuse_file_info *fi)
{
    int retstat = 0;
    
    log_msg("\nfs_fsync(path=\"%s\", datasync=%d, fi=0x%08x)\n",
	    path, datasync, fi);
    log_fi(fi);
    
    if (datasync)
	retstat = fdatasync(fi->fh);
    else
	retstat = fsync(fi->fh);
    
    return retstat;
}

/** Set extended attributes */
int fs_setxattr(const char *path, const char *name, const char *value, size_t size, int flags)
{
    int retstat = 0;
    char fpath[PATH_MAX];
    
    log_msg("\nfs_setxattr(path=\"%s\", name=\"%s\", value=\"%s\", size=%d, flags=0x%08x)\n",
	    path, name, value, size, flags);
    fs_fullpath(fpath, path);
    
    retstat = lsetxattr(fpath, name, value, size, flags);
    if (retstat < 0)
	retstat = -errno;
    
    return retstat;
}

/** Get extended attributes */
int fs_getxattr(const char *path, const char *name, char *value, size_t size)
{
    int retstat = 0;
    char fpath[PATH_MAX];
    
    log_msg("\nfs_getxattr(path = \"%s\", name = \"%s\", value = 0x%08x, size = %d)\n",
	    path, name, value, size);
    fs_fullpath(fpath, path);
    
    retstat = lgetxattr(fpath, name, value, size);
    if (retstat < 0)
	retstat = -errno;
    
    return retstat;
}

/** List extended attributes */
int fs_listxattr(const char *path, char *list, size_t size)
{
    int retstat = 0;
    char fpath[PATH_MAX];
    char *ptr;
    
    log_msg("fs_listxattr(path=\"%s\", list=0x%08x, size=%d)\n", path, list, size);
    fs_fullpath(fpath, path);
    
    retstat = llistxattr(fpath, list, size);
    if (retstat < 0) {
	retstat = -errno;
    } else {
    	log_msg("    returned attributes (length %d):\n", retstat);
    	for (ptr = list; ptr < list + retstat; ptr += strlen(ptr)+1)
		log_msg("    \"%s\"\n", ptr);
    }
    return retstat;
}

/** Remove extended attributes */
int fs_removexattr(const char *path, const char *name)
{
    int retstat = 0;
    char fpath[PATH_MAX];
    
    log_msg("\nfs_removexattr(path=\"%s\", name=\"%s\")\n",
	    path, name);
    fs_fullpath(fpath, path);
    
    retstat = lremovexattr(fpath, name);
    if (retstat < 0)
	retstat = -errno;
    
    return retstat;
}

/** Open directory
 *
 * This method should check if the open operation is permitted for
 * this  directory
 *
 * Introduced in version 2.3
 */
int fs_opendir(const char *path, struct fuse_file_info *fi)
{

    	//DIR *dp;
    	int retstat = 0;
	//int i;
	//char * dirname;
log_msg("\nfs_opendir(path=\"%s\", fi=0x%08x)\n", path, fi);
	/*
	dirname = (char*)malloc(sizeof(char)*(strlen(FS_DATA->rootdir)+100));

    	
    	
    	for (i = 0; i < FS_DATA->k; i++) {
		sprintf(dirname, "%s/dir_k%d", FS_DATA->rootdir, i+1);
    		dp = opendir(dirname);
    		if (dp != NULL) {
			fi->fh = (intptr_t) dp;
			
		}
	}
	if (dp == NULL)		
		retstat = fs_error("fs_opendir opendir");
	*/
    	log_fi(fi);
    	//free(dirname);
	//closedir((DIR *) (uintptr_t) fi->fh);
    	return retstat;
}

/** Read directory
 *
 * This supersedes the old getdir() interface.  New applications
 * should use this.
 *
 * The filesystem may choose between two modes of operation:
 *
 * 1) The readdir implementation ignores the offset parameter, and
 * passes zero to the filler function's offset.  The filler
 * function will not return '1' (unless an error happens), so the
 * whole directory is read in a single readdir operation.  This
 * works just like the old getdir() method.
 *
 * 2) The readdir implementation keeps track of the offsets of the
 * directory entries.  It uses the offset parameter and always
 * passes non-zero offset to the filler function.  When the buffer
 * is full (or an error happens) the filler function will return
 * '1'.
 *
 * Introduced in version 2.3
 */
int fs_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    	int retstat = 0;
    	DIR *dp;
    	struct dirent *de;
	char * pt, *s1;
	char *fname;
	char fnam2[MAX_FILENAME_LENGTH];
	char * dirname;
	int i;
	log_msg("\nfs_readdir(path=\"%s\", buf=0x%08x, filler=0x%08x, offset=%lld, fi=0x%08x)\n", path, buf, filler, offset, fi);
	dirname = (char*)malloc(sizeof(char)*(strlen(FS_DATA->rootdir)+MAX_FILENAME_LENGTH));

    	for (i = 0; i < FS_DATA->k; i++) {
		sprintf(dirname, "%s/%s", FS_DATA->rootdir, FS_DATA->dir_k[i]);
    		dp = opendir(dirname);
    		if (dp != NULL) {
			break;
		}
	}

	fname = (char*)malloc(sizeof(char)*(strlen(FS_DATA->rootdir)+MAX_FILENAME_LENGTH));
    	
    	de = readdir(dp);
    	if (de == 0) {
		retstat = -errno;
		return retstat;
    	}
    	// This will copy the entire directory into the buffer.  The loop exits
    	// when either the system readdir() returns NULL, or filler()
    	// returns something non-zero.  The first case just means I've
    	// read the whole directory; the second means the buffer is full.
	do {
		if ((strcmp(de->d_name , ".") == 0) ||(strcmp(de->d_name , "..") == 0)) {
			if (filler(buf, de->d_name, NULL, 0) != 0) {
	    			log_msg("    ERROR fs_readdir filler:  buffer full");
	   			return -ENOMEM;
			}
		} else {
			memcpy(fnam2, de->d_name, MAX_FILENAME_LENGTH);
			pt = strrchr(fnam2, '_');
			if ((strcmp(pt , "_meta") == 0)) {
				if (pt != NULL)
					*pt = '\0';
				s1++;
				if (filler(buf, fnam2, NULL, 0) != 0) {
	    				log_msg("    ERROR fs_readdir filler:  buffer full");
	   				return -ENOMEM;
				}
			}
		}
    	} while ((de = readdir(dp)) != NULL);
    	closedir(dp);
    	log_fi(fi);
    	free(fname);
	free(dirname);
    	return retstat;
}

/** Release directory
 *
 * Introduced in version 2.3
 */
int fs_releasedir(const char *path, struct fuse_file_info *fi)
{
    int retstat = 0;
    
    log_msg("\nfs_releasedir(path=\"%s\", fi=0x%08x)\n",
	    path, fi);
    log_fi(fi);
    
    
    
    return retstat;
}

/** Synchronize directory contents
 *
 * If the datasync parameter is non-zero, then only the user data
 * should be flushed, not the meta data
 *
 * Introduced in version 2.3
 */
// when exactly is this called?  when a user calls fsync and it
// happens to be a directory? ???
int fs_fsyncdir(const char *path, int datasync, struct fuse_file_info *fi)
{
    int retstat = 0;
    
    log_msg("\nfs_fsyncdir(path=\"%s\", datasync=%d, fi=0x%08x)\n",
	    path, datasync, fi);
    log_fi(fi);
    
    return retstat;
}

/**
 * Initialize filesystem
 *
 * The return value will passed in the private_data field of
 * fuse_context to all file operations and as a parameter to the
 * destroy() method.
 *
 * Introduced in version 2.3
 * Changed in version 2.6
 */
// Undocumented but extraordinarily useful fact:  the fuse_context is
// set up before this function is called, and
// fuse_get_context()->private_data returns the user_data passed to
// fuse_main().  Really seems like either it should be a third
// parameter coming in here, or else the fact should be documented
// (and this might as well return void, as it did in older versions of
// FUSE).
void *fs_init(struct fuse_conn_info *conn)
{
    
    log_msg("\nfs_init()\n");
    
    return FS_DATA;
}

/**
 * Clean up filesystem
 *
 * Called on filesystem exit.
 *
 * Introduced in version 2.3
 */
void fs_destroy(void *userdata)
{
    log_msg("\nfs_destroy(userdata=0x%08x)\n", userdata);
}

/**
 * Check file access permissions
 *
 * This will be called for the access() system call.  If the
 * 'default_permissions' mount option is given, this method is not
 * called.
 *
 * This method is not called under Linux kernel versions 2.4.x
 *
 * Introduced in version 2.5
 */
int fs_access(const char *path, int mask)
{
    int retstat = 0;
    char fpath[PATH_MAX];
   
    log_msg("\nfs_access(path=\"%s\", mask=0%o)\n",
	    path, mask);
    fs_fullpath(fpath, path);
    
    retstat = access(fpath, mask);
    
    if (retstat < 0)
	retstat = -errno;
    
    return retstat;
}

/**
 * Create and open a file
 *
 * If the file does not exist, first create it with the specified
 * mode, and then open it.
 *
 * If this method is not implemented or under Linux kernel
 * versions earlier than 2.6.15, the mknod() and open() methods
 * will be called instead.
 *
 * Introduced in version 2.5
 */
int fs_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
	int retstat = 0;
	char *fname;
	unsigned char digest[20];
	int i;
	
	struct coding_data * CODING_DATA;

	log_msg("\nfs_create(path=\"%s\", mode=0%03o, fi=0x%08x)\n", path, mode, fi);
	
	fi->fh = (uint64_t)(struct coding_data*)malloc(sizeof(struct coding_data));
	CODING_DATA 			= (struct coding_data*)fi->fh;
	CODING_DATA->numerased		= 0;
	CODING_DATA->size 		= 0;
	CODING_DATA->mode 		= 0;
	CODING_DATA->buf_index		= 0;
	CODING_DATA->data 		= (char **)malloc(sizeof(char*)*FS_DATA->k);
	CODING_DATA->coding		= (char **)malloc(sizeof(char*)*FS_DATA->m);
	CODING_DATA->block 		= (char *)malloc(sizeof(char)*FS_DATA->buffersize);
	CODING_DATA->erasures 		= (int *)malloc(sizeof(int)*(FS_DATA->k+FS_DATA->m));
	CODING_DATA->thread_data_array 	= (struct thread_data *)malloc(sizeof(struct thread_data)*(FS_DATA->k+FS_DATA->m));
	CODING_DATA->bptr 		= CODING_DATA->block;
	for (i = 0; i < FS_DATA->m; i++)
		CODING_DATA->coding[i] = (char *)malloc(sizeof(char)*FS_DATA->blocksize);
	
	fname = (char*)malloc(sizeof(char)*(strlen(FS_DATA->rootdir)+MAX_FILENAME_LENGTH));

	/* Open k and m directories for writing */
    	for (i=0; i<FS_DATA->k + FS_DATA->m; i++) {
		CODING_DATA->thread_data_array[i].blocksize = FS_DATA->blocksize;
		SHA1_Init(&CODING_DATA->thread_data_array[i].c);
		if (i<FS_DATA->k) {
			CODING_DATA->data[i] = CODING_DATA->block+(i*FS_DATA->blocksize);
			CODING_DATA->thread_data_array[i].data = CODING_DATA->data[i];
			
			sprintf(fname, "%s/%s%s_k%d", FS_DATA->rootdir, FS_DATA->dir_k[i], path, i+1);
		}else{
			CODING_DATA->thread_data_array[i].data = CODING_DATA->coding[i-FS_DATA->k];
			sprintf(fname, "%s/%s%s_m%d", FS_DATA->rootdir, FS_DATA->dir_m[i-FS_DATA->k], path, i-FS_DATA->k+1);
		}
		CODING_DATA->thread_data_array[i].blockfile = fopen(fname, "wb");
		if(CODING_DATA->thread_data_array[i].blockfile == NULL){
			log_msg("\nCannot open %d directory for writing\n", i);
			CODING_DATA->numerased++;
		}
	}

	if(CODING_DATA->numerased>FS_DATA->m) {
                for (i=0; i<FS_DATA->k+FS_DATA->m; i++) {
			SHA1_Final(digest, &CODING_DATA->thread_data_array[i].c);
			if (i < FS_DATA->k) {
  				sprintf(fname, "%s/%s%s_k%d",FS_DATA->rootdir, FS_DATA->dir_k[i], path, i+1);
    				retstat = unlink(fname);
			} else {
				sprintf(fname, "%s/%s%s_m%d",FS_DATA->rootdir, FS_DATA->dir_m[i-FS_DATA->k], path, i-FS_DATA->k+1);
    				retstat = unlink(fname);
			}
			if (CODING_DATA->thread_data_array[i].blockfile != NULL)
                               	fclose(CODING_DATA->thread_data_array[i].blockfile);
                }
		for (i = 0; i < FS_DATA->m; i++)
			free(CODING_DATA->coding[i]);
		free(CODING_DATA->thread_data_array);
		free(CODING_DATA->data);
		free(CODING_DATA->coding);
		free(CODING_DATA->block);
		free(CODING_DATA->erasures);
		free(CODING_DATA);
                retstat = -EIO;
		
        } else {
		SHA1_Init(&CODING_DATA->c);
	}

	free(fname);

    	return retstat;
}

/**
 * Change the size of an open file
 *
 * This method is called instead of the truncate() method if the
 * truncation was invoked from an ftruncate() system call.
 *
 * If this method is not implemented or under Linux kernel
 * versions earlier than 2.6.15, the truncate() method will be
 * called instead.
 *
 * Introduced in version 2.5
 */
int fs_ftruncate(const char *path, off_t offset, struct fuse_file_info *fi)
{
    int retstat = 0;
    log_msg("\nfs_ftruncate(path=\"%s\", offset=%lld, fi=0x%08x)\n",
	    path, offset, fi);
    log_fi(fi);
    
    retstat = ftruncate(fi->fh, offset);
    if (retstat < 0)
	retstat = -errno;
    
    return retstat;
}

/**
 * Get attributes from an open file
 *
 * This method is called instead of the getattr() method if the
 * file information is available.
 *
 * Currently this is only called after the create() method if that
 * is implemented (see above).  Later it may be called for
 * invocations of fstat() too.
 *
 * Introduced in version 2.5
 */
// Since it's currently only called after fs_create(), and fs_create()
// opens the file, I ought to be able to just use the fd and ignore
// the path...
int fs_fgetattr(const char *path, struct stat *statbuf, struct fuse_file_info *fi)
{
    	int retstat = 0;
	log_msg("\nfs_fgetattr(path=\"%s\", statbuf=0x%08x, fi=0x%08x)\n", path, statbuf, fi);
	log_fi(fi);

    	memset(statbuf, 0, sizeof(struct stat));
   	statbuf->st_mode = S_IFREG | 0444;
	statbuf->st_nlink = 1;
	statbuf->st_size = 0;

    	//retstat = fstat(FS_DATA->metafile, statbuf);
    	//if (retstat < 0)
	//retstat = fs_error("fs_fgetattr fstat");
    	
    	//log_stat(statbuf);
    	
    	return retstat;
}

struct fuse_operations fs_oper = {
  .getattr = fs_getattr,
  .readlink = fs_readlink,
  // no .getdir -- that's deprecated
  .getdir = NULL,
  .mknod = fs_mknod,
  .mkdir = fs_mkdir,
  .unlink = fs_unlink,
  .rmdir = fs_rmdir,
  .symlink = fs_symlink,
  .rename = fs_rename,
  .link = fs_link,
  .chmod = fs_chmod,
  .chown = fs_chown,
  .truncate = fs_truncate,
  .utime = fs_utime,
  .open = fs_open,
  .read = fs_read,
  .write = fs_write,
  /** Just a placeholder, don't set */ // huh???
  .statfs = fs_statfs,
  .flush = fs_flush,
  .release = fs_release,
  .fsync = fs_fsync,
  .setxattr = fs_setxattr,
  .getxattr = fs_getxattr,
  .listxattr = fs_listxattr,
  .removexattr = fs_removexattr,
  .opendir = fs_opendir,
  .readdir = fs_readdir,
  .releasedir = fs_releasedir,
  .fsyncdir = fs_fsyncdir,
  .init = fs_init,
  .destroy = fs_destroy,
  .access = fs_access,
  .create = fs_create,
  .ftruncate = fs_ftruncate,
  .fgetattr = fs_fgetattr
};

int main(int argc, char *argv[])
{
    	int fuse_stat;
	int k, m, w, blocksize, buffersize, packetsize;
	char *s1;
	char *dname;
	char * dirname;
	FILE * dirfile;
	int i;
	int technb;
	enum Coding_Technique tech;
	FILE* fp;
    	struct fs_state *fs_data;

    	// fsfs doesn't do any access checking on its own (the comment
    	// blocks in fuse.h mention some of the functions that need
    	// accesses checked -- but note there are other functions, like
    	// chown(), that also need checking!).  Since running fsfs as root
    	// will therefore open Metrodome-sized holes in the system
    	// security, we'll check if root is trying to mount the filesystem
    	// and refuse if it is.  The somewhat smaller hole of an ordinary
    	// user doing it with the allow_other flag is still there because
    	// I don't want to parse the options string.
    	if ((getuid() == 0) || (geteuid() == 0)) {
		fprintf(stderr, "Warning!: Running filesystem as root opens unnacceptable security holes\n");
    	}

    	// Perform some sanity checking on the command line:  make sure
    	// there are enough arguments, and that neither of the last two
    	// start with a hyphen (this will break if you actually have a
    	// rootpoint or mountpoint whose name starts with a hyphen, but so
    	// will a zillion other programs)
   	if ((argc < 3) || (argv[argc-2][0] == '-') || (argv[argc-1][0] == '-')){
		fprintf(stderr, "usage:  raidfs [FUSE and mount options] rootDir mountPoint\n");
		exit(0);
	}

	/* Read in parameters from setup file */
	fp = fopen("Coding_parameters.txt", "rb");
	if (fp == NULL) {
		fprintf(stderr, "Can't open Coding_parameters.txt\n");
		exit(0);	
	}
	if (fscanf(fp, "%d %d %d %d %d %d", &k, &m, &w, &packetsize, &blocksize, &technb) !=6) {
		fprintf(stderr, "Parameters are not correct\n");
		exit(0);
	}
	if (technb<0 || technb>6) {
		fprintf(stderr, "Not valid coding technique\n");
		exit(0);
	}
	fclose(fp);
	buffersize = blocksize*k;
	switch(technb) {
		case 0:
			if (w != 8 && w != 16 && w != 32) {
				fprintf(stderr,  "w must be one of {8, 16, 32}\n");
				exit(0);
			}
			tech = Reed_Sol_Van;
			break;
		case 1:
			if (m != 2) {
				fprintf(stderr,  "m must be equal to 2\n");
				exit(0);
			}
			if (w != 8 && w != 16 && w != 32) {
				fprintf(stderr,  "w must be one of {8, 16, 32}\n");
				exit(0);
			}
			tech = Reed_Sol_R6_Op;
			break;
		case 2:
			if (packetsize == 0) {
				fprintf(stderr, "Must include packetsize.\n");
				exit(0);
			}
			tech = Cauchy_Orig;
			break;
		case 3:
			if (packetsize == 0) {
				fprintf(stderr, "Must include packetsize.\n");
				exit(0);
			}
			tech = Cauchy_Good;
			break;
		case 4:
			if (k > w) {
				fprintf(stderr,  "k must be less than or equal to w\n");
				exit(0);
			}
			if (w <= 2 || !(w%2) || !is_prime(w)) {
				fprintf(stderr,  "w must be greater than two and w must be prime\n");
				exit(0);
			}
			if (packetsize == 0) {
				fprintf(stderr, "Must include packetsize.\n");
				exit(0);
			}
			if ((packetsize%(sizeof(int))) != 0) {
				fprintf(stderr,  "packetsize must be a multiple of sizeof(int)\n");
				exit(0);
			}
			tech = Liberation;
			break;
		case 5:
			if (k > w) {
				fprintf(stderr,  "k must be less than or equal to w\n");
				exit(0);
			}
			if (w <= 2 || !((w+1)%2) || !is_prime(w+1)) {
				fprintf(stderr,  "w must be greater than two and w+1 must be prime\n");
				exit(0);
			}
			if (packetsize == 0) {
				fprintf(stderr, "Must include packetsize.\n");
				exit(0);
			}
			if ((packetsize%(sizeof(int))) != 0) {
				fprintf(stderr,  "packetsize must be a multiple of sizeof(int)\n");
				exit(0);
			}
			tech = Blaum_Roth;
			break;
		case 6:
			if (packetsize == 0) {
				fprintf(stderr, "Must include packetsize\n");
				exit(0);
			}
			if (w != 8) {
				fprintf(stderr, "w must equal 8\n");
				exit(0);
			}
			if (m != 2) {
				fprintf(stderr, "m must equal 2\n");
				exit(0);
			}
			if (k > w) {
				fprintf(stderr, "k must be less than or equal to w\n");
				exit(0);
			}
			tech = Liber8tion;
			break;
	}
	if (blocksize%(sizeof(int)*w*packetsize) != 0) {
		fprintf(stderr, "Proper values for parameters: blocksize mod (sizeof(int)*w*packetsize) = 0\n");
		exit(0);
	}

	dname = (char*)malloc(sizeof(char)*(MAX_DIR_LENGTH));
	dirname = (char*)malloc(sizeof(char)*(MAX_DIR_LENGTH));

    	fs_data = malloc(sizeof(struct fs_state));
    	if (fs_data == NULL) {
		perror("main calloc");
		abort();
    	}

    	// Pull the rootdir out of the argument list and save it in my
    	// internal data
    	fs_data->rootdir = realpath(argv[argc-2], NULL);
    	argv[argc-2] = argv[argc-1];
    	argv[argc-1] = NULL;
    	argc--;
    	
	fs_data->tech = tech;
	fs_data->k = k;
	fs_data->m = m;
	fs_data->w = w;
	fs_data->packetsize = packetsize;
	fs_data->blocksize = blocksize;
	fs_data->buffersize = buffersize;
    	fs_data->logfile = log_open();
	fs_data->matrix = NULL;
	fs_data->bitmatrix = NULL;
	fs_data->schedule = NULL;
	switch(tech) {
		case Reed_Sol_Van:
			fs_data->matrix 	= reed_sol_vandermonde_coding_matrix(k, m, w);
			break;
		case Reed_Sol_R6_Op:
			fs_data->matrix 	= reed_sol_r6_coding_matrix(k, w);
			break;
		case Cauchy_Orig:
			fs_data->matrix 	= cauchy_original_coding_matrix(k, m, w);
			fs_data->bitmatrix	= jerasure_matrix_to_bitmatrix(k, m, w, fs_data->matrix);
			fs_data->schedule 	= jerasure_smart_bitmatrix_to_schedule(k, m, w, fs_data->bitmatrix);
			break;
		case Cauchy_Good:
			fs_data->matrix		= cauchy_good_general_coding_matrix(k, m, w);
			fs_data->bitmatrix 	= jerasure_matrix_to_bitmatrix(k, m, w, fs_data->matrix);
			fs_data->schedule 	= jerasure_smart_bitmatrix_to_schedule(k, m, w, fs_data->bitmatrix);
			break;
		case Liberation:
			fs_data->bitmatrix 	= liberation_coding_bitmatrix(k, w);
			fs_data->schedule 	= jerasure_smart_bitmatrix_to_schedule(k, m, w, fs_data->bitmatrix);
			break;
		case Blaum_Roth:
			fs_data->bitmatrix	= blaum_roth_coding_bitmatrix(k, w);
			fs_data->schedule 	= jerasure_smart_bitmatrix_to_schedule(k, m, w, fs_data->bitmatrix);
			break;
		case Liber8tion:
			fs_data->bitmatrix 	= liber8tion_coding_bitmatrix(k);
			fs_data->schedule 	= jerasure_smart_bitmatrix_to_schedule(k, m, w, fs_data->bitmatrix);
			break;
	}


	/*Open file with names of directories*/
	strcpy(dirname, fs_data->rootdir);
	s1 = strrchr(dirname, '/');
	if (s1 != NULL){
		*s1 = '\0';
    	}
	sprintf(dname, "%s/Coding_directories.txt", dirname);
	dirfile = fopen(dname, "r");
	if (dirfile == NULL) {
		fprintf(stderr, "Can't open Coding_directories.txt!\n");
		exit(0);	
	}
	fs_data->dir_k = (char **)malloc(sizeof(char*)*k);
	fs_data->dir_m = (char **)malloc(sizeof(char*)*m);
	for (i = 0; i < k; i++) {
		fs_data->dir_k[i] = (char *)malloc(sizeof(char)*MAX_DIR_LENGTH);
		if(fscanf(dirfile, "%s", fs_data->dir_k[i])==EOF) {
			fprintf(stderr, "Nb of directories is less than k+m!\n");
			exit(0);
		}
	}
	for (i = 0; i < m; i++) {
		fs_data->dir_m[i] = (char *)malloc(sizeof(char)*MAX_DIR_LENGTH);
		if(fscanf(dirfile, "%s", fs_data->dir_m[i])==EOF) {
			fprintf(stderr, "Nb of directories is less than k+m!\n");
			exit(0);
		}
	}
    	fclose(dirfile);
    	// turn over control to fuse
	fprintf(stderr, "coding technique: %s\n", tech_tab[technb]);
	fprintf(stderr, "k=%d m=%d w=%d\n", k, m, w);
	fprintf(stderr, "packetsize=%d\n", packetsize);
	fprintf(stderr, "blocksize=%d\n", blocksize);
	fprintf(stderr, "buffersize=%d\n", buffersize);
    	fprintf(stderr, "about to call fuse_main\n");
	fuse_stat = fuse_main(argc, argv, &fs_oper, fs_data);
	free(dname);
	free(dirname);
	jerasure_free_schedule(fs_data->schedule);
	for (i = 0; i < k; i++) {
		free(fs_data->dir_k[i]);
	}
	for (i = 0; i < m; i++) {
		free(fs_data->dir_m[i]);
	}	
	free(fs_data->dir_k);
	free(fs_data->dir_m);
	free(fs_data->matrix);
	free(fs_data->bitmatrix);
	free(fs_data->schedule);
	free(fs_data->rootdir);
	fclose(fs_data->logfile);
	free(fs_data);
	
    	fprintf(stderr, "fuse_main returned %d\n", fuse_stat);
    	return fuse_stat;
}

